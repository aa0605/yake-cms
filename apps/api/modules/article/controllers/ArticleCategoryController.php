<?php

namespace apps\api\modules\article\controllers;

use common\map\CommonMap;
use common\modules\article\map\ArticleCategoryMap;
use xing\commonSet\models\CommonSet;
use Yii;
use common\modules\article\ArticleCategory;

class ArticleCategoryController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\article\ArticleCategory';



    public function actionIndexPublish($page = 1)
    {
        try {
            $list = ArticleCategory::getLists([
                'page' => $page,
                'display' => CommonMap::BOOLEAN_YES,
                'parentId' => ArticleCategoryMap::NEWS_ID
            ]);
            $categoryIdMicro = CommonSet::readKeyValue(ArticleCategoryMap::KEY_MICRO);
            foreach ($list as $k => $v) if ($v->categoryId == $categoryIdMicro) unset($list[$k]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }


}
