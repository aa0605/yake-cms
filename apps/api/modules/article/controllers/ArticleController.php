<?php

namespace apps\api\modules\article\controllers;

use apps\api\modules\ApiBaseController;
use common\map\api\ResponseMap;
use common\modules\article\Article;
use common\modules\article\logic\ArticleLogic;
use common\modules\task\map\TaskMap;
use common\modules\task\service\TaskProgressService;
use common\modules\user\logic\UserCalculationLogLogic;
use common\modules\user\logic\UserLogic;
use common\modules\user\map\UserCalculationLogMap;
use common\modules\user\UserFollow;
use xing\helper\exception\ApiException;
use Yii;

/**
 * Default controller for the `article` module
 */
class ArticleController extends ApiBaseController
{
    public $modelClass = 'common\modules\article\Article';

    public function actionView($articleId)
    {
        try {
            $data = Article::findOne($articleId);
            if (empty($data)) throw new ApiException(ResponseMap::ERROR_DATA_EMPTY);
            $return = $data->toArray();
            $return['content'] = $data->articleData;
            $return['author'] = $data->author;
            $return['articleList'] = Article::getListModel()->andWhere(['<', 'A.articleId', $articleId])->limit(3)->all();
            $return['isFollow'] = $data->userId ? !empty(UserFollow::readInfo($this->userId, $data->userId)) : 1;
            $return['isVoteUp'] = Article::find()->where(['articleId' => $articleId, 'userId' => $this->userId])->exists();
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }


    public function actionIndex($categoryId = null, $page = 1, $userId = null)
    {
        try {
            $list = Article::index($page, $categoryId, $this->userId, $userId);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionCreate()
    {
        $db = Article::getDb()->beginTransaction();
        try {
            UserLogic::checkPublish($this->userId);
            $categoryId = Yii::$app->request->post('categoryId');
            $title = Yii::$app->request->post('title');
            $content = Yii::$app->request->post('content');
            $thumb = Yii::$app->request->post('thumbnail');
            $isOriginal = Yii::$app->request->post('isOriginal');
            TaskProgressService::finishTaskOnce($this->userId, TaskMap::CODE_PUBLISH_ARTICLE);
            $data = ArticleLogic::publish($this->userId, $categoryId, $title, $thumb, $content, $isOriginal);

            $db->commit();
            return $this->returnData($data);
        } catch (\Exception $e) {
            $db->rollBack();
            return $this->returnExceptionError($e);
        }
    }

    public function actionDelete($articleId)
    {
        try {
            ArticleLogic::delete($this->userId, $articleId);
            return $this->returnApiSuccess();
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionSearch($q, $page = 1)
    {

        try {
            $list = Article::getModel($page)->where(['like', 'title', $q])->all();
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }
}

