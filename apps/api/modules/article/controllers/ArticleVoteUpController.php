<?php

namespace apps\api\modules\article\controllers;

use common\modules\article\logic\ArticleVoteUpLogic;
use common\modules\task\map\TaskMap;
use common\modules\task\service\TaskProgressService;
use common\modules\user\logic\UserAuthorLevelLogic;
use Yii;
use common\modules\article\ArticleVoteUp;

class ArticleVoteUpController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\article\ArticleVoteUp';

    public function actionView($id)
    {
        try {
            $data = ArticleVoteUp::findOne($id);
            return $this->returnData($data);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionIndex($page)
    {
        try {
            $list = ArticleVoteUp::getLists(['page' => $page]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionCreate($id)
    {
        $db = ArticleVoteUp::getDb()->beginTransaction();
        try {
            $m = ArticleVoteUpLogic::create($this->userId, $id);
            TaskProgressService::finishTaskOnce($this->userId, TaskMap::CODE_THUMB);
            UserAuthorLevelLogic::reward($this->userId, 'Vote');
            $db->commit();
            return $this->returnData($m);
        } catch (\Exception $e) {
            $db->rollBack();
            return $this->returnExceptionError($e);
        }
    }

    public function actionDelete($id)
    {
        try {
            ArticleVoteUpLogic::delete($this->userId, $id);
            return $this->returnApiSuccess();
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
