<?php

namespace apps\api\modules\site\controllers;

use common\service\ContentSafeService;
use xing\upload\UploadYiiLogic;
use Yii;
use yii\web\UploadedFile;

class FileUploadController extends \apps\api\modules\ApiBaseController
{

    public $checkSign = false;

    public $checkLogin = false;


    public function actionUpload()
    {

        try {
            $module = trim(Yii::$app->request->post('module') ?: Yii::$app->request->get('module'), '"');
            $return = UploadYiiLogic::apiUpload('file', $module);
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    /**
     * 上传图片
     * @return array
     * @throws \Exception
     */
    public function actionUploadImage()
    {

        try {
            $module = Yii::$app->request->post('module');
            $return = UploadYiiLogic::apiUpload('file', $module);
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionUploadAvatar($sex)
    {

        try {
            $return = UploadYiiLogic::apiUpload('file', 'LoveUser');
            LoveUserLogic::checkAvatarBeauty($return['saveUrl'], $sex);
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionUploadBaseImage()
    {
        $base64 = Yii::$app->request->post('base64');
        $module = trim(Yii::$app->request->post('module'), '"');
        try {
            $return = UploadYiiLogic::uploadBase64Image($base64, $module);
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    public function actionUploadTemp()
    {

        try {
            $module = Yii::$app->request->post('module');
            $return = UploadYiiLogic::uploadTemp('file', $module, $this->userId);
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionUploadCommon()
    {

        try {
            $module = Yii::$app->request->post('module');
            $return = UploadYiiLogic::apiUpload('file', $module);
            ContentSafeService::checkImage([$return['url']], ['porn', 'terrorism', 'live']);
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }
}
