<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2018/6/3
 * Time: 17:49
 */

namespace apps\api\modules\site\controllers;

use xing\helper\exception\ModelYiiException;
use common\modules\site\SiteFeedback;
use Yii;

class SiteFeedbackController extends \apps\api\modules\ApiBaseController
{

    public $modelClass = '\common\modules\site\SiteFeedback';

    public function actionCreate()
    {
        try {
            $m = new SiteFeedback;
            $m->userId = $this->userId;
            $m->type = Yii::$app->request->post('type') ?: null;
            $m->content = Yii::$app->request->post('content');
            if (!$m->save()) throw new ModelYiiException($m);
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }
}