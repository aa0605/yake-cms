<?php

namespace apps\api\modules\user\controllers;

use Yii;
use common\modules\user\ComputeLog;

class ComputeLogController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\ComputeLog';



    public function actionIndex($page)
    {
        try {
            $list = ComputeLog::getLists(['page' => $page]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }


}
