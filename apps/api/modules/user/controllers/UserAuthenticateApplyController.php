<?php

namespace apps\api\modules\user\controllers;

use common\modules\user\logic\UserAuthenticateApplyLogic;
use Yii;
use common\modules\user\UserAuthenticateApply;

class UserAuthenticateApplyController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserAuthenticateApply';




    public function actionView($type)
    {
        try {
            $data = UserAuthenticateApply::readOne($this->userId, $type);
            return $this->returnData($data);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionIndex($page)
    {
        try {
            $list = UserAuthenticateApply::getLists(['page' => $page]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionApply()
    {
        try {
            $idNumber = Yii::$app->request->post('idNumber');
            $image1 = Yii::$app->request->post('image1');
            $image2 = Yii::$app->request->post('image2');
            $type = Yii::$app->request->post('type');
            $name = Yii::$app->request->post('name');
            $m = UserAuthenticateApplyLogic::save($this->userId, $type, $name, $idNumber, $image1, $image2);
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionDelete($id)
    {
        try {
            $this->userDeleteData($id);
            return $this->returnApiSuccess();
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }
}
