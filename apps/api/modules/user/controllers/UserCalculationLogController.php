<?php

namespace apps\api\modules\user\controllers;

use common\modules\user\map\UserCalculationLogMap;
use common\modules\user\User;
use Yii;
use common\modules\user\UserCalculationLog;

class UserCalculationLogController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserCalculationLog';



    public function actionIndex($page, $type = null)
    {
        try {
            $list = UserCalculationLog::getModel($page)
                ->filterWhere(['userId' => $this->userId, 'type' => $type])
                ->with(['user' => function ($q) {
                    $q->select(['userId', 'avatar', 'sqc', 'nickname', 'mobile']);
                }])
                ->asArray()
                ->all();
            ;
            return $this->returnJson(['list' => $list, 'calculation' => User::logicFindOne($this->userId)->calculation]);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }


    public function actionInviteCount()
    {
        try {
            $where = ['userId' => $this->userId, 'type' => UserCalculationLogMap::TYPE_RECOMMEND];
            $inviteCalculation = UserCalculationLog::find()->where($where)->count();
            $inviteUserNumber = User::find()->where(['recommendUserId' => $this->userId])->count();
            return $this->returnJson(['inviteCalculation' => $inviteCalculation, 'inviteUserNumber' => $inviteUserNumber]);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }
}
