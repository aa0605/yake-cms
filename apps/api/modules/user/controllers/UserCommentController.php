<?php

namespace apps\api\modules\user\controllers;

use common\map\CommonMap;
use common\modules\user\logic\UserAuthorLevelLogic;
use Yii;
use common\modules\article\ArticleComment;

class UserCommentController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\article\ArticleComment';


    public function actionIndex($page)
    {
        try {
            $list = ArticleComment::getModel($page)
                ->where(['userId' => $this->userId, 'status' => CommonMap::BOOLEAN_YES])
                ->with(['user' => function ($q) {
                    $q->select(['userId', 'avatar', 'sqc', 'nickname']);
                }])
                ->asArray()
                ->all();
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
