<?php

namespace apps\api\modules\user\controllers;

use common\modules\user\logic\UserGratuityLogic;
use Yii;
use common\modules\user\UserGratuity;

class UserGratuityController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserGratuity';



    public function actionIndex($page)
    {
        try {
            $list = UserGratuity::getLists(['page' => $page]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionCreate($money, $type, $targetId)
    {
        try {
            $m = UserGratuityLogic::create($this->userId, $money, $type, $targetId);
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
