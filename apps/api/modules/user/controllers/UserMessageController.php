<?php

namespace apps\api\modules\user\controllers;


use common\map\CommonMap;
use common\modules\user\map\UserMap;
use common\modules\user\UserMessage;
use Yii;

class UserMessageController extends \apps\api\modules\ApiBaseController
{
    /**
     * @return array|mixed
     * @throws \Exception.
     */
    public function actionIndex($page)
    {
        try {
            $list = UserMessage::getModel($page)
                ->where(['userId' => $this->userId])
                ->with(['user' => function ($q) {$q->select(UserMap::$baseField);}])
                ->asArray()
                ->all();
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    /**
     * 删除
     * @param $dynamicId
     * @return array
     * @throws \Exception
     */
    public function actionReadAll()
    {
        try {
            UserMessage::updateAll(['isRead' => CommonMap::BOOLEAN_YES], ['userId' => $this->userId]);
            return $this->returnApiSuccess();
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }
}
