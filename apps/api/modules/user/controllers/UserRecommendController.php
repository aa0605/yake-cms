<?php

namespace apps\api\modules\user\controllers;

use Yii;
use common\modules\user\UserRecommend;

class UserRecommendController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserRecommend';
    public function actionView($id)
    {
        try {
            $data = UserRecommend::findOne($id);
            return $this->returnData($data);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionIndex($page)
    {
        try {
            $list = UserRecommend::getLists(['page' => $page]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }


}
