<?php

namespace apps\api\modules\user\controllers;

use common\modules\user\logic\UserSqcLockLogic;
use Yii;
use common\modules\user\UserSqcLock;

class UserSqcLockController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserSqcLock';



    public function actionIndex($status, $page = 1)
    {
        try {
            $list = UserSqcLock::getLists(['page' => $page, 'userId' => $this->userId, 'status' => $status]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionCreate($sqc)
    {
        try {
            $m = UserSqcLockLogic::create($this->userId, $sqc);
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }


    public function actionUncage($lockId)
    {
        try {
            $m = UserSqcLockLogic::uncage($this->userId, $lockId);
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
