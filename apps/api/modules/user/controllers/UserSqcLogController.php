<?php

namespace apps\api\modules\user\controllers;

use common\modules\user\map\UserSqcLockMap;
use common\modules\user\UserSqcLock;
use Yii;
use common\modules\user\UserSqcLog;

class UserSqcLogController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserSqcLog';

    public function actionIndex($page)
    {
        try {
            $list = UserSqcLog::getLists(['page' => $page, 'userId' => $this->userId]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionMy()
    {
        try {
            $return = [
                'sqc' => $this->user->sqc,
                'calculation' => $this->user->calculation,
                'sqcLock' => UserSqcLock::count($this->userId, UserSqcLockMap::STATUS_LOCK),
                'sqcRelease' => UserSqcLock::count($this->userId, UserSqcLockMap::STATUS_RELEASE),
            ];
            return $this->returnData($return);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
