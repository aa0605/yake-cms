<?php

namespace apps\api\modules\user\controllers;

use Yii;
use common\modules\user\UserSqcRecharge;

class UserSqcRechargeController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserSqcRecharge';




    public function actionView($id)
    {
        try {
            $data = UserSqcRecharge::findOne($id);
            return $this->returnData($data);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionIndex($page)
    {
        try {
            $list = UserSqcRecharge::getLists(['page' => $page, 'userId' =>$this->userId]);
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionCreate()
    {
        try {
            $m =  new UserSqcRecharge;
            $m->userId = $this->userId;
            $m->address = Yii::$app->request->post('address');
            $m->number = Yii::$app->request->post('number');
            $m->createTime = date('Y-m-d H:i:s');
            $m->logicSave();
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
