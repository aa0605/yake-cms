<?php

namespace apps\api\modules\user\controllers;

use common\modules\user\logic\UserSqcWithdrawLogic;
use common\modules\user\User;
use Yii;
use common\modules\user\UserSqcWithdraw;

class UserSqcWithdrawController extends \apps\api\modules\ApiBaseController
{
    public $modelClass = 'common\modules\user\UserSqcWithdraw';



    public function actionIndex($page)
    {
        try {
            $list = UserSqcWithdraw::getModel($page, 1)->orderBy(['id' => SORT_DESC])->where(['userId' => $this->userId])->all();
            return $this->returnList($list);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

    public function actionCreate($money, $address, $name = '')
    {
        try {
            $m = UserSqcWithdrawLogic::create($this->userId, $address, $money, $name);
            return $this->returnData($m);
        } catch (\Exception $e) {
            return $this->returnExceptionError($e);
        }
    }

}
