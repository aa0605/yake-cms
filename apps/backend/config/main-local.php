<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YOs-Dm7mg_A5hjzfmc1fvl9v6HYG59c5',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [ //生成器名称
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [ //设置我们自己的模板
                    //模板名 => 模板路径
                    'myCrud' => '@vendor/xing.chen/helper/yii/gii/templates/crud/default',
                ]
            ],
            'controller' => [ //生成器名称
                'class' => 'yii\gii\generators\controller\Generator',
                'templates' => [ //设置我们自己的模板
                    //模板名 => 模板路径
                    'api' => '@vendor/xing.chen/helper/yii/gii/templates/controller/api',
                ]
            ],
        ],
    ];
}

return $config;
