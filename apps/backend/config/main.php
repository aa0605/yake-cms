<?php
$params = array_merge(
    require __DIR__ . '/../../../common/config/params.php',
    require __DIR__ . '/../../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'apps\backend\controllers',
    'bootstrap' => ['log'],
    'defaultRoute' => 'admin/site/index',
    'modules' => [
        'admin' => [
            'class' => 'xing\ace\Module',
            // Do not verify permissions
            'verifyAuthority' => false,
        ],
        'article' => [
            'class' => 'apps\backend\modules\article\Module',
        ],
        'site' => [
            'class' => 'apps\backend\modules\site\Module',
        ],
        'user' => [
            'class' => 'apps\backend\modules\user\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'xing\ace\models\AdminUser',
            'enableAutoLogin' => true,
            'loginUrl' => ['/admin/site/login'],
        ],
        // Background user
        'admin' => [
            'class' => '\yii\web\User',
            'identityClass' => 'xing\ace\models\AdminUser',
            'enableAutoLogin' => true,
            'loginUrl' => ['/admin/default/login'],
            'idParam' => '_adminId',
            'identityCookie' => ['name' => '_admin','httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
