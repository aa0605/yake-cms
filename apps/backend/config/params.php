<?php
return [
    'adminEmail' => 'admin@example.com',
    'xingUploader' => [
        // 前端访问路径
        'visitDomain' => IMG_DOMAIN . 'upload/',
        // 上传url
        'uploadUrl' => 'article/file-upload/xing',
        'config' => [
            'defaultImage' => '/images/icon/upload.jpg',
            'disableGlobalDnd' => true,
            'accept' => [
                'title' => 'Images',
                'extensions' => 'jpg,jpeg,bmp,png',
                'mimeTypes' => 'image/jpg,image/jpeg,image/png,image/bmp',
            ],
            'pick' => [
                'multiple' => false,
            ],
        ],
    ],
];
