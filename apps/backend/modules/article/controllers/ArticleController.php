<?php

namespace apps\backend\modules\article\controllers;

use xing\article\backend\controllers\ArticleBackendTrait;
use Yii;
use apps\backend\BaseController;

/**
 * ArticleCommentController implements the CRUD actions for ArticleComment model.
 */
class ArticleController extends BaseController
{
    use ArticleBackendTrait;

    public function beforeAction($action)
    {
        parent::beforeAction($action);
        switch ($action->id) {
            case 'update':
            case 'create':
                $this->viewPath = '';
        }
        return true;
    }
}
