<?php

namespace apps\backend\modules\article\controllers;

use xing\article\backend\controllers\CategoryBackendTrait;
use Yii;
use apps\backend\BaseController;

class CategoryController extends BaseController
{
    use CategoryBackendTrait;

}