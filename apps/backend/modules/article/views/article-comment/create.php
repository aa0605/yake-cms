<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\article\ArticleComment */

$this->title = '增加文章评论';
$this->params['breadcrumbs'][] = ['label' => '文章评论', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-comment-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
