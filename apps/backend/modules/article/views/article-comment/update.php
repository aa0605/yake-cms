<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\article\ArticleComment */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '文章评论', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->commentId, 'url' => ['view', 'id' => $model->commentId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="article-comment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
