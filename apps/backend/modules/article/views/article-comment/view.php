<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\article\ArticleComment */

$this->title = $model->commentId;
$this->params['breadcrumbs'][] = ['label' => '文章评论', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-comment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->commentId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->commentId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'commentId',
            'articleId',
            'userId',
            'content',
        ],
    ]) ?>

</div>
