<?php

namespace apps\backend\modules\site\controllers;

use xing\commonSet\backend\controllers\CommonSetTrait;

/**
 * CommonSetController implements the CRUD actions for CommonSet model.
 */
class CommonSetController extends \apps\backend\controllers\BaseController
{
    use CommonSetTrait;
}
