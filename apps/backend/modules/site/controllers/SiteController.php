<?php

namespace apps\backend\modules\site\controllers;

use apps\backend\controllers\BaseController;
use yii\web\Controller;

/**
 * Default controller for the `site` module
 */
class SiteController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
