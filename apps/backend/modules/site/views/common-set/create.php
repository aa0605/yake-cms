<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\site\CommonSet */

$this->title = '增加Common Set';
$this->params['breadcrumbs'][] = ['label' => 'Common Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="common-set-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
