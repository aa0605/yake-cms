<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\site\CommonSet */

$this->title = $model->key;
$this->params['breadcrumbs'][] = ['label' => 'Common Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="common-set-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->key], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->key], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'key',
            'label',
            'describe',
            'value',
            'updateTime',
            'userId',
        ],
    ]) ?>

</div>
