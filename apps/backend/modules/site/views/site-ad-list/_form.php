<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteAdList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-ad-list-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading">
             </div>
    <div class="panel-body">

            <?= $form->field($model, 'adId')->dropDownList(\common\modules\site\SiteAd::dropDownList('title', [], null)) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pageType')->dropDownList(\common\modules\site\map\SiteAdListMap::$pageType) ?>

        <?= $form->field($model, 'src')->widget('xing\webuploader\yii2\FileInput') ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
