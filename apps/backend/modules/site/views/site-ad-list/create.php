<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteAdList */

$this->title = '增加广告';
$this->params['breadcrumbs'][] = ['label' => '广告管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-ad-list-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
