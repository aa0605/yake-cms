<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\site\SiteAdListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '广告管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-ad-list-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('增加', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'listId',
//            'adId',
            'title',
            [
                'attribute' => 'pageType',
                'value' => function($data) {
                    return \common\modules\site\map\SiteAdListMap::$pageType[$data->pageType] ?? null;
                }
            ],
            [
                'attribute' => 'src',
                'format' => 'raw',
                'value' => function($data) {
                    $img = \xing\upload\UploadYiiLogic::getDataUrl($data->src);
                    return Html::a('<img src="' .$img. '" style="width: 50px;">', $img, ['target' => '_blank']);
                }
            ],
            //'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
