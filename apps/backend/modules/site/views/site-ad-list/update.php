<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteAdList */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '广告管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->listId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="site-ad-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
