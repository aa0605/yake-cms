<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteAd */

$this->title = '增加Site Ad';
$this->params['breadcrumbs'][] = ['label' => 'Site Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-ad-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
