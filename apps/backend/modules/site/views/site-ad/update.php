<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteAd */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => 'Site Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->adId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="site-ad-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
