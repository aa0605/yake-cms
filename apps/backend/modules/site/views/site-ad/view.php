<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteAd */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Site Ads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-ad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->adId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->adId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'adId',
            'title',
            'adType',
            'status',
        ],
    ]) ?>

</div>
