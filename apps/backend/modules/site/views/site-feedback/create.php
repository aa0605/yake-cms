<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteFeedback */

$this->title = '增加用户反馈';
$this->params['breadcrumbs'][] = ['label' => '用户反馈', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-feedback-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
