<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\site\SiteFeedback */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '用户反馈', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="site-feedback-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
