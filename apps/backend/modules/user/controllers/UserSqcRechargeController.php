<?php

namespace apps\backend\modules\user\controllers;

use common\modules\user\logic\UserSqcLogLogic;
use common\modules\user\map\UserSqcLogMap;
use common\modules\user\User;
use Yii;
use common\modules\user\UserSqcRecharge;
use common\modules\user\search\UserSqcRechargeSearch;
use apps\backend\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserSqcRechargeController implements the CRUD actions for UserSqcRecharge model.
 */
class UserSqcRechargeController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSqcRechargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new UserSqcRecharge();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            // return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $db = $model::getDb()->beginTransaction();
        try {

            if ($model->load(Yii::$app->request->post()) && $model->save())  {
                $user = User::findOne($model->userId);
                $sqc = Yii::$app->request->post($user->formName())['sqc'] ?? 0;

                if ($sqc) UserSqcLogLogic::increase($model->userId, $sqc, UserSqcLogMap::TYPE_RECHARGE, $this->userId);
                $db->commit();
                return $this->redirectUpdateLastPage();
            }

            $db->commit();
            return $this->render('update', [
                'model' => $model,
            ]);
        } catch (\Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserSqcRecharge model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserSqcRecharge the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserSqcRecharge::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
