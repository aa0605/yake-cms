<?php

namespace apps\backend\modules\user\controllers;

use common\modules\user\logic\UserSqcLogLogic;
use common\modules\user\map\UserSqcLogMap;
use common\modules\user\map\UserSqcWithdrawMap;
use common\modules\user\User;
use Yii;
use common\modules\user\UserSqcWithdraw;
use common\modules\user\search\UserSqcWithdrawSearch;
use apps\backend\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserSqcWithdrawController implements the CRUD actions for UserSqcWithdraw model.
 */
class UserSqcWithdrawController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSqcWithdrawSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new UserSqcWithdraw();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
            // return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$sqc = $model->totalMoney ?? 0;

        $db = $model::getDb()->beginTransaction();
        try {
			if (!empty($_POST)) {
				
				if (in_array($model->status, [UserSqcWithdrawMap::STATUS_FINISH, UserSqcWithdrawMap::STATUS_REJECT])) 
					throw new \Exception('不可再处理');
			}
            if ($model->load(Yii::$app->request->post()) && $model->save())  {
				$user = User::findOne($model->userId);
				// 拒绝
				if ($model->status == UserSqcWithdrawMap::STATUS_REJECT) {
	
					if ($sqc)
					    UserSqcLogLogic::increase($model->userId, $sqc, UserSqcLogMap::TYPE_WITHDRAW_RETURN, $this->userId);
				}
                $db->commit();
                return $this->redirectUpdateLastPage();
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        } catch (\Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserSqcWithdraw model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserSqcWithdraw the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserSqcWithdraw::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
