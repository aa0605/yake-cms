<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthenticateApply */
/* @var $form yii\widgets\ActiveForm */
$model->status = \common\modules\user\map\UserAuthenticateApplyMap::STATUS_REJECT;
?>

<div class="user-author-apply-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading"><?= \common\modules\user\User::findOne($model->userId)->nickname?></div>
    <div class="panel-body">

        <?= $form->field($model, 'status')->dropDownList(\common\modules\user\map\UserAuthenticateApplyMap::$status) ?>

        <?= $form->field($model, 'reason')->textarea() ?>
        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
