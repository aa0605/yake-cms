<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthenticateApply */

$this->title = '增加User Author Apply';
$this->params['breadcrumbs'][] = ['label' => '作者认证', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-apply-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
