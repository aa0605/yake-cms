<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserAuthenticateApplySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '作者认证申请';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-apply-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'userId',
                'format' => 'raw',
                'value' => function($data) {
                    $user = \common\modules\user\User::findOne($data->userId);
                    return Html::a('<img src="' .$user->avatar. '" style="width: 50px;">', $user->avatar, ['target' => '_blank']) . '昵称：' .$user->nickname . '手机号：' . $user->mobile;
                }
            ],
            'idNumber',
            [
                'attribute' => 'image1',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a('<img src="' .$data->image1. '" style="height: 50px;">', $data->image1, ['target' => '_blank']);
                }
            ],
            [
                'attribute' => 'image2',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a('<img src="' .$data->image2. '" style="height: 50px;">', $data->image2, ['target' => '_blank']);
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\modules\user\map\UserAuthenticateApplyMap::$status[$data->status] ?? null;
                }
            ],
            [
                'label' => '操作',
                'format' => 'raw',
                'value' => function($data) {
                    $ok = $data->status == \common\modules\user\map\UserAuthenticateApplyMap::STATUS_OK;
                    $reject =  $data->status == \common\modules\user\map\UserAuthenticateApplyMap::STATUS_REJECT;
                    return ($ok ? '已认证' : Html::button('通过', ['class' => 'btn btn-xs btn-info', 'onclick' => "ok(this, {$data->applyId})"]))
                        .  ($reject ? '已拒绝' : Html::a('拒绝', ['update', 'id' => $data->applyId], ['class' => 'btn btn-xs btn-danger']))
                        ;
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
        ],
    ]); ?>
</div>

<style>
    .btn {
        margin-left: 5px;
    }
</style>

<script>
    function ok(obj, applyId) {
        $(obj).html('加载中');

        var url = "<?=\yii\helpers\Url::to(['ok', 'id' => ''])?>";

        $.ajax({
            url: url +  applyId,
            success: function(result) {
                $(obj).parent().html(result === '1' ? '已通过' : '失败');
            }
        });
    }
</script>