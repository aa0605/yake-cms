<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthenticateApply */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '作者认证', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applyId, 'url' => ['view', 'id' => $model->applyId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-author-apply-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
