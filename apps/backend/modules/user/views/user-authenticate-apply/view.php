<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthenticateApply */

$this->title = $model->applyId;
$this->params['breadcrumbs'][] = ['label' => '作者认证', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-apply-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->applyId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->applyId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applyId',
            'userId',
            'idNumber',
            'image1',
            'image2',
        ],
    ]) ?>

</div>
