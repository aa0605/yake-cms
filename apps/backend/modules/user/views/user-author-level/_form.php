<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthorLevel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-author-level-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading">
             </div>
    <div class="panel-body">

            <?= $form->field($model, 'level')->textInput(['placeholder' => '普通用户等级为0']) ?>

    <?= $form->field($model, 'experience')->textInput() ?>

    <?= $form->field($model, 'addition')->textInput(['maxlength' => true, 'placeholder' => '瓜分每日sqc的加成，1表示不加不减，1.2表示加20%，点赞、评论、阅读请在下面设置']) ?>

        <?= $form->field($model, 'dayMaxArticle')->textInput(['placeholder' => '0表示无限制']) ?>

        <?= $form->field($model, 'dayMaxVideo')->textInput(['placeholder' => '0表示无限制']) ?>
        <?= $form->field($model, 'dayMaxVote')->textInput(['placeholder' => '0表示无限制']) ?>

        <?= $form->field($model, 'dayMaxComment')->textInput(['placeholder' => '0表示无限制']) ?>

        <?= $form->field($model, 'dayMaxRead')->textInput(['placeholder' => '0表示无限制']) ?>

        <?= $form->field($model, 'dayMaxShare')->textInput(['placeholder' => '0表示无限制']) ?>

        <?= $form->field($model, 'rewardArticle')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rewardVideo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rewardVote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rewardComment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rewardRead')->textInput(['maxlength' => true]) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
