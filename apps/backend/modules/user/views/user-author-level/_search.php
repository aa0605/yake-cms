<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthorLevelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-author-level-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'level') ?>

    <?= $form->field($model, 'experience') ?>

    <?= $form->field($model, 'addition') ?>

    <?= $form->field($model, 'dayMaxVote') ?>

    <?= $form->field($model, 'dayMaxComment') ?>

    <?php // echo $form->field($model, 'dayMaxRead') ?>

    <?php // echo $form->field($model, 'dayMaxShare') ?>

    <?php // echo $form->field($model, 'rewardVote') ?>

    <?php // echo $form->field($model, 'rewardComment') ?>

    <?php // echo $form->field($model, 'rewardRead') ?>

    <?php // echo $form->field($model, 'rewardShare') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        <div class="help-block"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
