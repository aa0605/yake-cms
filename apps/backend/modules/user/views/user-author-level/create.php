<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthorLevel */

$this->title = '增加';
$this->params['breadcrumbs'][] = ['label' => '作者等级', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-level-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
