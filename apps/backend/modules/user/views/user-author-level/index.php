<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserAuthorLevelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '作者等级';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-level-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('增加', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'level',
            'experience',
            'addition',
            'dayMaxArticle',
            'dayMaxVideo',
            'dayMaxVote',
            'dayMaxComment',
            'dayMaxRead',
            'dayMaxShare',
            'rewardArticle',
            'rewardVideo',
            'rewardVote',
            'rewardComment',
            'rewardRead',
            'rewardShare',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
