<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthorLevel */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '作者等级', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->level, 'url' => ['view', 'id' => $model->level]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-author-level-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
