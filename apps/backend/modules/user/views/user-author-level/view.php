<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserAuthorLevel */

$this->title = $model->level;
$this->params['breadcrumbs'][] = ['label' => '作者等级', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-author-level-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->level], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->level], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'level',
            'experience',
            'addition',
            'dayMaxVote',
            'dayMaxComment',
            'dayMaxRead',
            'rewardVote',
            'rewardComment',
            'rewardRead',
        ],
    ]) ?>

</div>
