<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserCalculationLog */

$this->title = '增加';
$this->params['breadcrumbs'][] = ['label' => '趣火记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-calculation-log-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
