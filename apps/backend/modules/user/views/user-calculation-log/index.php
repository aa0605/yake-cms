<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\search\UserCalculationLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '趣火记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-calculation-log-index form-inline">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('增加', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'logId',
            [
                'attribute' => 'userId',
                'format' => 'raw',
                'value' => function($data) use($searchModel) {
                    return Html::a($data->user->nickname ?: '-', ['user/view', 'id' => $data->user->userId])
                        . ' ' . Html::a('记录查看', ['index', $searchModel->formName() => ['userId' => $data->userId]])
                        ;
                }
            ],
            'type',
            'money',
            'createTime',

//            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
