<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserCalculationLog */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '趣火记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->logId, 'url' => ['view', 'id' => $model->logId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-calculation-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
