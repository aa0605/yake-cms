<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserComputeLog */

$this->title = '增加User Compute Log';
$this->params['breadcrumbs'][] = ['label' => '趣火记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-compute-log-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
