<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserComputeLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '趣火记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-compute-log-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'computeId',
            'userId',
            'number',
            'createTime',
            'note',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
