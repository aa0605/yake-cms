<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserComputeLog */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '趣火记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->computeId, 'url' => ['view', 'id' => $model->computeId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-compute-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
