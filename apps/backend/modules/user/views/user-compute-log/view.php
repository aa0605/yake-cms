<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserComputeLog */

$this->title = $model->computeId;
$this->params['breadcrumbs'][] = ['label' => '趣火记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-compute-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->computeId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->computeId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'computeId',
            'userId',
            'number',
            'createTime',
            'note',
        ],
    ]) ?>

</div>
