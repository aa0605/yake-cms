<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-message-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading">
             </div>
    <div class="panel-body">

            <?= $form->field($model, 'userId')->label('用户id') ?>


    <?= $form->field($model, 'createDate')->textInput(['value' => $model->createDate ?: date('Y-m-d H:i:s')]) ?>

    <?= $form->field($model, 'content')->textarea(['maxlength' => true]) ?>


        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
