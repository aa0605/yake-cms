<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserMessage */

$this->title = '增加';
$this->params['breadcrumbs'][] = ['label' => '用户消息', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-message-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
