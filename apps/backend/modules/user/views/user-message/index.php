<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\search\UserMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '用户消息';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-message-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('增加', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'msgId',
            'userId',
//            'code',
//            'type',
//            'targetId',
            'isRead',
            'createDate',
            'content',
            //'json',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
