<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserMessage */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '用户消息', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->msgId, 'url' => ['view', 'id' => $model->msgId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-message-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
