<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserRecommendSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-recommend-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'recommendId') ?>

    <?= $form->field($model, 'executeUserId') ?>

    <?= $form->field($model, 'toUserId') ?>

    <?= $form->field($model, 'parentUserId') ?>

    <?= $form->field($model, 'createTime') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        <div class="help-block"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
