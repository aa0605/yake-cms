<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserRecommend */

$this->title = '增加';
$this->params['breadcrumbs'][] = ['label' => '推荐管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-recommend-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
