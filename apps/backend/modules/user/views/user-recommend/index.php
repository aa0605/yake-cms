<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserRecommendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '推荐管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-recommend-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'recommendId',
            [
                'attribute' => 'executeUserId',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->executeUser->nickname ?: '-', ['user/view', 'id' => $data->executeUser->userId]);
                }
            ],
            [
                'attribute' => 'toUserId',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->toUser->nickname ?: '-', ['user/view', 'id' => $data->toUser->userId]);
                }
            ],
            [
                'attribute' => 'parentUserId',
                'format' => 'raw',
                'value' => function($data) {
                    if (empty($data->parentUserId)) return null;
                    return Html::a($data->parentUser->nickname ?: '-', ['user/view', 'id' => $data->parentUser->userId]);
                }
            ],
            'createTime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
