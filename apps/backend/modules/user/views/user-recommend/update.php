<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserRecommend */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '推荐管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->recommendId, 'url' => ['view', 'id' => $model->recommendId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-recommend-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
