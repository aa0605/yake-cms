<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSignLog */

$this->title = '增加User Sign Log';
$this->params['breadcrumbs'][] = ['label' => 'User Sign Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sign-log-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
