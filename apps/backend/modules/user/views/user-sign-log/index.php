<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserSignLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Sign Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sign-log-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('增加', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'signId',
            'userId',
            'date',
            'createTime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
