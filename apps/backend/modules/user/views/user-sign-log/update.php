<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSignLog */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => 'User Sign Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->signId, 'url' => ['view', 'id' => $model->signId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-sign-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
