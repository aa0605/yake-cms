<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\search\UserSqcLockSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-sqc-lock-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userId')->label('用户id') ?>



    <?= $form->field($model, 'status')->dropDownList(\common\modules\user\map\UserSqcLockMap::$status, ['prompt' => '请选择']) ?>


    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        <div class="help-block"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
