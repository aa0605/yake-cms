<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcLock */

$this->title = '增加';
$this->params['breadcrumbs'][] = ['label' => '琐仓记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-lock-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
