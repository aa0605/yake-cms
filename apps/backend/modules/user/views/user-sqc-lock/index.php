<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\search\UserSqcLockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '琐仓记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-lock-index form-inline">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'lockId',
            [
                'attribute' => 'userId',
                'format' => 'raw',
                'value' => function($data) use($searchModel) {
                    return Html::a($data->user->nickname ?: '-', ['user/view', 'id' => $data->user->userId])
                        . ' ' . Html::a('记录查看', ['index', $searchModel->formName() => ['userId' => $data->userId]])
                        ;
                }
            ],
            'money',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\modules\user\map\UserSqcLockMap::$status[$data->status] ?? null;
                }
            ],
            'createTime',
            'endTime',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>
</div>
