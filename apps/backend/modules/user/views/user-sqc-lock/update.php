<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcLock */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '琐仓记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->lockId, 'url' => ['view', 'id' => $model->lockId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-sqc-lock-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
