<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcLock */

$this->title = $model->lockId;
$this->params['breadcrumbs'][] = ['label' => '琐仓记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-lock-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->lockId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->lockId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'lockId',
            'userId',
            'money',
            'status',
            'createTime',
            'endTime',
        ],
    ]) ?>

</div>
