<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcLog */

$this->title = '增加User Sqc Log';
$this->params['breadcrumbs'][] = ['label' => 'sqc收支记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-log-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
