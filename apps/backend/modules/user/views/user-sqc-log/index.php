<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserSqcLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'sqc收支记录';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-log-index form-inline">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'logId',
            [
                'attribute' => 'userId',
                'format' => 'raw',
                'value' => function($data) use($searchModel) {
                    return Html::a($data->user->nickname ?: '-', ['user/view', 'id' => $data->user->userId])
                        . ' ' .Html::a('记录', ['index', $searchModel->formName() => ['userId' => $data->userId]])
                        ;
                }
            ],
            'money',
            [
                'attribute' => 'type',
                'value' => function($data) {
                    return \common\modules\user\map\UserSqcLogMap::$type[$data->type] ?? null;
                }
            ],
            'balance',
            'targetId',
            'createTime',
            'note',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
