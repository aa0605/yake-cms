<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcLog */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => 'sqc收支记录', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->logId, 'url' => ['view', 'id' => $model->logId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-sqc-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
