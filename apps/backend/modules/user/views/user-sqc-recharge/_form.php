<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcRecharge */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-sqc-recharge-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading">用户id：<?=$model->user->userId ?? ''?> 用户昵称：<?=$model->user->nickname ?? ''?></div>
    <div class="panel-body">

    <?= $form->field($model, 'status')->dropDownList(\common\modules\user\map\UserSqcRechargeMap::$status, ['value' => \common\modules\user\map\UserSqcRechargeMap::STATUS_FINISH]) ?>

    <?= $form->field($model, 'note')->textarea(['maxlength' => true, 'placeholder' => '仅管理员可见']) ?>

        <?= $form->field($model->user ?: new \common\modules\user\User(), 'sqc')->label('增加sqc数量')->textInput(['placeholder' => '当前用户sqc为：'. ($model->user->sqc ?? 0), 'value' => ''])?>

        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
