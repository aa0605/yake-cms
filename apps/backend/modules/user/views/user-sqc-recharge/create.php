<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcRecharge */

$this->title = '增加充值';
$this->params['breadcrumbs'][] = ['label' => '充值', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-recharge-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
