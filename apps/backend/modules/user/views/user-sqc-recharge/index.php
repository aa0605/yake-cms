<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\search\UserSqcRechargeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '充值';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-recharge-index form-inline">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'userId',
            'number',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\modules\user\map\UserSqcRechargeMap::$status[$data->status] ?? null;
                }
            ],
            'address',
            'createTime',
            'handelTime',
            'note',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
</div>
