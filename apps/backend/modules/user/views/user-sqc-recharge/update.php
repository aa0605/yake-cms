<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcRecharge */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '充值', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-sqc-recharge-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
