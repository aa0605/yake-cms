<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-sqc-task-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading">
             </div>
    <div class="panel-body">

            <?= $form->field($model, 'createTime')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'notLoginNumber')->textInput() ?>

    <?= $form->field($model, 'releaseLockNumber')->textInput() ?>

    <?= $form->field($model, 'lockSqc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'totalCalculation')->textInput() ?>

    <?= $form->field($model, 'grantSqc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grantNumber')->textInput() ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
