<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\search\UserSqcTaskSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-sqc-task-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'taskId') ?>

    <?= $form->field($model, 'createTime') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'notLoginNumber') ?>

    <?= $form->field($model, 'releaseLockNumber') ?>

    <?php // echo $form->field($model, 'lockSqc') ?>

    <?php // echo $form->field($model, 'totalCalculation') ?>

    <?php // echo $form->field($model, 'grantSqc') ?>

    <?php // echo $form->field($model, 'grantNumber') ?>

    <?php // echo $form->field($model, 'note') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        <div class="help-block"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
