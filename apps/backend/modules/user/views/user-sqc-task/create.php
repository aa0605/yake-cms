<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcTask */

$this->title = '增加sqc每日任务';
$this->params['breadcrumbs'][] = ['label' => 'sqc每日任务', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-task-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
