<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\search\UserSqcTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '发放日志';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-task-index form-inline">

    <p>
        每天0点时会自动发放；每次发放无论成功或失败都会有日志，但致命错误很可能不会有日志。
    </p>
    <p>
        会员越多，执行时间越长。
    </p>
    <p>
        <?= Html::button('立即发放 ' . \common\modules\site\logic\CommonSetLogic::getDailySqc() . ' SQC', ['class' => 'btn btn-danger', 'onclick' => 'grant(this)']) ?>
        <?= Html::a('修改每日发放SQC数量' , ['/site/common-set/update', 'id' => \common\modules\site\map\CommonSetMap::KEY_SQC_GIVE_DAY], ['class' => 'btn btn-info', 'target' => '_blank']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'taskId',
            'createTime',
            'finishTime',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\modules\user\map\UserSqcTaskMap::$status[$data->status] ?? null;
                }
            ],
            'notLoginNumber',
            'releaseLockNumber',
            'lockSqc',
            'totalCalculation',
            'grantNumber',
            'grantSqc',
            'note',

        ],
    ]); ?>
</div>

<script>
    function grant(obj) {
        $(obj).attr('disabled', true)
        if(confirm("确定现在发放吗？本次发放不会影响到每日自动发放。相当于多发放一次，并且用户算力截止时间为当前时间，通常用于测试。每次发放在用户量大的时候且高峰期可能会造成资源竞争大大影响系统性能。")){
            $(obj).html('正在执行，在出结果之前请不要在其他页面中再次执行，以免重复发放')
            $.ajax({
                //请求地址
                url : "<?=\yii\helpers\Url::to(['grant'])?>",
                dataType: 'JSON',
                //请求成功
                success : function(result) {
                    console.log(result);
                    $(obj).html(result.message)
                },
                //请求失败，包含具体的错误信息
                error : function(e){
                    console.log(e.status);
                    console.log(e.responseText);
                    $(obj).html('失败')
                    alert(e.responseText)
                },
                complete: function () {
                    $(obj).attr('disabled', false)
                }
            });
        } else {
            $(obj).attr('disabled', false)
        }


    }
</script>