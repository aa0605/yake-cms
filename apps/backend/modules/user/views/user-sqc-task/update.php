<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcTask */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => 'sqc每日任务', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->taskId, 'url' => ['view', 'id' => $model->taskId]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-sqc-task-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
