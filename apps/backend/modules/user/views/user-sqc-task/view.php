<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcTask */

$this->title = $model->taskId;
$this->params['breadcrumbs'][] = ['label' => 'sqc每日任务', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->taskId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->taskId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'taskId',
            'date',
            'status',
            'notLoginNumber',
            'releaseLockNumber',
            'lockSqc',
            'totalCalculation',
            'grantSqc',
            'note',
        ],
    ]) ?>

</div>
