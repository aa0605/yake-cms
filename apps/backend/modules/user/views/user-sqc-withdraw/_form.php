<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcWithdraw */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-sqc-withdraw-form">

    <?php $form = ActiveForm::begin([
    'options' => ['class' => 'form-horizontal'],
    'fieldConfig' => [
    'template' => '<label class="col-sm-2 control-label">{label}</label><div class="col-sm-5">{input}{error}{hint}</div>',
    ],
    ]); ?>
<div class="panel panel-primary">
    <div class="panel-heading">
             </div>
    <div class="panel-body">


    <?= $form->field($model, 'status')->dropDownList(\common\modules\user\map\UserSqcWithdrawMap::$status, ['value' => $model->status ?: \common\modules\user\map\UserSqcRechargeMap::STATUS_FINISH]) ?>

    <?= $form->field($model, 'reason')->textarea(['maxlength' => true, 'placeholder' => '如果拒绝，请填写拒绝原因']) ?>

    <?= $form->field($model, 'note')->textarea(['maxlength' => true, 'placeholder' => '仅管理员可见']) ?>

        <div class="form-group text-center">
            <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
