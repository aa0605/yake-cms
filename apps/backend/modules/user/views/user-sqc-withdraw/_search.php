<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\search\UserSqcWithdrawSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-sqc-withdraw-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php  echo $form->field($model, 'status')->dropDownList(\common\modules\user\map\UserSqcWithdrawMap::$status, ['prompt' => '请选择']) ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'createTime') ?>

    <?php // echo $form->field($model, 'handelTime') ?>

    <?php // echo $form->field($model, 'reason') ?>

    <?php // echo $form->field($model, 'note') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        <div class="help-block"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
