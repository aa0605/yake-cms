<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcWithdraw */

$this->title = '增加提现';
$this->params['breadcrumbs'][] = ['label' => '提现', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-withdraw-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
