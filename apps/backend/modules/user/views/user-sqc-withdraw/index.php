<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\search\UserSqcWithdrawSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '提现';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-withdraw-index form-inline">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'userId',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data->user->nickname, ['user/view', 'id' => $data->user->userId]);
                }
            ],
            'totalMoney',
            'commission',
            'money',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\modules\user\map\UserSqcWithdrawMap::$status[$data->status] ?? null;
                }
            ],
            'address',
//            'name',
            'createTime',
            'handelTime',
            'reason',
            'note',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
</div>
