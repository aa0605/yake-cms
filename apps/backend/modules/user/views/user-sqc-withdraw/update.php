<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcWithdraw */

$this->title = '修改';
$this->params['breadcrumbs'][] = ['label' => '提现', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="user-sqc-withdraw-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
