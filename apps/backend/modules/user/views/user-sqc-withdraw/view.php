<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSqcWithdraw */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => '提现', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-sqc-withdraw-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '确定删除吗？',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'userId',
            'totalMoney',
            'commission',
            'money',
            'status',
            'address',
            'name',
            'createTime',
            'handelTime',
            'reason',
            'note',
        ],
    ]) ?>

</div>
