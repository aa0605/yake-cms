<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'mobile') ?>

    <?= $form->field($model, 'status')->dropDownList((new \common\modules\user\User())::$status, ['prompt' => '全部']) ?>

    <?php // echo $form->field($model, 'money') ?>

    <?php // echo $form->field($model, 'sqc') ?>

    <?php // echo $form->field($model, 'nickname') ?>

    <?php // echo $form->field($model, 'lastLoginTime') ?>

    <?php // echo $form->field($model, 'loginNumber') ?>

    <?php // echo $form->field($model, 'lang') ?>

    <?php // echo $form->field($model, 'lastLoginIp') ?>

    <?php // echo $form->field($model, 'deviceToken') ?>

    <?php // echo $form->field($model, 'createTime') ?>

    <?php // echo $form->field($model, 'mobileVendor') ?>

    <?php // echo $form->field($model, 'mobileModel') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'regSources') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'password_hash') ?>

    <?php // echo $form->field($model, 'registerIP') ?>

    <?php // echo $form->field($model, 'unreadMessage') ?>

    <?php // echo $form->field($model, 'withdrawLock') ?>

    <div class="form-group">
        <?= Html::submitButton('搜索', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('重置', ['class' => 'btn btn-default']) ?>
        <div class="help-block"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
