<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\user\User */

$this->title = '增加User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
