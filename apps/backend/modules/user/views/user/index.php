<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\user\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '用户';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index form-inline">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a('增加', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 不显示搜索框 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'userId',
            'mobile',
            'authorLevel',
//            'loginToken',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return \common\modules\user\User::$status[$data->status] ?? null;
                }
            ],
            'sqc',
            'calculation',
            //'nickname',
            //'lastLoginTime',
            //'loginNumber',
            //'lang',
            //'lastLoginIp',
            //'deviceToken',
            //'createTime:datetime',
            //'mobileVendor',
            //'mobileModel',
            //'auth_key',
            //'regSources',
            //'password_reset_token',
            //'password_hash',
            //'registerIP',
            //'unreadMessage',
            //'withdrawLock',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}'],
        ],
    ]); ?>
</div>
