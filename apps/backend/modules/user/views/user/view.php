<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\user\User */

$this->title = $model->userId;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->userId], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userId',
            'mobile',
            'thirdId',
//            'loginToken',
            'status',
            'sqc',
            'nickname',
            'lastLoginTime',
            'loginNumber',
            'lang',
            'lastLoginIp',
            'deviceToken',
            'createTime:datetime',
            'mobileVendor',
            'mobileModel',
            'auth_key',
            'regSources',
//            'password_reset_token',
//            'password_hash',
            'registerIP',
            'unreadMessage',
            'withdrawLock',
        ],
    ]) ?>

</div>
