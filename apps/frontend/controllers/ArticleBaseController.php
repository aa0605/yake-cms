<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2018/9/16
 * Time: 23:30
 */

namespace apps\frontend\controllers;

use xing\article\logic\TemplateLogicc;
use Yii;

trait ArticleBaseController
{
    use \xing\article\frontend\controllers\ArticleFrontendTrait;

}