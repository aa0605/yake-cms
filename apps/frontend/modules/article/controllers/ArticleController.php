<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2018/9/16
 * Time: 20:40
 */

namespace apps\frontend\modules\article\controllers;


use xing\article\logic\TemplateLogic;
use Yii;

class ArticleController extends \apps\frontend\controllers\BaseController
{
    use \apps\frontend\controllers\ArticleBaseController;

}