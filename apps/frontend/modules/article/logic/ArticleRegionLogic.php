<?php


namespace apps\frontend\modules\article\logic;


use common\modules\site\Region;

class ArticleRegionLogic
{


    public static function getRegion()
    {
        $host = $_SERVER['HTTP_HOST'];
        $twoDomain = explode('.', $host)[0];
        $regionId = intval($twoDomain);
        $region = Region::findOne($regionId);
        return [
            'regionId' => $regionId ?: '',
            'name' => $region->name ?? '',

        ];
    }
}