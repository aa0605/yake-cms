<?php
/* @var $this \yii\web\View */
use common\modules\site\logic\SiteRegionLogic;

$this->title = '标题';
$this->metaTags[] = '<meta name="keywords" content=""/>';
$this->metaTags[] = '<meta name="description" content=""/>';
?>

<div class="banner">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/rbyslx/"><img src="/yishu/images/5eb3aad9eac0a.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/gjjyzykfy/"><img src="/yishu/images/5e995db4e7086.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/psonehd/3341.html"><img src="/yishu/images/5e4bcd34ce303.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/hwxly/"><img src="/yishu/images/5e784fa5e0aea.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="www.psoneart.com/zbkc/"><img src="/yishu/images/5e5616e6e720b.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/zsg/"><img src="/yishu/images/5ba36094708c5.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/yylx/"><img src="/yishu/images/5d53b7b8b99cb.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="http://www.psoneart.com/kxkkc/"><img src="/yishu/images/5c8867471b448.jpg" alt="艺术留学"></a>
            </div>
            <div class="swiper-slide">
                <a href="/oneldx/"><img src="/yishu/images/5c933d3899f45.jpg" alt="艺术留学"></a>
            </div>
        </div>
        <div class="swiper-pagination" style=""></div>
    </div>
    <script>

    </script>
</div>

<div class="ldxsj" style="">
    <div class="ldxdh row" style="">
        <div onmouseover="ldxqh_bg(1)"
             onclick="window.open('http://www.psoneart.com/oneldx/?k=1','_blank');return false;" class="col-xs-2">插画纯艺系
        </div>
        <div onmouseover="ldxqh_bg(2)"
             onclick="window.open('http://www.psoneart.com/oneldx/?k=2','_blank');return false;" class="col-xs-2">视觉设计系
        </div>
        <div onmouseover="ldxqh_bg(3)"
             onclick="window.open('http://www.psoneart.com/oneldx/?k=3','_blank');return false;" class="col-xs-2">数字媒体系
        </div>
        <div onmouseover="ldxqh_bg(4)"
             onclick="window.open('http://www.psoneart.com/oneldx/?k=4','_blank');return false;" class="col-xs-2">设计创新系
        </div>
        <div onmouseover="ldxqh_bg(5)"
             onclick="window.open('http://www.psoneart.com/oneldx/?k=5','_blank');return false;" class="col-xs-2">建筑空间系
        </div>
        <div onmouseover="ldxqh_bg(6)"
             onclick="window.open('http://www.psoneart.com/oneldx/?k=6','_blank');return false;" class="col-xs-2">时尚设计系
        </div>
    </div>
    <div class="ldxxq row" style="">
        <div class="nr" style="">
            <div class="sz" style="">UCP跨学科体系</div>
            <div class="mc" style="">与国外名校无缝对接</div>
        </div>
        <div class="nr" style="">
            <div class="sz" style="">海外招生官联盟</div>
            <div class="mc" style="">推荐信+文书指导+作品集审核</div>
        </div>
        <div class="nr" style="">
            <div class="sz" style="">限额招生质量保障</div>
            <div class="mc" style="">各专业限额招生100名</div>
        </div>
        <div class="nr" style="">
            <div class="sz" style="">高额奖学金申请</div>
            <div class="mc" style="">导师多V1辅导+私人定制</div>
        </div>
        <div class="nr" style="border:0;">
            <div class="sz" style="">6900+成功案例</div>
            <div class="mc" style="">六大教学体系+三重审核</div>
        </div>
    </div>
</div>

<style>

    <!--

    .ldxsj .ldxxq {

        width: 1560px;

        height: 150px;

        margin: 0 auto;

        padding: 20px 0;

    }

    .ldxsj .ldxxq .nr {

        float: left;

        text-align: center;

        width: 20%;

        border-right: 1px solid #d7d7d8;

        padding: 10px 0;

    }

    .ldxsj .ldxxq .nr .sz {

        font-size: 26px;

        color: #EE782A;

        line-height: 52px;

    }

    .ldxsj .ldxxq .nr .mc {

        font-size: 16px;

        line-height: 32px;

        color: #333333;

        margin-top: 0px;

    }

    .v20190507_index {

        width: 100%;

        font-weight: 300;

        /*font-family: 'PingFang SC','Microsoft Yahei','微软雅黑', 'HanHei SC', 'Helvetica Neue', 'Helvetica', 'Lantinghei SC', 'Hiragino Sans GB', 'STXihei', '华文细黑', 'Microsoft Sans Serif', 'WenQuanYi Micro Hei', 'SimSun', '宋体', 'sans';  */

        font-family: 'PingFang SC', "Helvetica", "Arial", "微软雅黑", "Microsoft Yahei", "宋体", "simsun", 'HanHei SC', 'Helvetica Neue', 'Helvetica', 'Lantinghei SC', 'Hiragino Sans GB', 'STXihei', '华文细黑', 'Microsoft Sans Serif', 'WenQuanYi Micro Hei';

    }

    .v20190507_index img {

        display: block;

        margin: 0;

        padding: 0;

        border: 0;

    }

    .v20190507_index .czmh {

        color: #ed7829;

    }

    .v20190507_index .dbt {

        width: 1560px;

        margin: 0 auto;

        padding-top: 90px;

        position: relative;

    }

    .v20190507_index .dbt .pic {

        position: absolute;

        margin-top: 22px;

        z-index: 1;

    }

    .v20190507_index .dbt .ebt {

        padding-left: 30px;

        font-size: 38px;

        line-height: 48px;

        color: #333333;

        font-weight: 400;

        text-align: left;

        position: relative;

        z-index: 2;

    }

    .v20190507_index .dbt .hx {

        width: 30px;

        height: 1px;

        margin: 20px 0 20px 30px;

        background-color: #333333;

    }

    .v20190507_index .dbt .zbt {

        padding-left: 30px;

        font-size: 24px;

        line-height: 24px;

        color: #333333;

        font-weight: 400;

        text-align: left;

    }


    .v20190507_index_xz {

        width: 100%;

        height: 810px;

        position: relative;

    }

    .v20190507_index_xz > .pic {

        position: absolute;

        width: 100%;

        height: 810px;

        z-index: 1;

    }

    .v20190507_index_xz > .pic img {

        width: 100%;

        height: 810px;

    }

    .v20190507_index_xz .dbt {

        position: relative;

        z-index: 2;

    }

    .v20190507_index_xz .dbt .ebt {

        color: #fff;

    }

    .v20190507_index_xz .dbt .hx {

        background-color: #fff;

    }

    .v20190507_index_xz .dbt .zbt {

        color: #fff;

    }

    .v20190507_index_xz .nrk {

        width: 1590px;

        height: 520px;

        margin: 0 auto;

        position: relative;

        z-index: 2;

    }

    .v20190507_index_xz .nrk .nr {

        margin: 30px 15px;

        width: 500px;

        height: 200px;

        border-top: 20px solid #ed7829;

        float: left;

        background-color: #fff;

        position: relative;

        z-index: 1;

        cursor: pointer;

    }

    .v20190507_index_xz .nrk .nr .pic {

        width: 160px;

        height: 160px;

        margin: 10px;

        float: left;

    }

    .v20190507_index_xz .nrk .nr .dpic {

        position: absolute;

        z-index: 2;

        margin-top: 38px;

        margin-left: 146px;

    }

    .v20190507_index_xz .nrk .nr .lx {

        width: 270px;

        height: 60px;

        font-size: 60px;

        color: #ed7829;

        font-weight: 400;

        line-height: 60px;

        margin-left: 20px;

        margin-right: 30px;

        float: left;

    }

    .v20190507_index_xz .nrk .nr .bt {

        width: 270px;

        font-size: 18px;

        line-height: 18px;

        color: #333;

        font-weight: 400;

        margin-top: 10px;

        margin-left: 20px;

        margin-right: 30px;

        float: left;

    }

    .v20190507_index_xz .nrk .nr .jj {

        width: 270px;

        font-size: 14px;

        line-height: 20px;

        color: #333;

        margin-top: 15px;

        margin-left: 20px;

        margin-right: 30px;

        float: left;

    }

    .v20190507_index_cgal .nrk {

        width: 1580px;

        margin: 0 auto;

        padding-bottom: 90px;

    }

    .v20190507_index_cgal .nrk .swiper-slide {

        height: 389px;

    }

    .v20190507_index_cgal .nrk .nr {

        width: 375px;

        margin: 10px;

        float: left;

        box-shadow: 0px 0px 10px 1px rgba(237, 120, 41, 0.6);

        position: relative;

        padding-bottom: 30px;

    }

    .v20190507_index_cgal .nrk .nr .pic {

        width: 355px;

        height: 198px;

        margin: 10px;

    }

    .v20190507_index_cgal .nrk .nr .an {

        width: 58px;

        height: 58px;

        top: 80px;

        left: 50%;

        margin-left: -29px;

        position: absolute;

        cursor: pointer;

    }

    .v20190507_index_cgal .nrk .nr .xm {

        padding-left: 40px;

        font-size: 18px;

        line-height: 18px;

        color: #333;

        font-weight: 400;

        padding-top: 10px;

    }

    .v20190507_index_cgal .nrk .nr .hx {

        width: 30px;

        height: 1px;

        background-color: #333;

        margin: 15px 40px;

    }

    .v20190507_index_cgal .nrk .nr .jj {

        padding-left: 40px;

        font-size: 14px;

        line-height: 14px;

        color: #333;

    }

    .v20190507_index_cgal .nrk .nr .jj .zy {

        margin-top: 10px;

    }

    .v20190507_index_cgal .nrk .nr .jj .yx {

        margin-top: 10px;

    }

    .v20190507_index_cgal .nrk .fydh {

        width: 100%;

        min-width: 1560px;

        position: relative;

        margin-top: 40px;

    }

    .v20190507_index_cgal .nrk .fydh .swiper-pagination {

        position: absolute;

        z-index: 20;

        bottom: 0px;

        width: 100%;

        text-align: center;

    }

    .v20190507_index_cgal .nrk .fydh .swiper-pagination .swiper-pagination-switch {

        display: inline-block;

        width: 200px;

        height: 4px;

        border-radius: 0px;

        background: #999;

        margin: 0 5px;

        opacity: 0.8;

        cursor: pointer;

    }

    .v20190507_index_cgal .nrk .fydh .swiper-pagination .swiper-active-switch {

        background: #ed7523;

    }

    .v20190507_index_kcfw {

        width: 100%;

        height: 1069px;

        position: relative;

        overflow: hidden;

    }

    .v20190507_index_kcfw > .pic {

        position: absolute;

        width: 100%;

        height: 1069px;

        z-index: 1;

    }

    .v20190507_index_kcfw > .pic img {

        width: 100%;

        height: 1069px;

    }

    .v20190507_index_kcfw .dbt {

        position: relative;

        z-index: 2;

    }

    .v20190507_index_kcfw .dbt .ebt {

        color: #fff;

    }

    .v20190507_index_kcfw .dbt .hx {

        background-color: #fff;

    }

    .v20190507_index_kcfw .dbt .zbt {

        color: #fff;

    }

    .v20190507_index_kcfw .nrk {

        width: 100%;

        margin: 0 auto;

        position: relative;

        z-index: 2;

    }

    .v20190507_index_kcfw .nrk .wz .bt .vmpic {

        width: 12px;

        height: 30px;

        display: inline-block;

        margin-right: 5px;

    }

    .v20190507_index_kcfw .nrk .wz .jj .vmpic {

        width: 6px;

        height: 6px;

        display: inline-block;

        margin-right: 7px;

    }

    .v20190507_index_kcfw .nrk .wz .vmpic img {

        display: inline-block;

        vertical-align: middle;

    }

    .v20190507_index_kcfw .nrk .pic {

        width: 490px;

        height: 310px;

        margin: 20px;

    }

    .v20190507_index_kcfw .nrk .wz {

        width: 240px;

    }

    .v20190507_index_kcfw .nrk .wz .bt {

        margin-top: 80px;

        font-size: 18px;

        line-height: 30px;

        color: #ed7829;

        font-weight: 400;

        padding-bottom: 10px;

    }

    .v20190507_index_kcfw .nrk .wz .jj {

        font-size: 14px;

        line-height: 14px;

        color: #333;

        margin-top: 10px;

        font-weight: 400;

    }

    .v20190507_index_kcfw .nrk .wz .an {

        font-size: 14px;

        line-height: 28px;

        color: #ed7829;

        margin-top: 30px;

        padding: 0 20px;

        font-weight: 400;

        display: inline-block;

        border: 1px solid #ed7829;

        border-top-left-radius: 28px;

        border-bottom-left-radius: 28px;

        border-top-right-radius: 28px;

        border-bottom-right-radius: 28px;

        -moz-border-radius-topleft: 28px;

        -moz-border-radius-bottomleft: 28px;

        -moz-border-radius-topright: 28px;

        -moz-border-radius-bottomright: 28px;

        -webkit-border-top-left-radius: 28px;

        -webkit-border-bottom-left-radius: 28px;

        -webkit-border-top-right-radius: 28px;

        -webkit-border-bottom-right-radius: 28px;

        cursor: pointer;

    }

    .v20190507_index_kcfw .nrk .wz .an:hover {

        background-color: #ed7829;

        color: #fff;

    }

    .v20190507_index_kcfw .nrk .nrz {

        width: 50%;

        min-width: 600px;

        height: 350px;

        background-color: #fff;

        float: left;

        margin-left: -10px;

        margin-top: 10px;

        margin-bottom: 10px;

    }

    .v20190507_index_kcfw .nrk .nrz .wz {

        padding-left: 50px;

        float: right;

    }

    .v20190507_index_kcfw .nrk .nrz .pic {

        float: right;

    }

    .v20190507_index_kcfw .nrk .nrr {

        width: 50%;

        min-width: 600px;

        height: 350px;

        background-color: #fff;

        float: right;

        margin-right: -10px;

        margin-top: 10px;

        margin-bottom: 10px;

    }

    .v20190507_index_kcfw .nrk .nrr .wz {

        margin-left: 30px;

        float: left;

    }

    .v20190507_index_kcfw .nrk .nrr .pic {

        float: left;

    }

    .v20190507_index_kcfw .nrk .nrz:hover {

        margin-top: 0;

        margin-bottom: 20px;

    }

    .v20190507_index_kcfw .nrk .nrr:hover {

        margin-top: 0;

        margin-bottom: 20px;

    }

    .v20190507_index_kcfw .zxpic {

        width: 226px;

        height: 226px;

        position: absolute;

        z-index: 3;

        top: 505px;

        left: 50%;

        margin-left: -113px;

    }

    .v20190507_index_zsglm .nrk {

        width: 1560px;

        margin: 0 auto;

        padding-bottom: 100px;

    }

    .v20190507_index_zsglm .nrk .tpk {

        width: 700px;

        float: left;

    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic {

        width: 700px;

        height: 300px;

        float: left;

        position: relative;

        cursor: pointer;


    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 {

        position: absolute;

        width: 150px;

        height: 120px;

        top: 170px;

        right: 10px;

        background-color: #313131;

        color: #fff;

    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 .y {

        width: 38px;

        height: 38px;

        border-top-left-radius: 38px;

        border-bottom-left-radius: 38px;

        border-top-right-radius: 38px;

        border-bottom-right-radius: 38px;

        -moz-border-radius-topleft: 38px;

        -moz-border-radius-bottomleft: 38px;

        -moz-border-radius-topright: 38px;

        -moz-border-radius-bottomright: 38px;

        -webkit-border-top-left-radius: 38px;

        -webkit-border-bottom-left-radius: 38px;

        -webkit-border-top-right-radius: 38px;

        -webkit-border-bottom-right-radius: 38px;

        border: 1px solid #fff;

        margin: 20px auto 0;

        position: relative;

    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 .s {

        width: 38px;

        height: 38px;

        font-size: 36px;

        line-height: 38px;

        font-weight: 400;

        position: absolute;

        top: 10px;

        right: 5px;

        text-align: center;

    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 .z {

        font-size: 20px;

        line-height: 20px;

        font-weight: 400;

        text-align: center;

        margin-top: 20px;

    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic:hover .tp1 .s {

        top: 0;

        right: 0;

    }

    .v20190507_index_zsglm .nrk .tpk .tp1pic:hover .tp1 {

        background-color: #ed7829;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic {

        width: 350px;

        height: 400px;

        float: left;

        margin-top: 10px;

        position: relative;

        cursor: pointer;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 {

        position: absolute;

        width: 150px;

        height: 120px;

        top: 10px;

        right: 10px;

        background-color: #313131;

        color: #fff;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 .y {

        width: 38px;

        height: 38px;

        border-top-left-radius: 38px;

        border-bottom-left-radius: 38px;

        border-top-right-radius: 38px;

        border-bottom-right-radius: 38px;

        -moz-border-radius-topleft: 38px;

        -moz-border-radius-bottomleft: 38px;

        -moz-border-radius-topright: 38px;

        -moz-border-radius-bottomright: 38px;

        -webkit-border-top-left-radius: 38px;

        -webkit-border-bottom-left-radius: 38px;

        -webkit-border-top-right-radius: 38px;

        -webkit-border-bottom-right-radius: 38px;

        border: 1px solid #fff;

        margin: 20px auto 0;

        position: relative;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 .s {

        width: 38px;

        height: 38px;

        font-size: 36px;

        line-height: 38px;

        font-weight: 400;

        position: absolute;

        top: 10px;

        right: 5px;

        text-align: center;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 .z {

        font-size: 20px;

        line-height: 20px;

        font-weight: 400;

        text-align: center;

        margin-top: 20px;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic:hover .tp2 .s {

        top: 0;

        right: 0;

    }

    .v20190507_index_zsglm .nrk .tpk .tp2pic:hover .tp2 {

        background-color: #ed7829;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic {

        width: 340px;

        height: 400px;

        float: left;

        margin-top: 10px;

        margin-left: 10px;

        position: relative;

        cursor: pointer;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 {

        position: absolute;

        width: 150px;

        height: 120px;

        bottom: 10px;

        left: 10px;

        background-color: #313131;

        color: #fff;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 .y {

        width: 38px;

        height: 38px;

        border-top-left-radius: 38px;

        border-bottom-left-radius: 38px;

        border-top-right-radius: 38px;

        border-bottom-right-radius: 38px;

        -moz-border-radius-topleft: 38px;

        -moz-border-radius-bottomleft: 38px;

        -moz-border-radius-topright: 38px;

        -moz-border-radius-bottomright: 38px;

        -webkit-border-top-left-radius: 38px;

        -webkit-border-bottom-left-radius: 38px;

        -webkit-border-top-right-radius: 38px;

        -webkit-border-bottom-right-radius: 38px;

        border: 1px solid #fff;

        margin: 20px auto 0;

        position: relative;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 .s {

        width: 38px;

        height: 38px;

        font-size: 36px;

        line-height: 38px;

        font-weight: 400;

        position: absolute;

        top: 10px;

        right: 5px;

        text-align: center;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 .z {

        font-size: 20px;

        line-height: 20px;

        font-weight: 400;

        text-align: center;

        margin-top: 20px;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic:hover .tp3 .s {

        top: 0;

        right: 0;

    }

    .v20190507_index_zsglm .nrk .tpk .tp3pic:hover .tp3 {

        background-color: #ed7829;

    }

    .v20190507_index_zsglm .nrk .twk {

        width: 850px;

        float: left;

        margin-left: 10px;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic {

        width: 290px;

        height: 710px;

        float: left;

        position: relative;

        cursor: pointer;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 {

        position: absolute;

        width: 150px;

        height: 120px;

        top: 310px;

        left: 10px;

        background-color: #313131;

        color: #fff;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 .y {

        width: 38px;

        height: 38px;

        border-top-left-radius: 38px;

        border-bottom-left-radius: 38px;

        border-top-right-radius: 38px;

        border-bottom-right-radius: 38px;

        -moz-border-radius-topleft: 38px;

        -moz-border-radius-bottomleft: 38px;

        -moz-border-radius-topright: 38px;

        -moz-border-radius-bottomright: 38px;

        -webkit-border-top-left-radius: 38px;

        -webkit-border-bottom-left-radius: 38px;

        -webkit-border-top-right-radius: 38px;

        -webkit-border-bottom-right-radius: 38px;

        border: 1px solid #fff;

        margin: 20px auto 0;

        position: relative;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 .s {

        width: 38px;

        height: 38px;

        font-size: 36px;

        line-height: 38px;

        font-weight: 400;

        position: absolute;

        top: 10px;

        right: 5px;

        text-align: center;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 .z {

        font-size: 20px;

        line-height: 20px;

        font-weight: 400;

        text-align: center;

        margin-top: 20px;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic:hover .tp4 .s {

        top: 0;

        right: 0;

    }

    .v20190507_index_zsglm .nrk .twk .tp4pic:hover .tp4 {

        background-color: #ed7829;

    }

    .v20190507_index_zsglm .nrk .twk .wz {

        width: 550px;

        height: 710px;

        float: left;

        margin-left: 10px;

        background-color: #ed7829;

        color: #fff;

        padding: 0 50px;

    }

    .v20190507_index_zsglm .nrk .twk .wz .bt {

        font-size: 40px;

        line-height: 50px;

        font-weight: 400;

        margin-top: 70px;

        padding-bottom: 20px;

        border-bottom: 4px solid #fff;

    }

    .v20190507_index_zsglm .nrk .twk .wz .jjk {

        padding-bottom: 25px;

    }

    .v20190507_index_zsglm .nrk .twk .wz .jjk .xbt {

        font-size: 16px;

        line-height: 16px;

        font-weight: 400;

        margin-top: 30px;

    }

    .v20190507_index_zsglm .nrk .twk .wz .jjk .jj {

        font-size: 14px;

        line-height: 19px;

        margin-top: 18px;

    }

    .v20190507_index_zsglm .nrk .twk .wz .an {

        font-size: 14px;

        line-height: 28px;

        color: #fff;

        padding: 0 20px;

        font-weight: 400;

        display: inline-block;

        border: 1px solid #fff;

        border-top-left-radius: 28px;

        border-bottom-left-radius: 28px;

        border-top-right-radius: 28px;

        border-bottom-right-radius: 28px;

        -moz-border-radius-topleft: 28px;

        -moz-border-radius-bottomleft: 28px;

        -moz-border-radius-topright: 28px;

        -moz-border-radius-bottomright: 28px;

        -webkit-border-top-left-radius: 28px;

        -webkit-border-bottom-left-radius: 28px;

        -webkit-border-top-right-radius: 28px;

        -webkit-border-bottom-right-radius: 28px;

        cursor: pointer;

    }

    .v20190507_index_zsglm .nrk .twk .wz .an:hover {

        color: #ed7829;

        background-color: #fff;

    }

    .v20190507_index_dstd {

        width: 100%;

        height: 1168px;

        position: relative;

    }

    .v20190507_index_dstd > .pic {

        position: absolute;

        width: 100%;

        height: 1168px;

        z-index: 1;

    }

    .v20190507_index_dstd > .pic img {

        width: 100%;

        height: 1168px;

    }

    .v20190507_index_dstd .dbt {

        position: relative;

        z-index: 2;

    }

    .v20190507_index_dstd .dbt .ebt {

        color: #fff;

    }

    .v20190507_index_dstd .dbt .hx {

        background-color: #fff;

    }

    .v20190507_index_dstd .dbt .zbt {

        color: #fff;

    }

    .v20190507_index_dstd .zsgk {

        width: 1560px;

        height: 338px;

        margin: 0 auto;

        background-color: #fff;

        position: relative;

        z-index: 2;

    }

    .v20190507_index_dstd .zsgk .fy_zf {

        width: 22px;

        height: 338px;

        float: left;

        margin-left: 30px;

    }

    .v20190507_index_dstd .zsgk .fy_zf img {

        margin-top: 129px;

    }

    .v20190507_index_dstd .zsgk .fy_yf {

        width: 22px;

        height: 338px;

        float: left;

        margin-right: 30px;

    }

    .v20190507_index_dstd .zsgk .fy_yf img {

        margin-top: 129px;

    }

    .v20190507_index_dstd .zsgk .swiper-container {

        width: 1350px;

        float: left;

        margin: 0 53px;

    }

    .v20190507_index_dstd .zsgk .nr {

        width: 270px;

        float: left;

        text-align: center;

        padding-top: 30px;

    }

    .v20190507_index_dstd .zsgk .nr .pic {

        width: 180px;

        height: 180px;

        margin: 0 auto;

        position: relative;

    }

    .v20190507_index_dstd .zsgk .nr .pic img {

        width: 180px;

        height: 180px;

        border-top-left-radius: 180px;

        border-bottom-left-radius: 180px;

        border-top-right-radius: 180px;

        border-bottom-right-radius: 180px;

        -moz-border-radius-topleft: 180px;

        -moz-border-radius-bottomleft: 180px;

        -moz-border-radius-topright: 180px;

        -moz-border-radius-bottomright: 180px;

        -webkit-border-top-left-radius: 180px;

        -webkit-border-bottom-left-radius: 180px;

        -webkit-border-top-right-radius: 180px;

        -webkit-border-bottom-right-radius: 180px;

    }

    .v20190507_index_dstd .zsgk .nr .pic .tw {

        display: none;

        width: 180px;

        height: 180px;

        position: absolute;

        z-index: 10;

        top: 0;

        background-color: rgba(48, 45, 44, 0.7);

        border-top-left-radius: 180px;

        border-bottom-left-radius: 180px;

        border-top-right-radius: 180px;

        border-bottom-right-radius: 180px;

        -moz-border-radius-topleft: 180px;

        -moz-border-radius-bottomleft: 180px;

        -moz-border-radius-topright: 180px;

        -moz-border-radius-bottomright: 180px;

        -webkit-border-top-left-radius: 180px;

        -webkit-border-bottom-left-radius: 180px;

        -webkit-border-top-right-radius: 180px;

        -webkit-border-bottom-right-radius: 180px;

        font-size: 16px;

        text-align: center;

        line-height: 180px;

        color: #fff;

        cursor: pointer;

    }

    .v20190507_index_dstd .zsgk .nr:hover .pic .tw {

        display: block;

    }

    .v20190507_index_dstd .zsgk .nr .xm {

        font-size: 28px;

        line-height: 28px;

        color: #323431;

        margin-top: 20px;

    }

    .v20190507_index_dstd .zsgk .nr .yx {

        font-size: 18px;

        line-height: 18px;

        color: #323431;

        margin-top: 10px;

    }

    .v20190507_index_dstd .zsgk .nr .zy {

        font-size: 14px;

        line-height: 14px;

        color: #323431;

        margin-top: 10px;

    }

    .v20190507_index_dstd .nrk {

        width: 1580px;

        margin: 30px auto 0;

        position: relative;

        z-index: 2;

    }

    .v20190507_index_dstd .nrk .nr {

        width: 296px;

        height: 431px;

        margin: 10px;

        float: left;

        position: relative;

        background-color: #fff;

    }

    .v20190507_index_dstd .nrk .nr .pic {

        width: 100%;

        overflow: hidden;

    }

    .v20190507_index_dstd .nrk .nr .pic a {

        display: block;

    }

    .v20190507_index_dstd .nrk .nr .pic img {

        width: 100%;

        transition: all 1s;

        -moz-transition: all 1s; /* Firefox 4 */

        -webkit-transition: all 1s; /* Safari and Chrome */

        -o-transition: all 1s; /* Opera */

    }

    .v20190507_index_dstd .nrk .nr:hover .pic img {

        transform: scale(1.1);

        -moz-transition: scale(1.1); /* Firefox 4 */

        -webkit-transition: scale(1.1); /* Safari and Chrome */

        -o-transition: scale(1.1); /* Opera */

    }


    .v20190507_index_dstd .nrk .nr .pick {

        position: relative;

    }

    .v20190507_index_dstd .nrk .nr .pick:hover {

        /*opacity:0.6; */

    }

    .v20190507_index_dstd .nrk .nr .pick:after, .v20190507_index_dstd .nrk .nr .pick:before, .v20190507_index_dstd .nrk .nr .pick .bi:after, .v20190507_index_dstd .nrk .nr .pick .bi:before {

        background-color: rgba(255, 255, 255, 0.9);

        content: "";

        display: block;

        position: absolute;

        z-index: 10;

        transition: all 0.3s ease 0s;

        -webkit-transition: all 0.3s ease 0s;

        -moz-transition: all 0.3s ease 0s;

        -o-transition: all 0.3s ease 0s;

        -ms-transition: all 0.3s ease 0s;

    }

    .v20190507_index_dstd .nrk .nr .pick:after {

        height: 6px;

        left: -0;

        top: 0;

        width: 0;

    }

    .v20190507_index_dstd .nrk .nr .pick:before {

        bottom: 0;

        height: 6px;

        right: 0;

        width: 0;

    }

    .v20190507_index_dstd .nrk .nr .pick .bi:after {

        bottom: 0;

        height: 0;

        left: 0;

        width: 6px;

    }

    .v20190507_index_dstd .nrk .nr .pick .bi:before {

        height: 0;

        right: 0;

        top: 0;

        width: 6px;

    }

    .v20190507_index_dstd .nrk .nr .pick:hover:after, .v20190507_index_dstd .nrk .nr .pick:hover:before {

        width: 100%;

    }

    .v20190507_index_dstd .nrk .nr .pick:hover .bi:after, .v20190507_index_dstd .nrk .nr .pick:hover .bi:before {

        height: 100%;

    }


    .v20190507_index_dstd .nrk .nr .btan {

        height: 37px;

        margin-top: 23px;

        margin-left: 25px;

        margin-right: 23px;

    }

    .v20190507_index_dstd .nrk .nr .btan .bt {

        font-size: 28px;

        line-height: 37px;

        color: #333333;

        float: left;

    }

    .v20190507_index_dstd .nrk .nr .btan .bt a {

        font-size: 28px;

        line-height: 37px;

        color: #333333;

    }

    .v20190507_index_dstd .nrk .nr .btan .an {

        font-size: 14px;

        line-height: 35px;

        color: #666666;

        border: 1px solid #797979;

        width: 79px;

        height: 35px;

        text-align: center;

        float: right;

        cursor: pointer;

    }

    .v20190507_index_dstd .nrk .nr .btan .an:hover {

        color: #fff;

        background-color: #797979;

    }

    .v20190507_index_dstd .nrk .nr .yx {

        font-size: 16px;

        line-height: 16px;

        color: #999999;

        margin-left: 25px;

        margin-top: 14px;

    }

    .v20190507_index_dstd .nrk .nr .zy {

        font-size: 16px;

        line-height: 16px;

        color: #999999;

        margin-left: 25px;

        margin-top: 14px;

    }

    .v20190507_index_rmzy .nrk {

        width: 1580px;

        height: 340px;

        margin: 0 auto;

    }

    .v20190507_index_rmzy .nrk .nr {

        width: 375px;

        margin: 0 10px;

        float: left;

        padding-bottom: 20px;

        box-shadow: 0px 0px 10px 1px rgba(237, 120, 41, 0.6);

        position: relative;

    }

    .v20190507_index_rmzy .nrk .nr .pic {

        width: 355px;

        height: 194px;

        margin: 10px 10px 0;

    }

    .v20190507_index_rmzy .nrk .nr .bt {

        font-size: 18px;

        line-height: 18px;

        color: #333;

        font-weight: 400;

        margin-top: 20px;

        margin-left: 40px;

    }

    .v20190507_index_rmzy .nrk .nr .hx {

        width: 30px;

        height: 1px;

        background-color: #333;

        margin: 15px 0 15px 40px;

    }

    .v20190507_index_rmzy .nrk .nr .sz {

        font-size: 14px;

        line-height: 14px;

        color: #333;

        margin-left: 40px;

        margin-bottom: 10px;

    }

    .v20190507_index_rmzy .nrk .nr .an {

        width: 96px;

        height: 28px;

        font-size: 14px;

        line-height: 28px;

        color: #fff;

        font-weight: 400;

        background-color: #ed7829;

        position: absolute;

        right: 40px;

        bottom: 30px;

        text-align: center;

        border-top-left-radius: 5px;

        border-bottom-left-radius: 5px;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

        -moz-border-radius-topleft: 5px;

        -moz-border-radius-bottomleft: 5px;

        -moz-border-radius-topright: 5px;

        -moz-border-radius-bottomright: 5px;

        -webkit-border-top-left-radius: 5px;

        -webkit-border-bottom-left-radius: 5px;

        -webkit-border-top-right-radius: 5px;

        -webkit-border-bottom-right-radius: 5px;

    }

    .v20190507_index_rmzy .nrk .nr .an:hover {

        color: #ed7829;

        background-color: #fff;

        cursor: pointer;

        border: 1px solid #ed7829;

    }

    .v20190507_index_gshj .nrwk {

        width: 100%;

        height: 800px;

        background-color: #313131;

    }

    .v20190507_index_gshj .nrwk .dhk {

        width: 1560px;

        margin: 0 auto;

        text-align: center;

        padding: 50px 0;

    }

    .v20190507_index_gshj .nrwk .dhk .dh {

        font-size: 20px;

        line-height: 20px;

        color: #999;

        border-left: 1px solid #666;

        padding: 0 40px;

        display: inline-block;

        cursor: pointer;

    }

    .v20190507_index_gshj .nrwk .dhk .xz {

        color: #ed7829;

    }

    .v20190507_index_gshj .nrwk .dhk .dh:first-child {

        border: 0;

    }

    .v20190507_index_gshj .nrwk .nrk {

        width: 1560px;

        margin: 0 auto;

    }

    .v20190507_index_gshj .nrwk .nrk .pic {

        width: 320px;

        height: 180px;

        float: left;

        border: 2px solid #fff;

        margin-left: 20px;

        margin-bottom: 20px;

    }

    .v20190507_index_gshj .nrwk .nrk .pic img {

        width: 316px;

        height: 176px;

    }

    .v20190507_index_gshj .nrwk .nrk .pic.pd {

        width: 880px;

        height: 580px;

        float: left;

        margin: 0;

    }

    .v20190507_index_gshj .nrwk .nrk .pic.pd img {

        width: 876px;

        height: 576px;

    }

    .v20190507_index_yszx .nrk {

        width: 1560px;

        margin: 0 auto;

        padding-bottom: 100px;

    }

    .v20190507_index_yszx .nrk .hdk {

        width: 380px;

        float: left;

    }

    .v20190507_index_yszx .nrk .swiper-container {

        box-shadow: 0px 0px 10px 1px rgba(237, 120, 41, 0.6);

    }

    .v20190507_index_yszx .nrk .hdk .nr {

        font-size: 14px;

        line-height: 21px;

        color: #333;

        padding-bottom: 30px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .pic {

        width: 322px;

        height: 200px;

        margin: 30px 30px 0 30px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .pic img {

        width: 322px;

        height: 200px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .bt {

        font-size: 16px;

        line-height: 25px;

        color: #333;

        margin-top: 20px;

        margin-bottom: 15px;

        font-weight: 400;

        padding: 0 30px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .bt a {

        color: #333;

    }

    .v20190507_index_yszx .nrk .hdk .nr .lx {

        padding-left: 30px;

        width: 190px;

        float: left;

    }

    .v20190507_index_yszx .nrk .hdk .nr .lx img {

        width: 21px;

        height: 21px;

        display: inline-block;

        vertical-align: middle;

        margin-right: 15px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .fy {

        padding-right: 30px;

        width: 190px;

        float: left;

    }

    .v20190507_index_yszx .nrk .hdk .nr .fy img {

        width: 21px;

        height: 21px;

        display: inline-block;

        vertical-align: middle;

        margin-right: 15px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .rq {

        padding: 0 30px;

        margin-top: 15px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .rq img {

        width: 21px;

        height: 21px;

        display: inline-block;

        vertical-align: middle;

        margin-right: 15px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .dz {

        padding: 0 30px;

        margin-top: 10px;

    }

    .v20190507_index_yszx .nrk .hdk .nr .dz img {

        width: 21px;

        height: 21px;

        display: inline-block;

        vertical-align: middle;

        margin-right: 15px;

    }

    .v20190507_index_yszx .nrk .hdk .fydh {

        width: 100%;

        position: relative;

        margin-top: 30px;

    }

    .v20190507_index_yszx .nrk .hdk .fydh .swiper-pagination {

        position: absolute;

        z-index: 20;

        bottom: 0px;

        width: 100%;

        text-align: center;

    }

    .v20190507_index_yszx .nrk .hdk .fydh .swiper-pagination .swiper-pagination-switch {

        display: inline-block;

        width: 50px;

        height: 4px;

        border-radius: 0px;

        background: #999;

        margin: 0 5px;

        opacity: 0.8;

        cursor: pointer;

    }

    .v20190507_index_yszx .nrk .hdk .fydh .swiper-pagination .swiper-active-switch {

        background: #ed7523;

    }

    .v20190507_index_yszx .nrk .wzk {

        width: 1126px;

        margin-left: 50px;

        float: left;

    }

    .v20190507_index_yszx .nrk .wzk .dtw {

        width: 1126px;

        height: 226px;

        background: #ed7523;

        color: #fff;

    }

    .v20190507_index_yszx .nrk .wzk .dtw .pic {

        width: 340px;

        height: 226px;

        padding: 20px;

        float: left;

    }

    .v20190507_index_yszx .nrk .wzk .dtw .bt {

        width: 776px;

        padding-top: 50px;

        font-size: 18px;

        line-height: 18px;

        font-weight: 400;

        margin-left: 10px;

        float: left;

    }

    .v20190507_index_yszx .nrk .wzk .dtw .bt a {

        color: #fff;

    }

    .v20190507_index_yszx .nrk .wzk .dtw .jj {

        width: 776px;

        padding-top: 14px;

        font-size: 14px;

        line-height: 19px;

        margin-left: 10px;

        float: left;

        display: -webkit-box;

        -webkit-box-orient: vertical;

        -webkit-line-clamp: 3;

        overflow: hidden;

    }

    .v20190507_index_yszx .nrk .wzk .dtw .rq {

        width: 776px;

        padding-top: 30px;

        font-size: 12px;

        line-height: 12px;

        margin-left: 10px;

        float: left;

    }

    .v20190507_index_yszx .nrk .wzk .listk {

        width: 1126px;

        overflow: hidden;

    }

    .v20190507_index_yszx .nrk .wzk .listk .dhk {

        text-align: center;

        margin-top: 40px;

        padding-bottom: 20px;

        border-bottom: 1px solid #999;

    }

    .v20190507_index_yszx .nrk .wzk .listk .dhk .dh {

        font-size: 18px;

        line-height: 18px;

        font-weight: 400;

        color: #333;

        display: inline-block;

        padding: 0 30px;

        cursor: pointer;

    }

    .v20190507_index_yszx .nrk .wzk .listk .dhk .dh.xz {

        color: #ed7829;

    }

    .v20190507_index_yszx .nrk .wzk .listk ul {

        width: 1190px;

        padding-top: 20px;

    }

    .v20190507_index_yszx .nrk .wzk .listk li {

        width: 530px;

        float: left;

        line-height: 20px;

        margin-right: 65px;

        display: block;

    }

    .v20190507_index_yszx .nrk .wzk .listk li a {

        width: 455px;

        font-size: 14px;

        line-height: 20px;

        font-weight: 400;

        color: #333;

        overflow: hidden;

        text-overflow: ellipsis;

        white-space: nowrap;

    }

    .v20190507_index_yszx .nrk .wzk .listk li span {

        width: 72px;

        font-size: 14px;

        line-height: 20px;

        font-weight: 400;

        color: #999;

    }

    .v20190507_index_hzhb {

        width: 100%;

        height: 995px;

        position: relative;

    }

    .v20190507_index_hzhb > .pic {

        position: absolute;

        width: 100%;

        height: 450px;

        z-index: 1;

    }

    .v20190507_index_hzhb > .pic img {

        width: 100%;

        height: 450px;

    }

    .v20190507_index_hzhb .dbt {

        position: relative;

        z-index: 2;

    }

    .v20190507_index_hzhb .dbt .ebt {

        color: #fff;

    }

    .v20190507_index_hzhb .dbt .hx {

        background-color: #fff;

    }

    .v20190507_index_hzhb .dbt .zbt {

        color: #fff;

    }

    .v20190507_index_hzhb .nrk {

        width: 1560px;

        height: 620px;

        margin: 0 auto;

        background-color: #eee;

        box-shadow: 0px 0px 10px 1px rgba(237, 120, 41, 0.6);

        position: relative;

        z-index: 2;

        padding: 55px 80px 75px;

    }

    .v20190507_index_hzhb .nrk .nr {

        width: 280px;

        float: left;

        text-align: center;

        margin: 25px 0;

    }

    .v20190507_index_hzhb .nrk .nr .pic {

        width: 166px;

        height: 166px;

        margin: 0 auto;

    }

    .v20190507_index_hzhb .nrk .nr .pic:hover {

        border-radius: 100%;

        /*box-shadow: 3px 3px 5px 0px rgba(153,153,153,0.8);*/

        box-shadow: 0px 0px 10px 3px rgba(153, 153, 153, 0.8);

    }

    .v20190507_index_hzhb .nrk .nr .pic img {

        width: 166px;

        height: 166px;

        border-radius: 100%;

    }

    .v20190507_index_hzhb .nrk .nr .bt {

        font-size: 18px;

        line-height: 18px;

        font-weight: 400;

        color: #333;

        margin-top: 20px;

    }

    @media (min-width: 0px) and (max-width: 1539px) {

        .ldxsj .ldxdh {

            line-height: 46px;

            height: 47px;

            font-size: 18px;

        }

        .ldxsj .ldxxq {

            width: 1200px;

            height: 100px;

            margin: 0 auto;

            padding: 10px 0;

        }

        .ldxsj .ldxxq .nr {

            float: left;

            text-align: center;

            width: 20%;

            border-right: 1px solid #d7d7d8;

            padding: 10px 0;

        }

        .ldxsj .ldxxq .nr .sz {

            font-size: 20px;

            color: #EE782A;

            line-height: 40px;

        }

        .ldxsj .ldxxq .nr .mc {

            font-size: 12px;

            line-height: 24px;

            color: #333333;

            margin-top: 0px;

        }

        .v20190507_index .dbt {

            width: 1200px;

            padding: 50px 0 0;

        }

        .v20190507_index .dbt .pic {

            margin-top: 15px;

        }

        .v20190507_index .dbt .ebt {

            padding-left: 25px;

            font-size: 36px;

            line-height: 36px;

        }

        .v20190507_index .dbt .hx {

            width: 22px;

            margin: 15px 0 15px 25px;

        }

        .v20190507_index .dbt .zbt {

            padding-left: 25px;

            font-size: 19px;

            line-height: 19px;

        }


        .v20190507_index_xz {

            height: 546px;

        }

        .v20190507_index_xz > .pic {

            height: 546px;

        }

        .v20190507_index_xz > .pic img {

            height: 546px;

        }

        .v20190507_index_xz .nrk {

            width: 1215px;

            height: 350px;

            margin-top: 15px;

        }

        .v20190507_index_xz .nrk .nr {

            margin: 15px 7.5px;

            width: 390px;

            height: 145px;

            border-top: 15px solid #ed7829;

        }

        .v20190507_index_xz .nrk .nr .pic {

            width: 110px;

            height: 110px;

        }

        .v20190507_index_xz .nrk .nr .dpic {

            margin-top: 20px;

            margin-left: 100px;

        }

        .v20190507_index_xz .nrk .nr .dpic img {

            width: 35px;

            height: 5px;

        }

        .v20190507_index_xz .nrk .nr .lx {

            width: 230px;

            height: 45px;

            font-size: 30px;

            line-height: 45px;

            margin-left: 10px;

            margin-right: 20px;

        }

        .v20190507_index_xz .nrk .nr .bt {

            width: 230px;

            font-size: 14px;

            line-height: 14px;

            margin-top: 0px;

            margin-left: 10px;

            margin-right: 20px;

        }

        .v20190507_index_xz .nrk .nr .jj {

            width: 230px;

            font-size: 12px;

            line-height: 15px;

            margin-top: 13.5px;

            margin-left: 10px;

            margin-right: 20px;

        }

        .v20190507_index_cgal .nrk {

            width: 1220px;

            margin: 30px auto 0;

            padding-bottom: 60px;

        }

        .v20190507_index_cgal .nrk .swiper-slide {

            height: 299px;

        }

        .v20190507_index_cgal .nrk .nr {

            width: 285px;

            padding-bottom: 20px;

        }

        .v20190507_index_cgal .nrk .nr .pic {

            width: 265px;

            height: 149px;

            margin: 10px;

        }

        .v20190507_index_cgal .nrk .nr .an {

            width: 45px;

            height: 45px;

            top: 62px;

            left: 50%;

            margin-left: -22.5px;

        }

        .v20190507_index_cgal .nrk .nr .an img {

            width: 45px;

            height: 45px;

        }

        .v20190507_index_cgal .nrk .nr .xm {

            padding-left: 30px;

            font-size: 14px;

            line-height: 14px;

            padding-top: 5px;

        }

        .v20190507_index_cgal .nrk .nr .hx {

            width: 22px;

            margin: 10px 30px;

        }

        .v20190507_index_cgal .nrk .nr .jj {

            padding-left: 30px;

            font-size: 12px;

            line-height: 12px;

        }

        .v20190507_index_cgal .nrk .nr .jj .zy {

            margin-top: 7px;

        }

        .v20190507_index_cgal .nrk .nr .jj .yx {

            margin-top: 7px;

        }

        .v20190507_index_cgal .nrk .fydh {

            min-width: 1200px;

        }

        .v20190507_index_cgal .nrk .fydh .swiper-pagination .swiper-pagination-switch {

            width: 150px;

            height: 3px;

        }

        .v20190507_index_kcfw {

            height: 770px;

        }

        .v20190507_index_kcfw > .pic {

            height: 770px;

        }

        .v20190507_index_kcfw > .pic img {

            height: 770px;

        }

        .v20190507_index_kcfw .nrk {

            margin: 20px auto 0;

        }

        .v20190507_index_kcfw .nrk .wz .bt .vmpic {

            width: 8px;

            height: 22px;

            margin-right: 4px;

        }

        .v20190507_index_kcfw .nrk .wz .bt .vmpic img {

            width: 8px;

            height: 22px;

        }

        .v20190507_index_kcfw .nrk .wz .jj .vmpic {

            width: 5px;

            height: 5px;

            margin-right: 5px;

        }

        .v20190507_index_kcfw .nrk .wz .jj .vmpic img {

            width: 5px;

            height: 5px;

        }

        .v20190507_index_kcfw .nrk .pic {

            width: 368px;

            height: 233px;

            margin: 15px;

        }

        .v20190507_index_kcfw .nrk .wz {

            width: 180px;

        }

        .v20190507_index_kcfw .nrk .wz .bt {

            margin-top: 57px;

            font-size: 14px;

            line-height: 22px;

        }

        .v20190507_index_kcfw .nrk .wz .jj {

            font-size: 12px;

            line-height: 12px;

            margin-top: 8px;

        }

        .v20190507_index_kcfw .nrk .wz .an {

            font-size: 12px;

            line-height: 20px;

            margin-top: 20px;

            padding: 0 20px;

            border-top-left-radius: 20px;

            border-bottom-left-radius: 20px;

            border-top-right-radius: 20px;

            border-bottom-right-radius: 20px;

            -moz-border-radius-topleft: 20px;

            -moz-border-radius-bottomleft: 20px;

            -moz-border-radius-topright: 20px;

            -moz-border-radius-bottomright: 20px;

            -webkit-border-top-left-radius: 20px;

            -webkit-border-bottom-left-radius: 20px;

            -webkit-border-top-right-radius: 20px;

            -webkit-border-bottom-right-radius: 20px;

        }

        .v20190507_index_kcfw .nrk .nrz {

            height: 263px;

        }

        .v20190507_index_kcfw .nrk .nrz .wz {

            padding-left: 20px;

        }

        .v20190507_index_kcfw .nrk .nrr {

            height: 263px;

        }

        .v20190507_index_kcfw .nrk .nrr .wz {

            margin-left: 15px;

        }

        .v20190507_index_kcfw .zxpic {

            width: 190px;

            height: 190px;

            top: 347.5px;

            margin-left: -95px;

        }

        .v20190507_index_zsglm .nrk {

            width: 1200px;

            padding-bottom: 60px;

            margin-top: 30px;

        }

        .v20190507_index_zsglm .nrk .tpk {

            width: 538px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp1pic {

            width: 538px;

            height: 237px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp1pic img {

            width: 538px;

            height: 237px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 {

            width: 112px;

            height: 96px;

            top: 133px;

            right: 8px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 .y {

            width: 32px;

            height: 32px;

            border-top-left-radius: 32px;

            border-bottom-left-radius: 32px;

            border-top-right-radius: 32px;

            border-bottom-right-radius: 32px;

            -moz-border-radius-topleft: 32px;

            -moz-border-radius-bottomleft: 32px;

            -moz-border-radius-topright: 32px;

            -moz-border-radius-bottomright: 32px;

            -webkit-border-top-left-radius: 32px;

            -webkit-border-bottom-left-radius: 32px;

            -webkit-border-top-right-radius: 32px;

            -webkit-border-bottom-right-radius: 32px;

            margin: 10px auto 0;

        }

        .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 .s {

            width: 32px;

            height: 32px;

            font-size: 29px;

            line-height: 32px;

            top: 8px;

            right: 4px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp1pic .tp1 .z {

            font-size: 16px;

            line-height: 16px;

            margin-top: 10px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp2pic {

            width: 261px;

            height: 317px;

            margin-top: 8px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp2pic img {

            width: 261px;

            height: 317px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 {

            width: 112px;

            height: 96px;

            top: 8px;

            right: 8px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 .y {

            width: 32px;

            height: 32px;

            border-top-left-radius: 32px;

            border-bottom-left-radius: 32px;

            border-top-right-radius: 32px;

            border-bottom-right-radius: 32px;

            -moz-border-radius-topleft: 32px;

            -moz-border-radius-bottomleft: 32px;

            -moz-border-radius-topright: 32px;

            -moz-border-radius-bottomright: 32px;

            -webkit-border-top-left-radius: 32px;

            -webkit-border-bottom-left-radius: 32px;

            -webkit-border-top-right-radius: 32px;

            -webkit-border-bottom-right-radius: 32px;

            margin: 10px auto 0;

        }

        .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 .s {

            width: 32px;

            height: 32px;

            font-size: 29px;

            line-height: 32px;

            top: 8px;

            right: 4px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp2pic .tp2 .z {

            font-size: 16px;

            line-height: 16px;

            margin-top: 10px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp3pic {

            width: 269px;

            height: 317px;

            margin-top: 8px;

            margin-left: 8px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp3pic img {

            width: 269px;

            height: 317px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 {

            position: absolute;

            width: 112px;

            height: 96px;

            bottom: 8px;

            left: 8px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 .y {

            width: 32px;

            height: 32px;

            border-top-left-radius: 32px;

            border-bottom-left-radius: 32px;

            border-top-right-radius: 32px;

            border-bottom-right-radius: 32px;

            -moz-border-radius-topleft: 32px;

            -moz-border-radius-bottomleft: 32px;

            -moz-border-radius-topright: 32px;

            -moz-border-radius-bottomright: 32px;

            -webkit-border-top-left-radius: 32px;

            -webkit-border-bottom-left-radius: 32px;

            -webkit-border-top-right-radius: 32px;

            -webkit-border-bottom-right-radius: 32px;

            margin: 10px auto 0;

        }

        .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 .s {

            width: 32px;

            height: 32px;

            font-size: 29px;

            line-height: 32px;

            top: 8px;

            right: 4px;

        }

        .v20190507_index_zsglm .nrk .tpk .tp3pic .tp3 .z {

            font-size: 16px;

            line-height: 16px;

            margin-top: 10px;

        }

        .v20190507_index_zsglm .nrk .twk {

            width: 654px;

            margin-left: 8px;

        }

        .v20190507_index_zsglm .nrk .twk .tp4pic {

            width: 224px;

            height: 562px;

        }

        .v20190507_index_zsglm .nrk .twk .tp4pic img {

            width: 224px;

            height: 562px;

        }

        .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 {

            position: absolute;

            width: 112px;

            height: 96px;

            top: 245px;

            left: 8px;

        }

        .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 .y {

            width: 32px;

            height: 32px;

            border-top-left-radius: 32px;

            border-bottom-left-radius: 32px;

            border-top-right-radius: 32px;

            border-bottom-right-radius: 32px;

            -moz-border-radius-topleft: 32px;

            -moz-border-radius-bottomleft: 32px;

            -moz-border-radius-topright: 32px;

            -moz-border-radius-bottomright: 32px;

            -webkit-border-top-left-radius: 32px;

            -webkit-border-bottom-left-radius: 32px;

            -webkit-border-top-right-radius: 32px;

            -webkit-border-bottom-right-radius: 32px;

            margin: 10px auto 0;

        }

        .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 .s {

            width: 32px;

            height: 32px;

            font-size: 29px;

            line-height: 32px;

            top: 8px;

            right: 4px;

        }

        .v20190507_index_zsglm .nrk .twk .tp4pic .tp4 .z {

            font-size: 16px;

            line-height: 16px;

            margin-top: 10px;

        }

        .v20190507_index_zsglm .nrk .twk .wz {

            width: 423px;

            height: 562px;

            margin-left: 7px;

            padding: 0 40px;

        }

        .v20190507_index_zsglm .nrk .twk .wz .bt {

            font-size: 32px;

            line-height: 46px;

            margin-top: 50px;

            padding-bottom: 25px;

            border-bottom: 3px solid #fff;

            margin-bottom: 30px;

        }

        .v20190507_index_zsglm .nrk .twk .wz .jjk {

            padding-bottom: 15px;

        }

        .v20190507_index_zsglm .nrk .twk .wz .jjk .xbt {

            font-size: 14px;

            line-height: 14px;

            margin-top: 0px;

        }

        .v20190507_index_zsglm .nrk .twk .wz .jjk .jj {

            font-size: 12px;

            line-height: 15px;

            margin-top: 12px;

        }

        .v20190507_index_zsglm .nrk .twk .wz .an {

            font-size: 12px;

            line-height: 20px;

            padding: 0 15px;

            border-top-left-radius: 20px;

            border-bottom-left-radius: 20px;

            border-top-right-radius: 20px;

            border-bottom-right-radius: 20px;

            -moz-border-radius-topleft: 20px;

            -moz-border-radius-bottomleft: 20px;

            -moz-border-radius-topright: 20px;

            -moz-border-radius-bottomright: 20px;

            -webkit-border-top-left-radius: 20px;

            -webkit-border-bottom-left-radius: 20px;

            -webkit-border-top-right-radius: 20px;

            -webkit-border-bottom-right-radius: 20px;

        }

        .v20190507_index_dstd {

            height: 822px;

        }

        .v20190507_index_dstd > .pic {

            height: 822px;

        }

        .v20190507_index_dstd > .pic img {

            height: 822px;

        }

        .v20190507_index_dstd .zsgk {

            width: 1200px;

            height: 250px;

            margin-top: 30px;

        }

        .v20190507_index_dstd .zsgk .fy_zf {

            width: 22px;

            height: 250px;

            margin-left: 20px;

        }

        .v20190507_index_dstd .zsgk .fy_zf img {

            width: 22px;

            height: 80px;

            margin-top: 85px;

        }

        .v20190507_index_dstd .zsgk .fy_yf {

            width: 22px;

            height: 250px;

            margin-right: 20px;

        }

        .v20190507_index_dstd .zsgk .fy_yf img {

            width: 22px;

            height: 80px;

            margin-top: 85px;

        }

        .v20190507_index_dstd .zsgk .swiper-container {

            width: 1050px;

            margin: 0 33px;

        }

        .v20190507_index_dstd .zsgk .nr {

            width: 210px;

            padding-top: 20px;

        }

        .v20190507_index_dstd .zsgk .nr .pic {

            width: 138px;

            height: 138px;

        }

        .v20190507_index_dstd .zsgk .nr .pic img {

            width: 138px;

            height: 138px;

            border-top-left-radius: 138px;

            border-bottom-left-radius: 138px;

            border-top-right-radius: 138px;

            border-bottom-right-radius: 138px;

            -moz-border-radius-topleft: 138px;

            -moz-border-radius-bottomleft: 138px;

            -moz-border-radius-topright: 138px;

            -moz-border-radius-bottomright: 138px;

            -webkit-border-top-left-radius: 138px;

            -webkit-border-bottom-left-radius: 138px;

            -webkit-border-top-right-radius: 138px;

            -webkit-border-bottom-right-radius: 138px;

        }

        .v20190507_index_dstd .zsgk .nr .pic .tw {

            width: 138px;

            height: 138px;

            border-top-left-radius: 138px;

            border-bottom-left-radius: 138px;

            border-top-right-radius: 138px;

            border-bottom-right-radius: 138px;

            -moz-border-radius-topleft: 138px;

            -moz-border-radius-bottomleft: 138px;

            -moz-border-radius-topright: 138px;

            -moz-border-radius-bottomright: 138px;

            -webkit-border-top-left-radius: 138px;

            -webkit-border-bottom-left-radius: 138px;

            -webkit-border-top-right-radius: 138px;

            -webkit-border-bottom-right-radius: 138px;

            font-size: 14px;

            line-height: 138px;

        }

        .v20190507_index_dstd .zsgk .nr .xm {

            font-size: 22px;

            line-height: 22px;

            margin-top: 5px;

        }

        .v20190507_index_dstd .zsgk .nr .yx {

            font-size: 14px;

            line-height: 14px;

            margin-top: 15px;

        }

        .v20190507_index_dstd .zsgk .nr .zy {

            font-size: 12px;

            line-height: 12px;

            margin-top: 7px;

        }

        .v20190507_index_dstd .nrk {

            max-width: 1220px;

        }

        .v20190507_index_dstd .nrk .nr {

            width: 227px;

            height: 330px;

            margin: 8px;

        }

        .v20190507_index_dstd .nrk .nr .btan {

            height: 30px;

            margin-top: 17px;

            margin-left: 22px;

            margin-right: 22px;

        }

        .v20190507_index_dstd .nrk .nr .btan .bt {

            font-size: 20px;

            line-height: 30px;

        }

        .v20190507_index_dstd .nrk .nr .btan .bt a {

            font-size: 20px;

            line-height: 30px;

        }

        .v20190507_index_dstd .nrk .nr .btan .an {

            width: 78px;

            height: 30px;

            line-height: 28px;

        }

        .v20190507_index_dstd .nrk .nr .yx {

            font-size: 12px;

            line-height: 12px;

            margin-left: 22px;

            margin-top: 10px;

        }

        .v20190507_index_dstd .nrk .nr .zy {

            font-size: 12px;

            line-height: 12px;

            margin-left: 22px;

            margin-top: 10px;

        }

        .v20190507_index_rmzy .nrk {

            width: 1220px;

            height: 254px;

            margin-top: 30px;

        }

        .v20190507_index_rmzy .nrk .nr {

            width: 285px;

            padding-bottom: 10px;

        }

        .v20190507_index_rmzy .nrk .nr .pic {

            width: 265px;

            height: 145px;

        }

        .v20190507_index_rmzy .nrk .nr .pic img {

            width: 265px;

            height: 145px;

        }

        .v20190507_index_rmzy .nrk .nr .bt {

            font-size: 14px;

            line-height: 14px;

            margin-top: 15px;

            margin-left: 30px;

        }

        .v20190507_index_rmzy .nrk .nr .hx {

            width: 24px;

            margin: 10px 0 10px 30px;

        }

        .v20190507_index_rmzy .nrk .nr .sz {

            font-size: 12px;

            line-height: 12px;

            margin-left: 30px;

            margin-bottom: 8px;

        }

        .v20190507_index_rmzy .nrk .nr .an {

            width: 90px;

            height: 26px;

            font-size: 12px;

            line-height: 26px;

            right: 30px;

            bottom: 20px;

        }

        .v20190507_index_gshj .nrwk {

            height: 579px;

            margin-top: 30px;

        }

        .v20190507_index_gshj .nrwk .dhk {

            width: 1200px;

            padding: 30px 0;

        }

        .v20190507_index_gshj .nrwk .dhk .dh {

            font-size: 16px;

            line-height: 16px;

            padding: 0 30px;

        }

        .v20190507_index_gshj .nrwk .nrk {

            width: 1200px;

        }

        .v20190507_index_gshj .nrwk .nrk .pic {

            width: 242px;

            height: 134px;

            margin-left: 20px;

            margin-bottom: 19px;

        }

        .v20190507_index_gshj .nrwk .nrk .pic img {

            width: 238px;

            height: 130px;

        }

        .v20190507_index_gshj .nrwk .nrk .pic.pd {

            width: 670px;

            height: 440px;

        }

        .v20190507_index_gshj .nrwk .nrk .pic.pd img {

            width: 666px;

            height: 436px;

        }

        .v20190507_index_yszx .nrk {

            width: 1200px;

            padding-bottom: 60px;

            margin-top: 30px;

        }

        .v20190507_index_yszx .nrk .hdk {

            width: 312px;

        }

        .v20190507_index_yszx .nrk .hdk .nr {

            font-size: 12px;

            line-height: 17px;

            padding-bottom: 20px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .pic {

            width: 272px;

            height: 168px;

            margin: 20px 20px 0 20px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .pic img {

            width: 272px;

            height: 168px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .bt {

            font-size: 14px;

            line-height: 20px;

            margin-top: 10px;

            margin-bottom: 0px;

            padding: 0 20px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .lx {

            padding-left: 20px;

            width: 156px;

            margin-top: 15px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .lx img {

            width: 17px;

            height: 17px;

            margin-right: 10px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .fy {

            padding-right: 20px;

            width: 156px;

            margin-top: 10px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .fy img {

            width: 17px;

            height: 17px;

            margin-right: 10px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .rq {

            padding: 0 20px;

            margin-top: 10px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .rq img {

            width: 17px;

            height: 17px;

            margin-right: 10px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .dz {

            padding: 0 20px;

        }

        .v20190507_index_yszx .nrk .hdk .nr .dz img {

            width: 17px;

            height: 17px;

            margin-right: 10px;

        }

        .v20190507_index_yszx .nrk .wzk {

            width: 858px;

            margin-left: 30px;

        }

        .v20190507_index_yszx .nrk .wzk .dtw {

            width: 858px;

            height: 191px;

        }

        .v20190507_index_yszx .nrk .wzk .dtw .pic {

            width: 283px;

            height: 191px;

        }

        .v20190507_index_yszx .nrk .wzk .dtw .pic img {

            width: 243px;

            height: 151px;

        }

        .v20190507_index_yszx .nrk .wzk .dtw .bt {

            width: 555px;

            padding-top: 40px;

            font-size: 16px;

            line-height: 16px;

            margin-left: 0px;

        }

        .v20190507_index_yszx .nrk .wzk .dtw .jj {

            width: 555px;

            padding-top: 7px;

            font-size: 12px;

            line-height: 19px;

            margin-left: 0px;

            float: left;

        }

        .v20190507_index_yszx .nrk .wzk .dtw .rq {

            width: 555px;

            padding-top: 20px;

            font-size: 10px;

            line-height: 10px;

            margin-left: 0px;

        }

        .v20190507_index_yszx .nrk .wzk .listk {

            width: 858px;

        }

        .v20190507_index_yszx .nrk .wzk .listk .dhk {

            margin-top: 25px;

            padding-bottom: 15px;

        }

        .v20190507_index_yszx .nrk .wzk .listk .dhk .dh {

            font-size: 14px;

            line-height: 14px;

            padding: 0 40px;

        }

        .v20190507_index_yszx .nrk .wzk .listk ul {

            width: 888px;

            padding-top: 5px;

        }

        .v20190507_index_yszx .nrk .wzk .listk li {

            width: 414px;

            line-height: 12px;

            margin-top: 8px;

            margin-right: 30px;

        }

        .v20190507_index_yszx .nrk .wzk .listk li a {

            width: 350px;

            font-size: 12px;

            line-height: 12px;

        }

        .v20190507_index_yszx .nrk .wzk .listk li span {

            width: 62px;

            font-size: 12px;

            line-height: 12px;

        }

        .v20190507_index_hzhb {

            height: 690px;

        }

        .v20190507_index_hzhb > .pic {

            height: 322px;

        }

        .v20190507_index_hzhb > .pic img {

            height: 322px;

        }

        .v20190507_index_hzhb .nrk {

            width: 1200px;

            height: 466px;

            padding: 30px;

            margin-top: 30px;

        }

        .v20190507_index_hzhb .nrk .nr {

            width: 226px;

            margin: 30px 0;

        }

        .v20190507_index_hzhb .nrk .nr .pic {

            width: 130px;

            height: 130px;

        }

        .v20190507_index_hzhb .nrk .nr .pic img {

            width: 130px;

            height: 130px;

            border-radius: 100%;

        }

        .v20190507_index_hzhb .nrk .nr .bt {

            font-size: 14px;

            line-height: 14px;

            margin-top: 15px;

        }

    }

    -->

</style>

<script>

    function ldxqh_bg(v) {

        for (i = 0; i <= 6; i++) {

            if (i != v) {

                if (i > 0) {

                    $(".ldxdh div").eq(i - 1).css({"color": "#999999", "background-color": "#F2F2F2"});

                }

            }

        }

        $(".ldxdh").css({"border-bottom": "1px solid " + ldxcolor[v - 1]});

        $(".ldxdh div").eq(v - 1).css({"color": "#fff", "background-color": ldxcolor[v - 1]});

        $(".ldxxq .nr .sz").css({"color": ldxcolor[v - 1]});

    }

</script>
<div class="tjanl">
    <div class="tjanlk">
        <div class="pictureSlider poster-main">
            <ul id="zturn" class="poster-list">
                <li class="poster-item  zturn-item" data-url="http://www.psoneart.com/cgalbk/3320.html">
                    <img src="/yishu/images/5e58bf36605b2.jpg" alt="动画专业作品集线上培训">
                </li>
                <li class="poster-item  zturn-item" data-url="http://www.psoneart.com/cgalbk/3319.html">
                    <img src="/yishu/images/5e58bf64a9002.jpg" alt="插画设计本科作品集线上培训">
                </li>
                <li class="poster-item  zturn-item" data-url="http://www.psoneart.com/cgalbk/3235.html">
                    <img src="/yishu/images/5e5644be07c3c.jpg" alt="动画专业英国本科留学">
                </li>
                <li class="poster-item  zturn-item" data-url="http://www.psoneart.com/cgalbk/3236.html">
                    <img src="/yishu/images/5e56468d024de.jpg" alt="美国产品设计本科留学">
                </li>
                <li class="poster-item  zturn-item" data-url="http://www.psoneart.com/cgalyjs/2094.html">
                    <img src="/yishu/images/5c948a07076ff.jpg" alt="英国插画专业留学申请">
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="v20190507_index">
    <div class="v20190507_index_xz">
        <div class="pic"><img src="/yishu/images/index_xz_bg.jpg"/></div>
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt" title="Choose PSONE艺术留学">Choose PS-ONE</div>
            <div class="hx"></div>
            <div class="zbt" title="选择PSONE艺术留学">选择PS-ONE</div>
        </div>
        <div class="nrk">
            <div class="nr">
                <div class="pic"><img src="/yishu/images/index_xz_01.jpg"/></div>
                <div class="dpic"><img src="/yishu/images/index_xz_dx.png"/></div>
                <div class="lx">A</div>
                <div class="bt" title="PSONE国际艺术教育">PS-ONE国际艺术教育</div>
                <div class="jj">获得来自教育行业领头代表<span class="czmh">弘成教育</span>的绝对认可，艺术留学行业内目前唯一一家拿到战略性数千万级融资的艺术留学机构。</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/viponeyyljh/#xzb','_blank');">
                <div class="pic"><img src="/yishu/images/index_xz_02.jpg"/></div>
                <div class="dpic"><img src="/yishu/images/index_xz_dx.png"/></div>
                <div class="lx">B</div>
                <div class="bt">多对一教学与服务系统</div>
                <div class="jj"><span class="czmh">衔接课程、教研审核、教学服务、增值服务</span>4大板块，跨部门合作，多环节跟踪，随时定位把关，全系统服务艺术生。</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/kxkkc/','_blank');">
                <div class="pic"><img src="/yishu/images/index_xz_03.jpg"/></div>
                <div class="dpic"><img src="/yishu/images/index_xz_dx.png"/></div>
                <div class="lx">C</div>
                <div class="bt">UCP跨学科教学</div>
                <div class="jj">国内艺术教学结合<span class="czmh">海外招生官教授</span>团队，全面发展学生的综合艺术素养，无缝式衔接海外教学课程体系。</div>
            </div>
            <div class="hx"></div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/zsg/#xzd','_blank');">
                <div class="pic"><img src="/yishu/images/index_xz_04.jpg"/></div>
                <div class="dpic"><img src="/yishu/images/index_xz_dx.png"/></div>
                <div class="lx">D</div>
                <div class="bt">六维教学保录体系</div>
                <div class="jj">提供针对性实际教学，<span class="czmh">顶尖院校标准</span>审核面试，理念、专业、技能的综合提升，让你的申请成功率不断升级。</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/cgal/','_blank');">
                <div class="pic"><img src="/yishu/images/index_xz_05.jpg"/></div>
                <div class="dpic"><img src="/yishu/images/index_xz_dx.png"/></div>
                <div class="lx">E</div>
                <div class="bt">学员俱乐部，优化背景提升</div>
                <div class="jj">提供<span class="czmh">官方暑期夏校</span>、在校项目搭建、品牌及设计师合作等，<span class="czmh">丰富学员CV</span>，提升学员的创造经历和社会阅历。
                </div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/zpjpx/','_blank');">
                <div class="pic"><img src="/yishu/images/index_xz_06.jpg"/></div>
                <div class="dpic"><img src="/yishu/images/index_xz_dx.png"/></div>
                <div class="lx">F</div>
                <div class="bt">限额招生，确保升学质量</div>
                <div class="jj">限额招生，设置门槛，<span class="czmh">坚决抵制代做</span>。保证每位PSONE艺术留学生都能稳步完成作品集，获得真实的专业收获。</div>
            </div>
        </div>
    </div>
    <div class="v20190507_index_cgal">
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Successful Case</div>
            <div class="hx"></div>
            <div class="zbt">成功案例</div>
        </div>
        <div class="nrk">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce280d7f1faf.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(714)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">雨佳同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：4个月</div>
                                <div class="zy">申请专业：服装设计</div>
                                <div class="yx">录取院校：马兰欧尼时装学院</div>
                            </div>
                        </div>
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce361767b203.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(713)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">露露同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：12个月</div>
                                <div class="zy">申请专业：珠宝设计</div>
                                <div class="yx">录取院校：英国皇家艺术学院</div>
                            </div>
                        </div>
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce27fbecd178.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(712)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">沐灵同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：10个月</div>
                                <div class="zy">申请专业：平面设计</div>
                                <div class="yx">录取院校：美国马里兰艺术学院</div>
                            </div>
                        </div>
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce27f5c68194.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(711)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">学阳同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：5个月</div>
                                <div class="zy">申请专业：服装设计</div>
                                <div class="yx">录取院校：中央圣马丁艺术与设计学院</div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce27ed598d36.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(710)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">小宇同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：3个月</div>
                                <div class="zy">申请专业：建筑设计</div>
                                <div class="yx">录取院校：AA建筑联盟学院</div>
                            </div>
                        </div>
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce27e9e0666b.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(709)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">凯茜同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：8个月</div>
                                <div class="zy">申请专业：服装设计</div>
                                <div class="yx">录取院校：帕森斯设计学院</div>
                            </div>
                        </div>
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce27e57a2ff6.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(708)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">幸岭同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：10个月</div>
                                <div class="zy">申请专业：平面设计</div>
                                <div class="yx">录取院校：美国纽约视觉艺术学院</div>
                            </div>
                        </div>
                        <div class="nr">
                            <div class="pic"><img src="/yishu/images/5ce27ddb60895.jpg"/></div>
                            <div class="an" onclick="return getPhotos_spzq(707)"><img src="/yishu/images/index_cgal_an.png"/>
                            </div>
                            <div class="xm">秋锡同学</div>
                            <div class="hx"></div>
                            <div class="jj">
                                <div class="xl">培训时间：12个月</div>
                                <div class="zy">申请专业：景观设计</div>
                                <div class="yx">录取院校：英国AA建筑联盟学院</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fydh">
                <div class="swiper-pagination" style=""></div>
            </div>
        </div>
    </div>
    <div class="v20190507_index_kcfw">
        <div class="pic"><img src="/yishu/images/index_kcfw_bg.jpg"/></div>
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Curriculum Service</div>
            <div class="hx"></div>
            <div class="zbt">课程服务</div>
        </div>
        <div class="nrk">
            <div class="nrz">
                <div class="pic"><img src="/yishu/images/index_kcfw_01.jpg"/></div>
                <div class="wz">
                    <div class="bt">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_sj.png"/></div>
                        本科作品集课程
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        TOP院校师资教学
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        六大系工作室联合教学
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        全面培养学生艺术能力
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        个性化定制作品集课程
                    </div>
                    <div class="an" onclick="window.open('http://www.psoneart.com/zpjpx/','_blank');">了解更多</div>
                </div>
            </div>
            <div class="nrr">
                <div class="pic"><img src="/yishu/images/index_kcfw_02.jpg"/></div>
                <div class="wz">
                    <div class="bt">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_sj.png"/></div>
                        研究生作品集课程
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        跨学科体系，打造作品集
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        顶级海归导师5级审核
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        接轨国际名校课程体系
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        限额招生，保证教学质量
                    </div>
                    <div class="an" onclick="window.open('http://www.psoneart.com/zpjpx/','_blank');">了解更多</div>
                </div>
            </div>
            <div class="nrz">
                <div class="pic"><img src="/yishu/images/index_kcfw_03.jpg"/></div>
                <div class="wz">
                    <div class="bt">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_sj.png"/></div>
                        VIP课程
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        跨学科体系，打造作品集
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        海外招生官亲自审核作品集
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        不限课时，全程多V1教学
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        奖学金获取率高达85%以上
                    </div>
                    <div class="an" onclick="window.open('http://www.psoneart.com/viponeyyljh/','_blank');">了解更多</div>
                </div>
            </div>
            <div class="nrr">
                <div class="pic"><img src="/yishu/images/index_kcfw_04.jpg"/></div>
                <div class="wz">
                    <div class="bt">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_sj.png"/></div>
                        招生官审核
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        招生官全程把控作品集
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        招生官亲笔推荐信
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        一对一模拟面试
                    </div>
                    <div class="jj">
                        <div class="vmpic"><img src="/yishu/images/index_kcfw_yd.png"/></div>
                        高额奖学金录取保障
                    </div>
                    <div class="an" onclick="window.open('http://www.psoneart.com/zsg/','_blank');">了解更多</div>
                </div>
            </div>
        </div>
        <div class="zxpic"><img src="/yishu/images/index_kcfw_zx.png"/></div>
    </div>
    <div class="v20190507_index_zsglm">
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Admissions Advisory Board</div>
            <div class="hx"></div>
            <div class="zbt">招生官联盟</div>
        </div>
        <div class="nrk">
            <div class="tpk">
                <div class="tp1pic" onclick="window.open('http://www.psoneart.com/zsg/','_blank');">
                    <img src="/yishu/images/index_zsglm_01.jpg"/>
                    <div class="tp1">
                        <div class="y">
                            <div class="s">1</div>
                        </div>
                        <div class="z">定制规划</div>
                    </div>
                </div>
                <div class="tp2pic" onclick="window.open('http://www.psoneart.com/zsg/','_blank');">
                    <img src="/yishu/images/index_zsglm_02.jpg"/>
                    <div class="tp2">
                        <div class="y">
                            <div class="s">2</div>
                        </div>
                        <div class="z">作品审核</div>
                    </div>
                </div>
                <div class="tp3pic" onclick="window.open('http://www.psoneart.com/zsg/','_blank');">
                    <img src="/yishu/images/index_zsglm_03.jpg"/>
                    <div class="tp3">
                        <div class="y">
                            <div class="s">3</div>
                        </div>
                        <div class="z">亲笔推荐信</div>
                    </div>
                </div>
            </div>
            <div class="twk">
                <div class="tp4pic" onclick="window.open('http://www.psoneart.com/zsg/','_blank');">
                    <img src="/yishu/images/index_zsglm_04.jpg"/>
                    <div class="tp4">
                        <div class="y">
                            <div class="s">4</div>
                        </div>
                        <div class="z">高额奖学金</div>
                    </div>
                </div>
                <div class="wz">
                    <div class="bt" title="PSONE艺术留学 Admission Officer Association">PS-ONE Admission Officer
                        Association
                    </div>
                    <div class="jjk">
                        <div class="xbt">招生官联盟：</div>
                        <div class="jj">
                            为学生带来直面院校、海外优质教育资源的机会。通过招生官联盟，学生能得到最契合高校招生标准的艺术留学申请指导，有方向、有重点的提升能力，最大程度优化申请结果、增加申请成功和高额奖学金几率。
                        </div>
                    </div>
                    <div class="jjk">
                        <div class="xbt">审核服务：</div>
                        <div class="jj">
                            为艺术生每一个作品集项目把关，增加申请艺术名校的成功机率。给与最契合招生标准的提升建议，让学生在收获offer的路上不会跑偏。作品集终审给与建议提高录取保障，有效增加高额奖学金获取几率。
                        </div>
                    </div>
                    <div class="jjk">
                        <div class="xbt">规划服务：</div>
                        <div class="jj">
                            最权威解读申请过程中的每个节点，为学生的艺术学习做长线规划。通过长期跟进，最深入挖掘亮点，助力学生收获最有深度的PS文书。还原最真实的面试场景，最终给出一封最有说服力的推荐信。
                        </div>
                    </div>
                    <div class="an" id="bdan">预约报名</div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div class="v20190507_index_dstd">
        <div class="pic"><img src="/yishu/images/index_dstd_bg.jpg"/></div>
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Mentor Team</div>
            <div class="hx"></div>
            <div class="zbt">导师团队</div>
        </div>
        <div class="zsgk">
            <div class="fy_zf"><img src="/yishu/images/index_dstd_zf.png"></div>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f7730f1e9c.jpg"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Timothy Goodman</div>
                            <div class="yx">纽约视觉艺术学院</div>
                            <div class="zy">插画与平面设计教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f77ec4e354.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Ariel Raz</div>
                            <div class="yx">美国斯坦福大学</div>
                            <div class="zy">设计思维学科教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f77df8f928.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Debbie Millman</div>
                            <div class="yx">纽约视觉艺术学院</div>
                            <div class="zy">Branding专业教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f78d7ba976.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Maria Luisa Rossi</div>
                            <div class="yx">美国创意设计学院</div>
                            <div class="zy">综合设计专业教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f78e0a3062.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Andrew R. Haas</div>
                            <div class="yx">AA建筑联盟学院</div>
                            <div class="zy">建筑专业教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f78e99cb54.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Clara Lieu</div>
                            <div class="yx">罗德岛设计学院</div>
                            <div class="zy">插画专业教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f78f222432.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">James Pegg</div>
                            <div class="yx">英国伦敦时装学院</div>
                            <div class="zy">服装设计专业教授</div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="nr">
                            <div class="pic">
                                <img src="/yishu/images/5c0f78fa734e0.png"/>
                                <div class="tw"
                                     onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">向TA提问
                                </div>
                            </div>
                            <div class="xm">Gail Anderson</div>
                            <div class="yx">纽约视觉艺术学院</div>
                            <div class="zy">平面设计专业教授</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fy_yf"><img src="/yishu/images/index_dstd_yf.png"></div>
        </div>
        <div class="nrk">
            <div class="nr">
                <div class="pic"><a href="/mstd/149.html" target="_blank">
                        <div class="pick"><img src="/yishu/images/5a13da2899870.jpg" alt="悉尼大学数字传媒 | 电子交互艺术"/><span
                                    class="bi"></span></div>
                    </a></div>
                <div class="btan">
                    <div class="bt"><a href="/mstd/149.html" target="_blank">魏老师</a></div>
                    <div class="an" onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">立即咨询
                    </div>
                </div>
                <div class="yx">悉尼大学</div>
                <div class="zy">数字传媒 | 电子交互艺术</div>
            </div>
            <div class="nr">
                <div class="pic"><a href="/mstd/1232.html" target="_blank">
                        <div class="pick"><img src="/yishu/images/5af29c1fcdb84.jpg" alt="帕森斯设计学院服装设计"/><span class="bi"></span>
                        </div>
                    </a></div>
                <div class="btan">
                    <div class="bt"><a href="/mstd/1232.html" target="_blank">张老师</a></div>
                    <div class="an" onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">立即咨询
                    </div>
                </div>
                <div class="yx">帕森斯设计学院</div>
                <div class="zy">服装设计</div>
            </div>
            <div class="nr">
                <div class="pic"><a href="/mstd/309.html" target="_blank">
                        <div class="pick"><img src="/yishu/images/5a13f1935f0f8.jpg" alt="伯明翰城市大学珠宝学院珠宝设计"/><span
                                    class="bi"></span></div>
                    </a></div>
                <div class="btan">
                    <div class="bt"><a href="/mstd/309.html" target="_blank">王老师</a></div>
                    <div class="an" onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">立即咨询
                    </div>
                </div>
                <div class="yx">伯明翰城市大学珠宝学院</div>
                <div class="zy">珠宝设计</div>
            </div>
            <div class="nr">
                <div class="pic"><a href="/mstd/267.html" target="_blank">
                        <div class="pick"><img src="/yishu/images/5a13e36ad6737.jpg"
                                               alt="坎伯韦尔艺术学院 UAL-Camberwell书籍艺术 | 平面设计"/><span class="bi"></span></div>
                    </a></div>
                <div class="btan">
                    <div class="bt"><a href="/mstd/267.html" target="_blank">熊老师</a></div>
                    <div class="an" onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">立即咨询
                    </div>
                </div>
                <div class="yx">坎伯韦尔艺术学院 UAL-Camberwell</div>
                <div class="zy">书籍艺术 | 平面设计</div>
            </div>
            <div class="nr">
                <div class="pic"><a href="/mstd/172.html" target="_blank">
                        <div class="pick"><img src="/yishu/images/5a12707632ff8.jpg" alt="英国谢菲尔德大学城市设计"/><span class="bi"></span>
                        </div>
                    </a></div>
                <div class="btan">
                    <div class="bt"><a href="/mstd/172.html" target="_blank">王老师</a></div>
                    <div class="an" onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');">立即咨询
                    </div>
                </div>
                <div class="yx">英国谢菲尔德大学</div>
                <div class="zy">城市设计</div>
            </div>
        </div>
    </div>
    <div class="v20190507_index_rmzy">
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Hot Majors</div>
            <div class="hx"></div>
            <div class="zbt">热门专业</div>
        </div>
        <div class="nrk">
            <div class="nr">
                <div class="pic"><img src="/yishu/images/5c09ec372cc44.png" alt="服装设计专业"/></div>
                <div class="bt">服装设计</div>
                <div class="hx"></div>
                <div class="sz">申请占比：18%</div>
                <div class="sz">TOP5成功率：65%</div>
                <div class="an" onclick="window.open('http://www.psoneart.com/ssx/186.html','_blank');">了解更多</div>
            </div>
            <div class="nr">
                <div class="pic"><img src="/yishu/images/5c09ed3a64ee6.png" alt="平面设计专业"/></div>
                <div class="bt">平面设计</div>
                <div class="hx"></div>
                <div class="sz">申请占比：14%</div>
                <div class="sz">TOP5成功率：73%</div>
                <div class="an" onclick="window.open('http://www.psoneart.com/sjsjx/168.html','_blank');">了解更多</div>
            </div>
            <div class="nr">
                <div class="pic"><img src="/yishu/images/5d3e703e7771f.png" alt="交互设计专业"/></div>
                <div class="bt">交互专业</div>
                <div class="hx"></div>
                <div class="sz">申请占比：13%</div>
                <div class="sz">TOP5成功率：64%</div>
                <div class="an" onclick="window.open('http://www.psoneart.com/sjcxx/174.html','_blank');">了解更多</div>
            </div>
            <div class="nr">
                <div class="pic"><img src="/yishu/images/5c09ed05efbf0.png" alt="动画设计专业"/></div>
                <div class="bt">动画设计</div>
                <div class="hx"></div>
                <div class="sz">申请占比：9%</div>
                <div class="sz">TOP5成功率：65%</div>
                <div class="an" onclick="window.open('http://www.psoneart.com/sjmtx/200.html','_blank');">了解更多</div>
            </div>
        </div>
    </div>
    <div class="v20190507_index_gshj">
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt" title="Environment of PSONE艺术留学">Environment of PS-ONE</div>
            <div class="hx"></div>
            <div class="zbt">公司环境</div>
        </div>
        <div class="nrwk">
            <div class="dhk">
                <div class="dh xz" id="gshj_dh_1" onmouseover="go_gshj(1)">北京</div>
                <div class="dh" id="gshj_dh_2" onmouseover="go_gshj(2)">上海</div>
                <div class="dh" id="gshj_dh_3" onmouseover="go_gshj(3)">重庆</div>
                <div class="dh" id="gshj_dh_4" onmouseover="go_gshj(4)">郑州</div>
                <div class="dh" id="gshj_dh_5" onmouseover="go_gshj(5)">成都</div>
                <div class="dh" id="gshj_dh_6" onmouseover="go_gshj(6)">深圳</div>
                <div class="dh" id="gshj_dh_7" onmouseover="go_gshj(7)">西安</div>
                <div class="dh" id="gshj_dh_8" onmouseover="go_gshj(8)">广州</div>
                <div class="dh" id="gshj_dh_11" onmouseover="go_gshj(11)">青岛</div>
                <div class="dh" id="gshj_dh_9" onmouseover="go_gshj(9)">纽约</div>
                <div class="dh" id="gshj_dh_10" onmouseover="go_gshj(10)">伦敦</div>
            </div>
            <div class="nrk" id="gshj_nr_1">
                <div class="pic pd"><img src="/yishu/images/5ce2630c84d25.jpg" alt="北京艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce263ec8d161.jpg" alt="北京艺术留学作品集"/></div>
                <div class="pic "><img src="/yishu/images/5ce263df6f7f3.jpg" alt="北京艺术留学作品集机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce263b9ebc5d.jpg" alt="北京艺术留学中心"/></div>
                <div class="pic "><img src="/yishu/images/5ce2638dc1a92.jpg" alt="北京艺术生留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce2636cb6d57.jpg" alt="北京作品集培训机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce263fdb3ebd.jpg" alt="北京艺术留学哪家好"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_2">
                <div class="pic pd"><img src="/yishu/images/5d089802c6068.jpg" alt="上海艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b458ca28.jpg" alt="上海艺术留学作品集"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b335b4d2.jpg" alt="上海艺术留学培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b24f1a94.jpg" alt="上海艺术留学作品集机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b153b1fe.jpg" alt="上海艺术留学中介"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b07e921f.jpg" alt="PSONE(品思)上海艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce26af94b299.jpg" alt="艺术留学作品集"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_3">
                <div class="pic pd"><img src="/yishu/images/5ce26b651e4f0.jpg" alt="重庆艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce26bc1019c1.jpg" alt="psone重庆艺术留学作品集培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce26bb34289e.jpg" alt="重庆艺术留学作品集培训机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce26ba03acac.jpg" alt="重庆艺术留学培训中心"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b92086f0.jpg" alt="重庆作品集培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b82988a4.jpg" alt="重庆艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce26b7310309.jpg" alt="重庆艺术生留学广场"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_4">
                <div class="pic pd"><img src="/yishu/images/5ce26be5c16a4.jpg" alt="郑州艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce26d9b45204.jpg" alt="郑州艺术留学作品集"/></div>
                <div class="pic "><img src="/yishu/images/5ce26d8df32af.jpg" alt="郑州艺术留学作品集培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce26d7fef520.jpg" alt="郑州艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce26d6e9a85c.jpg" alt="郑州艺术留学国际教育"/></div>
                <div class="pic "><img src="/yishu/images/5ce26d3f771da.jpg" alt="psone(品思)郑州艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce26bf828eea.jpg" alt="郑州出国留学培训"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_5">
                <div class="pic pd"><img src="/yishu/images/5ce26dbe5bcfc.jpg" alt="成都艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce279034c8cd.jpg" alt="成都作品集培训部"/></div>
                <div class="pic "><img src="/yishu/images/5ce278f3580d0.jpg" alt="成都出国留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce278e57aa53.jpg" alt="成都艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce278d671c40.jpg" alt="psone成都艺术教育"/></div>
                <div class="pic "><img src="/yishu/images/5ce278b44fb15.jpg" alt="成都作品集辅导"/></div>
                <div class="pic "><img src="/yishu/images/5ce278a4e6c03.jpg" alt="成都品思艺术留学中心"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_6">
                <div class="pic pd"><img src="/yishu/images/5ce27929c2a95.jpg" alt="深圳艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b1580d14.jpg" alt="深圳艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b07cdee9.jpg" alt="深圳艺术留学培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce27ac354732.jpg" alt="深圳艺术留学作品集"/></div>
                <div class="pic "><img src="/yishu/images/5ce27ab3e8d7c.jpg" alt="深圳艺术留学作品集培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce27aa2a3f2c.jpg" alt="深圳艺术留学场景"/></div>
                <div class="pic "><img src="/yishu/images/5ce2793e29cf3.jpg" alt="PSONE深圳艺术生留学"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_7">
                <div class="pic pd"><img src="/yishu/images/5d07512d9db52.jpg" alt="西安艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b8de5d9e.jpg" alt="西安艺术留学图片"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b807768e.jpg" alt="西安艺术留学场景"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b719eb6f.jpg" alt="西安艺术留学培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b5f57d4c.jpg" alt="西安艺术生留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b5050f30.jpg" alt="西安艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce27b3f183a0.jpg" alt="西安艺术留学作品集培训"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_8">
                <div class="pic pd"><img src="/yishu/images/5ce27ba7d7a04.jpg" alt="广州艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27c02a718c.jpg" alt="广州艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce27bf354d4d.jpg" alt="广州艺术留学中介"/></div>
                <div class="pic "><img src="/yishu/images/5ce27be1e39fe.jpg" alt="psone广州艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27bd41d61f.jpg" alt="广州艺术留学培训"/></div>
                <div class="pic "><img src="/yishu/images/5ce27bc49de80.jpg" alt="广州艺术生留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27bb675eb0.jpg" alt="广州艺术类留学"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_11">
                <div class="pic pd"><img src="/yishu/images/5d71dca878020.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5d71dcba4d105.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5d71dcd06c08e.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5d71dce2c5149.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5d71dcf664f34.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5d71dd05a7278.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5d71dd1539f17.jpg" alt=""/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_9">
                <div class="pic pd"><img src="/yishu/images/5ce27c88c6606.jpg" alt="美国艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27cdd33b62.jpg" alt="psone美国艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27cce95ce4.jpg" alt="美国艺术生留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27cc129b45.jpg" alt="美国艺术类留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce27cb2e13d3.jpg" alt="美国艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce27ca5a3c67.jpg" alt=""/></div>
                <div class="pic "><img src="/yishu/images/5ce27c9708e03.jpg" alt="美国艺术留学中介"/></div>
            </div>
            <div class="nrk dn" id="gshj_nr_10">
                <div class="pic pd"><img src="/yishu/images/5ce6588f22fa9.jpg" alt="英国艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce6589d6eb62.jpg" alt="英国伦敦艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce658aa4e6f8.jpg" alt="psone英国艺术留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce658b8735c3.jpg" alt="英国艺术生留学"/></div>
                <div class="pic "><img src="/yishu/images/5ce658c7b7d32.jpg" alt="psone英国伦敦留学场地"/></div>
                <div class="pic "><img src="/yishu/images/5ce658d810c85.jpg" alt="英国艺术留学机构"/></div>
                <div class="pic "><img src="/yishu/images/5ce6592cca2ca.jpg" alt="英国艺术留学中介"/></div>
            </div>
        </div>
    </div>
    <div class="v20190507_index_yszx">
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Art News</div>
            <div class="hx"></div>
            <div class="zbt">艺术资讯</div>
        </div>
        <div class="nrk">
            <div class="hdk">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="nr">
                                <div class="pic"><a href="http://www.psoneart.com/psonehd/3495.html"
                                                    rel="external nofollow" target="_blank"><img
                                                src="/yishu/images/5ea7ccdcc8675.jpg"></a></div>
                                <div class="bt"><a href="http://www.psoneart.com/psonehd/3495.html" target="_blank">疫情下的海外留学安全，资深国际安全防卫专家现身说法</a>
                                </div>
                                <div class="lx"><img src="/yishu/images/index_hdzx_ico_01.png"><span>线上</span></div>
                                <div class="fy"><img src="/yishu/images/index_hdzx_ico_02.png"><span>免费</span></div>
                                <div style="clear:both;"></div>
                                <div class="rq"><img
                                            src="/yishu/images/index_hdzx_ico_03.png"><span>2020 年 5 月 8 日 19:00-20:00</span></div>
                                <div class="dz"><img src="/yishu/images/index_hdzx_ico_04.png"><span>直播平台小鹅通</span></div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="nr">
                                <div class="pic"><a href="http://www.psoneart.com/psonehd/3478.html"
                                                    rel="external nofollow" target="_blank"><img
                                                src="/yishu/images/5e9e71e7ec417.jpg"></a></div>
                                <div class="bt"><a href="http://www.psoneart.com/psonehd/3478.html" target="_blank">哈佛大学GSD建筑学院导师主创STUDIO，邀你为艺术家设计虚拟居所</a>
                                </div>
                                <div class="lx"><img src="/yishu/images/index_hdzx_ico_01.png"><span>线下</span></div>
                                <div class="fy"><img src="/yishu/images/index_hdzx_ico_02.png"><span>报名咨询</span></div>
                                <div style="clear:both;"></div>
                                <div class="rq"><img src="/yishu/images/index_hdzx_ico_03.png"><span>暂定5月中旬开课</span></div>
                                <div class="dz"><img src="/yishu/images/index_hdzx_ico_04.png"><span>PS-ONE成都校区</span></div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="nr">
                                <div class="pic"><a href="http://www.psoneart.com/psonehd/3480.html"
                                                    rel="external nofollow" target="_blank"><img
                                                src="/yishu/images/5e9e98f148b9b.jpg"></a></div>
                                <div class="bt"><a href="http://www.psoneart.com/psonehd/3480.html" target="_blank">艺术专业留学生，公益设计艺术竞赛，作品兑换全球火到断货的Switch？</a>
                                </div>
                                <div class="lx"><img src="/yishu/images/index_hdzx_ico_01.png"><span>线上</span></div>
                                <div class="fy"><img src="/yishu/images/index_hdzx_ico_02.png"><span>免费</span></div>
                                <div style="clear:both;"></div>
                                <div class="rq"><img
                                            src="/yishu/images/index_hdzx_ico_03.png"><span>征集时间：2020年4月20日-5月20日</span></div>
                                <div class="dz"><img src="/yishu/images/index_hdzx_ico_04.png"><span>PSONE国际艺术教育</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fydh">
                    <div class="swiper-pagination" style=""></div>
                </div>
            </div>
            <div class="wzk">
                <div class="dtw">
                    <div class="pic"><a href="http://www.psoneart.com/psonehd/3477.html" rel="external nofollow"
                                        target="_blank"><img src="/yishu/images/5e9e67da0c1b0.jpg"></a></div>
                    <div class="bt"><a href="http://www.psoneart.com/psonehd/3477.html" target="_blank">日本艺术留学，PSONE匠壹研修会，日本艺术修士直通车</a>
                    </div>
                    <div class="jj">
                        日本独特的文化土壤孕育了以东京五美为代表的一大批顶尖艺术名校，凭借高水准的教学水平、和在地理、学费上的优势，始终位于艺术留学生择校选择的第一梯队。本着以实现学生艺术梦想为核心的教育理念，PS-ONE集结中日最优良的教学资源，在2020及今后每一个申请季，为每一位有梦想的日本艺术留学生们保驾护航！PS-ONE匠壹研修会上线了！...
                    </div>
                    <div class="rq">PS-ONE活动丨2020.04.21 14:57:47</div>
                </div>
                <div class="listk">
                    <div class="dhk">
                        <div class="dh xz" id="yszx_dh_1" onmouseover="go_yszx(1)">院校/专业</div>
                        <div class="dh" id="yszx_dh_2" onmouseover="go_yszx(2)">作品集锦囊</div>
                        <div class="dh" id="yszx_dh_3" onmouseover="go_yszx(3)">申请文书宝典</div>
                        <div class="dh" id="yszx_dh_4" onmouseover="go_yszx(4)">留学生活</div>
                    </div>
                    <div class="list" id="yszx_nr_1">
                        <ul>
                            <li><a href="http://www.psoneart.com/sqwsbd/3259.html"
                                   target="_blank">疫情过后，在准备艺术留学申请来得及吗？</a><span>2020.02.20</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3321.html" target="_blank">平面设计作品集风格“野路子”学员，SVA、MICA、CCA等名校争先抢</a><span>2020.02.28</span>
                            </li>
                            <li><a href="http://www.psoneart.com/psonehd/3342.html" target="_blank">艺术留学家长说系列分享，谁说艺术留学是一个人的事</a><span>2020.03.03</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3353.html" target="_blank">艺术作品集培训线上，跨学科一举拿下SVA、MICA的秘笈</a><span>2020.03.05</span>
                            </li>
                            <li><a href="http://www.psoneart.com/ysyx/3355.html" target="_blank">SAIC面试通过率，芝加哥艺术学院面试问题有哪些？</a><span>2020.03.05</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3360.html" target="_blank">线上艺术留学作品集培训，学员5个月0基础拿下AA、悉尼大学offer</a><span>2020.03.06</span>
                            </li>
                            <li><a href="http://www.psoneart.com/psonehd/3373.html" target="_blank">留学背景提升项目，5天100个项目，官方证书及推荐信，去圣马丁过暑假</a><span>2020.03.10</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3388.html"
                                   target="_blank">作品集线上培训课效果如何？</a><span>2020.03.13</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3401.html" target="_blank">作品集培训为什么那么贵？</a><span>2020.03.18</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3407.html"
                                   target="_blank">皇艺平面设计作品集获国家专利加持，学员专访</a><span>2020.03.19</span></li>
                        </ul>
                    </div>
                    <div class="list dn" id="yszx_nr_2">
                        <ul>
                            <li><a href="http://www.psoneart.com/zpjjn/3165.html"
                                   target="_blank">如何制作作品集？作品集创作中如何剖析自己？</a><span>2020.01.15</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3180.html" target="_blank">艺术作品集创作如何平衡客观事实和主观情绪的表达？</a><span>2020.01.21</span>
                            </li>
                            <li><a href="http://www.psoneart.com/psonehd/3221.html" target="_blank">线上跨学科作品集培训，如何用跨学科思维抵抗疫情？</a><span>2020.02.11</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3248.html" target="_blank">作品集线上培训体验，3个月0基础，作品集完美收官</a><span>2020.02.18</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3249.html"
                                   target="_blank">动画、电影专业作品集里面的动作特征</a><span>2020.02.18</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3308.html" target="_blank">交互设计线上作品集培训可行吗？学员亲述授课体验</a><span>2020.02.27</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3323.html"
                                   target="_blank">产品设计线上作品集培训课靠谱吗？</a><span>2020.02.28</span></li>
                            <li><a href="http://www.psoneart.com/psonehd/3341.html" target="_blank">海外线上作品集培训课，10位名校招生官，线上探索跨学科作品集！</a><span>2020.03.03</span>
                            </li>
                            <li><a href="http://www.psoneart.com/zpjjn/3387.html"
                                   target="_blank">疫情期间学生线上上课，作品集培训怎么样？</a><span>2020.03.13</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3408.html"
                                   target="_blank">留学作品集指导一般多少费用？</a><span>2020.03.19</span></li>
                        </ul>
                    </div>
                    <div class="list dn" id="yszx_nr_3">
                        <ul>
                            <li><a href="http://www.psoneart.com/lxsh/3114.html" target="_blank">艺术专业本科生申请国内外院校有何不同？</a><span>2019.12.27</span>
                            </li>
                            <li><a href="http://www.psoneart.com/lxsh/3148.html"
                                   target="_blank">艺术留学的学历是一张废纸吗？</a><span>2020.01.10</span></li>
                            <li><a href="http://www.psoneart.com/lxsh/3185.html" target="_blank">疫情对艺术留学申请有哪些影响？美澳限制入境，多国签证、航班告急</a><span>2020.02.03</span>
                            </li>
                            <li><a href="http://www.psoneart.com/sqwsbd/3215.html"
                                   target="_blank">2020年英国热门大学语言课申请详细介绍</a><span>2020.02.10</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3265.html" target="_blank">美国申请留学平面设计，学员拿下SVA、MICA多校带奖offer</a><span>2020.02.21</span>
                            </li>
                            <li><a href="http://www.psoneart.com/lxsh/3276.html"
                                   target="_blank">英国脱欧对艺术留学申请有哪些影响？</a><span>2020.02.24</span></li>
                            <li><a href="http://www.psoneart.com/psonehd/3291.html" target="_blank">日本艺术留学院校申请规划全攻略，这一套课程就够了！</a><span>2020.02.25</span>
                            </li>
                            <li><a href="http://www.psoneart.com/sqwsbd/3309.html" target="_blank">英国院校申请面试大全,总有一所大学是你的Design
                                    School</a><span>2020.02.27</span></li>
                            <li><a href="http://www.psoneart.com/lxsh/3348.html"
                                   target="_blank">为什么英国院校奖学金申请难？</a><span>2020.03.04</span></li>
                            <li><a href="http://www.psoneart.com/ysyx/3365.html" target="_blank">CSM圣马丁服装设计申请学员，零基础收获名校offer</a><span>2020.03.09</span>
                            </li>
                        </ul>
                    </div>
                    <div class="list dn" id="yszx_nr_4">
                        <ul>
                            <li><a href="http://www.psoneart.com/lxsh/3065.html" target="_blank">艺术留学录取方式有几种？</a><span>2019.12.13</span>
                            </li>
                            <li><a href="http://www.psoneart.com/lxsh/3183.html" target="_blank">瘟疫疫情下怎么艺术留学？6问PSONE全国教研中心</a><span>2020.02.03</span>
                            </li>
                            <li><a href="http://www.psoneart.com/lxsh/3185.html" target="_blank">疫情对艺术留学申请有哪些影响？美澳限制入境，多国签证、航班告急</a><span>2020.02.03</span>
                            </li>
                            <li><a href="http://www.psoneart.com/lxsh/3195.html" target="_blank">是就业好还是留学好？</a><span>2020.02.04</span>
                            </li>
                            <li><a href="http://www.psoneart.com/ysyx/3202.html"
                                   target="_blank">伦艺LCC交互设计研究生留学怎么样？</a><span>2020.02.06</span></li>
                            <li><a href="http://www.psoneart.com/zpjjn/3230.html"
                                   target="_blank">艺术留学线上课有哪些优势？如何保障教学质量？</a><span>2020.02.13</span></li>
                            <li><a href="http://www.psoneart.com/lxsh/3241.html" target="_blank">UAL发放高额奖学金，近期艺术留学事件</a><span>2020.02.17</span>
                            </li>
                            <li><a href="http://www.psoneart.com/sqwsbd/3243.html"
                                   target="_blank">艺术留学面试技巧，线上面试干货</a><span>2020.02.17</span></li>
                            <li><a href="http://www.psoneart.com/psonehd/3247.html"
                                   target="_blank">上海留学作品集培训，六大科系全覆盖</a><span>2020.02.18</span></li>
                            <li><a href="http://www.psoneart.com/sqwsbd/3256.html"
                                   target="_blank">出国留学语言考试全面停考怎么办？</a><span>2020.02.20</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
    <div class="v20190507_index_hzhb">
        <div class="pic"><img src="/yishu/images/index_hzhb_bg.jpg"/></div>
        <div class="dbt">
            <div class="pic"><img src="/yishu/images/bt_bg.png"/></div>
            <div class="ebt">Cooperation</div>
            <div class="hx"></div>
            <div class="zbt">合作伙伴</div>
        </div>
        <div class="nrk">
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/59.html','_blank');">
                <div class="pic"><img src="/yishu/images/5c662974591fc.png" alt="英国AA建筑联盟学院"/></div>
                <div class="bt">英国AA建筑联盟学院</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/psonehd/614.html','_blank');">
                <div class="pic"><img src="/yishu/images/5ad464f5373cb.png" alt="PSONE艺术留学签约ARTONE，ONEONE相遇将是ONE次方的奇迹"/></div>
                <div class="bt">Art One</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/51.html','_blank');">
                <div class="pic"><img src="/yishu/images/5ad465054025c.png" alt="美国艺术中心设计学院"/></div>
                <div class="bt">美国艺术中心设计学院</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/49.html','_blank');">
                <div class="pic"><img src="/yishu/images/5ad4651460808.png" alt="英国伦敦艺术大学"/></div>
                <div class="bt">英国伦敦艺术大学</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/','_blank');">
                <div class="pic"><img src="/yishu/images/5ad465275c01b.png" alt="英美国家留学院校国外艺术留学院校"/></div>
                <div class="bt">特斯拉</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/7.html','_blank');">
                <div class="pic"><img src="/yishu/images/5d3fba577d9c5.png" alt="美国卡内基梅隆大学"/></div>
                <div class="bt">美国卡内基梅隆大学</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/1.html','_blank');">
                <div class="pic"><img src="/yishu/images/5ad46bb73ac17.png" alt="美国帕森斯设计学院"/></div>
                <div class="bt">美国帕森斯设计学院</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/zxsq/','_blank');">
                <div class="pic"><img src="/yishu/images/5ad467df685f3.png" alt="艺术留学资讯"/></div>
                <div class="bt">UBER</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/43.html','_blank');">
                <div class="pic"><img src="/yishu/images/5ad46678785ff.png" alt="美国普瑞特设计学院"/></div>
                <div class="bt">美国普瑞特设计学院</div>
            </div>
            <div class="nr" onclick="window.open('http://www.psoneart.com/ysyxk/4.html','_blank');">
                <div class="pic"><img src="/yishu/images/5ad4669a784be.png" alt="皇家艺术学院-皇家艺术学院申请-皇家艺术学院排名"/></div>
                <div class="bt">英国皇家艺术学院</div>
            </div>
        </div>
    </div>
</div>
<div class="bdwk dn">
    <style>

        .bdwk .bdzz {

            position: fixed;

            top: 0px;

            left: 0px;

            width: 100%;

            height: 100%;

            background: url(images/pic38.png) repeat;

            z-index: 20000;

            display: none;

        }

        .bdwk .bdk {

            width: 574px;

            height: 544px;

            margin-left: -287px;

            position: fixed;

            top: 0px;

            left: 50%;

            display: none;

            z-index: 999999;

            background-color: #fff;

            -webkit-box-shadow: 0 6px 6px rgba(25, 25, 25, .3);

            -moz-box-shadow: 0 6px 6px rgba(25, 25, 25, .3);

            box-shadow: 0 6px 6px rgba(25, 25, 25, .3);

        }

        .bdwk .bdk .x {

            width: 27px;

            height: 27px;

            position: absolute;

            top: 20px;

            right: 20px;

        }

        .bdwk .bdk .dbt {

            margin-top: 50px;

            text-align: center;

            font-size: 30px;

            line-height: 30px;

            color: #333;

            padding: 0;

        }

        .bdk .xbt {

            margin-top: 14px;

            text-align: center;

            font-size: 14px;

            line-height: 14px;

            color: #666;

            margin-bottom: 50px;

        }

        .bdwk .bdk .srk {

            width: 375px;

            margin: 20px auto;

            height: 40px;

        }

        .bdwk .bdk .srk .bt {

            text-align: right;

            font-size: 16px;

            line-height: 40px;

            color: #111;

            width: 64px;

            height: 40px;

            float: left;

        }

        .bdwk .bdk .srk .sr {

            width: 298px;

            height: 40px;

            float: left;

            margin-left: 11px;

            border: 1px solid #999;

            border-top-left-radius: 3px;

            border-bottom-left-radius: 3px;

            border-top-right-radius: 3px;

            border-bottom-right-radius: 3px;

            -moz-border-radius-topleft: 3px;

            -moz-border-radius-bottomleft: 3px;

            -moz-border-radius-topright: 3px;

            -moz-border-radius-bottomright: 3px;

            -webkit-border-top-left-radius: 3px;

            -webkit-border-bottom-left-radius: 3px;

            -webkit-border-top-right-radius: 3px;

            -webkit-border-bottom-right-radius: 3px;

        }

        .bdwk .bdk .srk .sr:hover {

            border: 1px solid #ed7829;

        }

        .bdwk .bdk .srk .sr .pic {

            width: 16px;

            height: 28px;

            margin-left: 11px;

            padding-top: 12px;

            float: left;

        }

        .bdwk .bdk .srk .sr input {

            width: 230px;

            height: 38px;

            float: left;

            margin-left: 20px;

            display: block;

            border: 0;

            padding: 0;

        }

        .bdwk .bdk .srk .sr input:focus {

            border: 0;

            outline: 0;

        }

        .bdwk .bdk .srk .sr .xlk {

            margin-left: 20px;

            width: 230px;

            height: 38px;

            float: left;

            background: url('images/vip_bd_xljt.png') right center no-repeat;

            /* the width and the height of your image */

            width: 230px;

            height: 38px;

            overflow: hidden;

        }

        .bdwk .bdk .srk .sr .xlk select {

            -webkit-appearance: none;

            -moz-appearance: none;

            appearance: none;

            background: transparent;

            border: none;

            width: 240px;

            height: 100%;

            color: #999;

        }

        .bdwk .bdk .an {

            width: 300px;

            height: 40px;

            text-align: center;

            font-size: 16px;

            line-height: 40px;

            color: #fff;

            background-color: #ed7829;

            border-top-left-radius: 3px;

            border-bottom-left-radius: 3px;

            border-top-right-radius: 3px;

            border-bottom-right-radius: 3px;

            -moz-border-radius-topleft: 3px;

            -moz-border-radius-bottomleft: 3px;

            -moz-border-radius-topright: 3px;

            -moz-border-radius-bottomright: 3px;

            -webkit-border-top-left-radius: 3px;

            -webkit-border-bottom-left-radius: 3px;

            -webkit-border-top-right-radius: 3px;

            -webkit-border-bottom-right-radius: 3px;

            margin-left: 75px

        }


    </style>
    <div class="bdzz" style="display: block;"></div>
    <div class="bdk" style="display: block;">
        <div class="x"><img src="/yishu/images/vip_bd_gb.png" alt=""></div>
        <div class="dbt">预约报名</div>
        <div class="xbt">预约招生官</div>
        <div class="srk">
            <div class="bt">姓名</div>
            <div class="sr">
                <div class="pic"><img src="/yishu/images/vip_bd_xm.png" alt=""></div>
                <input type="text" name="xm" id="xm" placeholder="请填写真实姓名" onfocus="this.placeholder=''"
                       onblur="this.placeholder='请填写真实姓名'"/>
            </div>
        </div>
        <div class="srk">
            <div class="bt">电话</div>
            <div class="sr">
                <div class="pic"><img src="/yishu/images/vip_bd_dh.png" alt=""></div>
                <input type="text" name="sj" id="sj" placeholder="请填11位手机号码" onfocus="this.placeholder=''"
                       onblur="this.placeholder='请填11位手机号码'"/>
            </div>
        </div>
        <div class="srk">
            <div class="bt">申请学历</div>
            <div class="sr">
                <div class="pic"><img src="/yishu/images/vip_bd_xl.png" alt=""></div>
                <div class="xlk">
                    <select name="xl" id="xl">
                        <option value="">请选择您要申请的学历</option>
                        <option value="本科">本科</option>
                        <option value="研究生">研究生</option>
                        <option value="预科">预科</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="srk">
            <div class="bt">意向专业</div>
            <div class="sr">
                <div class="pic"><img src="/yishu/images/vip_bd_zy.png" alt=""></div>
                <input type="text" name="zy" id="zy" placeholder="请填写您的意向专业" onfocus="this.placeholder=''"
                       onblur="this.placeholder='请填写您的意向专业'"/>
            </div>
        </div>
        <div class="srk">
            <div class="bt">意向国家</div>
            <div class="sr">
                <div class="pic"><img src="/yishu/images/vip_bd_gj.png" alt=""></div>
                <input type="text" name="gj" id="gj" placeholder="请填写您的意向国家" onfocus="this.placeholder=''"
                       onblur="this.placeholder='请填写您的意向国家'"/>
            </div>
        </div>
        <div class="srk">
            <div class="an">立即提交</div>
        </div>
    </div>
    <script>

        $('#bdan').click(function () {

            $('.bdwk ').show();

            $('.bdwk .bdzz').height($(window).height());

            $('.bdwk .bdk').css('margin-top', ($(window).height() - $('.bdwk .bdk').height()) / 2);

        });

        $('.bdwk .bdzz,.bdwk .x').click(function () {

            $('.bdwk').hide();

        });

        bdk_lxjhan = 0;

        $('.bdwk .bdk .srk .an').click(function () {

            if ($(".bdwk .bdk #xm").val() != '' && yzshj($(".bdwk .bdk #sj").val())) {

                if (bdk_lxjhan == 0) {

                    bdk_lxjhan = 1;

                    $('.bdwk .bdk .srk .an').html('提交中···');

                    $.post(
                        '/zt/openac.php',

                        {

                            bt: "官网 作品集培训 预约",

                            k1: $(".bdwk .bdk #xm").val(),

                            k2: $(".bdwk .bdk #sj").val(),

                            k3: $(".bdwk .bdk #zy").val(),

                            k4: $(".bdwk .bdk #xl").val(),

                            k5: $(".bdwk .bdk #gj").val()

                        },

                        function (data) { //回调click函数

                            //alert(data);

                            if (data == "ok") {

                                alert("提交成功！");

                                $(".bdwk .bdk #xm").val("");

                                $(".bdwk .bdk #sj").val("");

                                $(".bdwk .bdk #xl").val("");

                                $(".bdwk .bdk #zy").val("");

                                $(".bdwk .bdk #gj").val("");

                                $('.bdwk .bdk .srk .an').html('立即提交');

                                bdk_lxjhan = 0;

                            } else {

                                alert("提交失败，请稍后再试！");

                                //alert(data);

                            }

                        }
                    );

                } else {

                    alert("提交中，请稍等！");

                }

            } else {

                alert("姓名或手机号错误!");

                return false;

            }

        })


        function yzshj(v) {

            var a = /^((\(\d{3}\))|(\d{3}\-))?13\d{9}|15\d{9}|18\d{9}|14\d{9}|16\d{9}|17\d{9}|19\d{9}$/;

            if (v.length != 11 || !v.match(a)) {

                alert("手机号错误!");

                return false;

            } else {
                return true;
            }

        }

    </script>
</div>
<script>

    mySwiper_v20190507_index_cgal = new Swiper('.v20190507_index_cgal .swiper-container', {

        //autoplay: 3000,//可选选项，自动滑动

        loop: true,

        calculateHeight: true,

        simulateTouch: false,

        pagination: '.v20190507_index_cgal .swiper-pagination',

        paginationClickable: true,

        slidesPerView: 1,

    });

    mySwiper_v20190507_index_dstd = new Swiper('.v20190507_index_dstd .swiper-container', {

        //autoplay: 3000,//可选选项，自动滑动

        autoplay: 3000,

        loop: true,

        calculateHeight: true,

        paginationClickable: true,

        slidesPerView: 5,

    });

    $('.v20190507_index_dstd .fy_zf').click(function () {

        mySwiper_v20190507_index_dstd.swipePrev();

    });

    $('.v20190507_index_dstd .fy_yf').click(function () {

        mySwiper_v20190507_index_dstd.swipeNext();

    });

    mySwiper_v20190507_index_yszx = new Swiper('.v20190507_index_yszx .swiper-container', {

        //autoplay: 3000,//可选选项，自动滑动

        loop: true,

        calculateHeight: true,

        simulateTouch: false,

        pagination: '.v20190507_index_yszx .swiper-pagination',

        paginationClickable: true,

        slidesPerView: 1,

    });


    function go_gshj(v) {

        $("#gshj_dh_" + v).addClass("xz");

        $("#gshj_nr_" + v).removeClass("dn");

        for (i = 1; i <= 11; i++) {

            if (i != v) {

                $("#gshj_dh_" + i).removeClass("xz");

                $("#gshj_nr_" + i).addClass("dn");

            }

        }

    }


    function go_yszx(v) {

        $("#yszx_dh_" + v).addClass("xz");

        $("#yszx_nr_" + v).removeClass("dn");

        for (i = 1; i <= 10; i++) {

            if (i != v) {

                $("#yszx_dh_" + i).removeClass("xz");

                $("#yszx_nr_" + i).addClass("dn");

            }

        }

    }

    function getPhotos_spzq(id) {

        $("#photos").load("/Index/load_spzq_video?id=" + id);

    }


</script>