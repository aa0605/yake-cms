<?php
/**
 *
 * @throws \Exception
 * @var string $catDir
 * @var \common\modules\article\Article $article
 */
use common\modules\article\logic\ArticleLogic;
use common\modules\site\logic\SiteRegionLogic;
$this->title = $article->title . '';
$this->metaTags[] = '<meta name="keywords" content=""/>';
$this->metaTags[] = '<meta name="description" content=""/>';

?>
</body>
</html>