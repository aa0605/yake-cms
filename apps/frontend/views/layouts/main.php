<?php
use yii\helpers\Html;
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">

        <meta http-equiv="Cache-Control" content="no-transform"/>
        <meta http-equiv="Cache-Control" content="no-siteapp"/>
        <title><?= Html::encode($this->title)?></title>
        <?php $this->head() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="renderer" content="webkit">
        <script>

            /*<!--function isPc(){

              if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){

                  window.location="/Wap";

              }else{

              }

          }

          isPc();-->*/

            //平台、设备和操作系统

            var system = {

                win: false,

                mac: false,

                xll: false,

                ipad: false

            };

            //检测平台

            var p = navigator.platform;

            system.win = p.indexOf("Win") == 0;

            system.mac = p.indexOf("Mac") == 0;

            system.xll = (p == "X11") || (p.indexOf("Linux") == 0);

            system.ipad = (navigator.userAgent.match(/iPad/i) != null) ? true : false;

            //跳转语句，如果是手机访问就自动跳转到wap.baidu.com页面

            if (system.win || system.mac || system.xll || system.ipad) {

                var sUserAgent = navigator.userAgent.toLowerCase();

                var bIsIpad = sUserAgent.match(/ipad/i) == 'ipad';

                var bIsIphone = sUserAgent.match(/iphone os/i) == 'iphone os';

                var bIsMidp = sUserAgent.match(/midp/i) == 'midp';

                var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == 'rv:1.2.3.4';

                var bIsUc = sUserAgent.match(/ucweb/i) == 'web';

                var bIsCE = sUserAgent.match(/windows ce/i) == 'windows ce';

                var bIsWM = sUserAgent.match(/windows mobile/i) == 'windows mobile';

                var bIsAndroid = sUserAgent.match(/android/i) == 'android';

                if (bIsIpad || bIsIphone || bIsMidp || bIsUc7 || bIsUc || bIsCE || bIsWM || bIsAndroid) {

                    window.location.href = "http://m.<?=TOP_DOMAIN?>/";

                }

            } else {

                window.location.href = "http://m.<?=TOP_DOMAIN?>/";

            }

        </script>
        <link rel="stylesheet" href="/yishu/css/bootstrap.min.css">
        <script src="/yishu/js/jquery-1.12.4.min.js"></script>
        <script src="/yishu/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/yishu/css/font-awesome.min.css">
        <link rel="stylesheet" href="/yishu/css/aliplayer-min.css"/>
        <script type="text/javascript" src="/yishu/js/aliplayer-min.js"></script>
        <link rel="stylesheet" href="/yishu/css/idangerous.swiper.css">
        <script src="/yishu/js/idangerous.swiper.min.js"></script>
        <link rel="stylesheet" href="/yishu/css/new.css">
        <script src="/yishu/js/myjs.js"></script>
        <style>

            @-ms-viewport {
                width: device-width;
            }

            @-o-viewport {
                width: device-width;
            }

            @viewport {
                width: device-width;
            }

        </style>
        <script>

            // Copyright 2014-2015 Twitter, Inc.

            // Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {

                var msViewportStyle = document.createElement('style')

                msViewportStyle.appendChild(
                    document.createTextNode(
                        '@-ms-viewport{width:auto!important}'
                    )
                )

                document.querySelector('head').appendChild(msViewportStyle)

            }

        </script>
        <!--[if lte IE 9]>

        <script src="http://apps.bdimg.com/libs/html5shiv/3.7/html5shiv.min.js"></script>

        <script src="http://apps.bdimg.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <![endif]-->
        <!--[if lte IE 8]>

        <style>


            .container {

                width: 1560px;

                max-width: 1560px;

            }

            .header {

                background-color: #000;

            }

            .header .csdh .dh {

                /*font-size: 0.8em;*/

                padding: 0;

                width: 30%;

            }

            .wmdkc .kck .nr .pic .wz {

                background-color: #666666;

            }

            .wmdkc .kck .nr:first-child + .nr + .nr + .nr {

                /*.wmdkc .kck .nr:last-child{*/

                margin: 0;

            }

            .rmzy .rmk .nr .wz {

                background-color: #666666;

            }

            .footer .footerk .logowmqrcode .wm .dh a {

                font-size: 14px;

                line-height: 14px;

            }

            .footer .footerk .logowmqrcode .wm .dh a:first-child + a + a + a + a + a {

                border: 0;

            }


        </style>

        <![endif]-->
    </head>
    <?php $this->beginBody() ?>
    <body>

    <style>

        a:hover {

            text-decoration: none;

        }

        .header .logo {

            width: 520px;

            max-width: 520px;

            padding-top: 42px;

            font-size: 16px;

            line-height: 24px;

            white-space: nowrap;

        }

        .header .logo .pic {

            width: 227px;

            height: 66px;

            float: left;

        }

        .header .logo .pic img {

            width: 227px;

            height: 66px;

        }

        .header .logo .wzk {

            width: 248px;

            height: 66px;

            float: left;

            color: #fff;

            padding-left: 20px;

            margin-left: 20px;

            border-left: 1px solid #fff;

        }

        .header .logo .wzk .zw {

            margin-top: 9px;

        }

        @media (min-width: 0px) and (max-width: 1539px) {

            .header .logo {

                width: 390px;

                max-width: 390px;

                padding-top: 32px;

                font-size: 12px;

                line-height: 18.5px;

            }

            .header .logo .pic {

                width: 171px;

                height: 50px;

            }

            .header .logo .pic img {

                width: 171px;

                height: 50px;

            }

            .header .logo .wzk {

                width: 191px;

                height: 51px;

                padding-left: 14px;

                margin-left: 14px;

            }

            .header .logo .wzk .zw {

                margin-top: 7px;

            }

        }

    </style>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="logo col-xs-4">
                    <a href="http://www.psoneart.com/" rel="external nofollow" title="艺术留学">
                        <div class="pic"><img src="images/logo_2019-05-14.png" alt="艺术留学"></div>
                        <div class="wzk">
                            <div class="zw">用艺术设计未来</div>
                            <div class="yw">using art to design your future</div>
                        </div>
                    </a></div>
                <div class="col-xs-8">
                    <div class="csdh row">
                        <div class="col-xs-8 col-xs-offset-4">
                            <div class="row">
                                <div class="cs col-xs-8"><a href="http://www.psoneart.com/tag_13/">北京</a>-<a
                                            href="http://www.psoneart.com/tag_14/">上海</a>-<a
                                            href="http://www.psoneart.com/tag_15/">成都</a>-<a
                                            href="http://www.psoneart.com/tag_9/">深圳</a>-<a
                                            href="http://www.psoneart.com/tag_16/">重庆</a>-<a
                                            href="http://www.psoneart.com/tag_17/">郑州</a>-<a
                                            href="http://www.psoneart.com/tag_21/">西安</a>-<a
                                            href="http://www.psoneart.com/tag_20/">广州</a>-<a
                                            href="http://www.psoneart.com/tag_52/">青岛</a>-<a
                                            href="http://www.psoneart.com/tag_22/">纽约</a>-<a
                                            href="http://www.psoneart.com/tag_23/">伦敦</a></div>
                                <div class="dh">400-139-3918</div>
                            </div>
                        </div>
                    </div>
                    <div class="nav row">
                        <div class="col-xs-1-9"><a href="/" rel="external nofollow"
                                                   style="color:#00CC00;text-decoration:none;border-bottom:2px solid #00CC00;">首页</a>
                        </div>
                        <div class="col-xs-1-9 dropdown">
                            <a href="/zpjpx/" class="" data-hover="dropdown">作品集培训</a>
                            <div class="dropdown-menu">
                                <li><a href="/zpjpx/">艺术留学作品集课程</a></li>
                                <li><a href="/viponeyyljh/">VIP-ONE里挑一<small class="jing">荐</small></a></li>
                                <li><a href="/rbyslx/">日本艺术留学</a></li>
                                <li><a href="/oneldx/">ONE有体系</a></li>
                                <li><a href="/kxkkc/">UCP跨学科</a></li>
                                <li><a href="/zsg/">招生官联盟</a></li>
                                <li><a href="/hwxly/">海外夏校</a></li>
                                <li><a href="/snysj/">少年艺术家</a></li>
                                <li><a href="/btecykkc/">BTEC预科课程</a></li>
                                <li><a href="/alevel/">A-Level课程</a></li>
                            </div>
                        </div>
                        <div class="col-xs-1-9"><a href="/cgal/" class="">成功案例</a></div>
                        <div class="col-xs-1-9"><a href="/mstd/" class="">艺术导师</a></div>
                        <div class="col-xs-1-9 dropdown">
                            <a href="/ysyxk/" class="" data-hover="dropdown">院校/专业</a>
                            <div class="dropdown-menu">
                                <li><a href="/ysyxk/">艺术院校</a></li>
                                <li><a href="/yszy/">艺术专业</a></li>
                            </div>
                        </div>
                        <div class="col-xs-1-9"><a href="/yylx/" class="">音乐留学</a></div>
                        <div class="col-xs-1-9 dropdown">
                            <a href="/zxsq/" class="" data-hover="dropdown">留学干货</a>
                            <div class="dropdown-menu">
                                <li><a href="/zxsq/">留学干货</a></li>
                                <li><a href="/hdzq/" rel="external nofollow">活动专区</a></li>
                            </div>
                        </div>
                        <div class="col-xs-1-9 dropdown">
                            <a href="/spkc/" class="" data-hover="dropdown">ONE-学堂</a>
                            <div class="dropdown-menu">
                                <li><a href="/spkc/">ONE-学堂</a></li>
                                <li><a href="/zbkc/" rel="external nofollow">直播课程</a></li>
                            </div>
                        </div>
                        <div class="col-xs-1-9"><a href="/spzq/" class="">ONE-TV</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= \common\widgets\Alert::widget() ?>
    <?= $content ?>

    <div class="footer">
        <div class="footerk container">
            <div class="logowmqrcode row">
                <div class="pic col-xs-4">
                    <a href="/" title="艺术类留学" rel="external nofollow"><img src="images/logo_bottom.png" alt="艺术类留学"></a>
                </div>
                <div class="wm col-xs-5">
                    <div class="dh row">
                        <a href="/gywm/">关于我们</a><a href="/zxns/" rel="nofollow">招贤纳士</a><a href="/sitemap.html">网站地图</a><a
                                href="/hdzq/" rel="external nofollow">活动专区</a><a
                                onclick="window.open('https://tb.53kf.com/code/client/10158415/1','_blank');"
                                rel="external nofollow">联系我们</a><a href="/cgfd/">美术留学</a>
                    </div>
                    <div class="dz row">
                        <div class="dz_dh col-12">
                            <span class="xz" id="dh_1" onclick="dhdz(1)" onmouseover="dhdz(1)">北京</span>-<span id="dh_12"
                                                                                                               onclick="dhdz(12)"
                                                                                                               onmouseover="dhdz(12)">北京海淀</span>-<span
                                    id="dh_3" onclick="dhdz(3)" onmouseover="dhdz(3)">上海</span>-<span id="dh_2"
                                                                                                      onclick="dhdz(2)"
                                                                                                      onmouseover="dhdz(2)">成都</span>-<span
                                    id="dh_4" onclick="dhdz(4)" onmouseover="dhdz(4)">深圳</span>-<span id="dh_6"
                                                                                                      onclick="dhdz(6)"
                                                                                                      onmouseover="dhdz(6)">重庆</span>-<span
                                    id="dh_5" onclick="dhdz(5)" onmouseover="dhdz(5)">郑州</span>-<span id="dh_9"
                                                                                                      onclick="dhdz(9)"
                                                                                                      onmouseover="dhdz(9)">西安</span>-<span
                                    id="dh_10" onclick="dhdz(10)" onmouseover="dhdz(10)">广州</span>-<span id="dh_11"
                                                                                                         onclick="dhdz(11)"
                                                                                                         onmouseover="dhdz(11)">青岛</span>-<span
                                    id="dh_7" onclick="dhdz(7)" onmouseover="dhdz(7)">纽约</span>-<span id="dh_8"
                                                                                                      onclick="dhdz(8)"
                                                                                                      onmouseover="dhdz(8)">伦敦</span>
                        </div>
                        <div class="dz_k col-12">
                            <div id="dz_1">
                                北京市 东城区 银河SOHO C座2层30203室</br>
                                400-665-2071 / 400-139-3918
                            </div>
                            <div class="dn" id="dz_2">
                                成都市 锦江区 四川航空广场SAC16层 1601室</br>
                                028 - 67279988
                            </div>
                            <div class="dn" id="dz_3">
                                上海市 静安区 愚园路 546号4号楼4层 </br>
                                021 - 63830131
                            </div>
                            <div class="dn" id="dz_4">
                                深圳市 福田区 金中环国际商务大厦1902</br>
                                0755 - 88604364
                            </div>
                            <div class="dn" id="dz_5">
                                郑州市 金水区 花园路建业凯旋广场 A座 701室-704室</br>
                                0371 - 63681260
                            </div>
                            <div class="dn" id="dz_6">
                                重庆市江北区庆云路1号国金中心T1办公楼3615-3616 </br>
                                023 - 67101176
                            </div>
                            <div class="dn" id="dz_7">
                                315 W 36th Street New York NY 10018</br>
                                400-665-2071 / 400-139-3918
                            </div>
                            <div class="dn" id="dz_8">
                                BASE KX, 103C CAMLEY STREET, LONDON, N1C 4PF</br>
                                +44 07802779749
                            </div>
                            <div class="dn" id="dz_9">
                                陕西省 西安市 碑林区 长安北路 长安国际F座 11层1101室</br>
                                029 - 85306570
                            </div>
                            <div class="dn" id="dz_10">
                                广东省广州市天河区珠江东路6号周大福金融中心16层1602（K11）</br>
                                020 - 89819016
                            </div>
                            <div class="dn" id="dz_11">
                                青岛市市南区 香港中路40号数码港旗舰大厦501</br>
                                0532 - 80956677
                            </div>
                            <div class="dn" id="dz_12">
                                北京市海淀区中关村大街27号中关村大厦6层603-605室</br>
                                400-665-2071 / 400-139-3918
                            </div>
                        </div>
                    </div>
                </div>
                <div class="qrcode col-xs-3">
                    <div class="row">
                        <div class="nr col-xs-4">
                            <div class="pic"><img src="images/5d2d526c19e45.png" alt="活动周刊"/></div>
                            <div class="bt">活动周刊</div>
                        </div>
                        <div class="nr col-xs-4">
                            <div class="pic"><img src="images/5a129d3a891a3.jpg" alt="关注公众号"/></div>
                            <div class="bt">关注公众号</div>
                        </div>
                        <div class="nr col-xs-4">
                            <div class="pic"><img src="images/5d2d6cd203238.png" alt="微信小程序"/></div>
                            <div class="bt">微信小程序</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="yqlj row">
                <div class="col-12">
                    <div class="dh row">
                        <div class="xz" onmouseover="yqlj(1)" id="yqdh_1">艺术留学</div>
                        <div class="" onmouseover="yqlj(2)" id="yqdh_2">友情链接</div>
                        <div class="" onmouseover="yqlj(3)" id="yqdh_3">合作伙伴</div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="nr row">
                        <div class="col-12" id="yqlj_1">
                            <div class="row">
                                <div class="col-xs-6">
                                    美国艺术留学机构：<a href="http://www.psoneart.com/tag_2/" target="_blank">艺术留学机构排名</a><a
                                            href="http://www.psoneart.com/tag_35/" target="_blank">艺术留学哪家好</a><a
                                            href="http://www.psoneart.com/tag_4/" target="_blank">国外艺术类院校</a><a
                                            href="http://www.psoneart.com/tag_5/" target="_blank">美国艺术类大学排名</a>
                                </div>
                                <div class="col-xs-6">
                                    英国艺术留学机构：<a>英国美术专业申请</a><a href="http://www.psoneart.com/sqwsbd/1947.html"
                                                               target="_blank">英国留学PS范文</a><a
                                            href="http://www.psoneart.com/tag_10/">美术专业留学</a><a
                                            href="http://www.psoneart.com/tag_8/" target="_blank">英国有哪些艺术大学</a>
                                </div>
                                <div class="col-xs-6">
                                    艺术留学中介机构：<a>欧洲艺术留学中介</a><a href="http://www.psoneart.com/tag_18/" target="_blank">艺术留学教育机构</a><a
                                            href="http://www.psoneart.com/tag_19/" target="_blank">作品集培训机构</a><a
                                            href="http://www.psoneart.com/tag_39/" target="_blank">艺术留学作品集</a>
                                </div>
                                <div class="col-xs-6">
                                    艺术生出国留学：<a href="http://www.psoneart.com/tag_1/" target="_blank">艺术研究生留学</a><a
                                            href="http://www.psoneart.com/tag_7/" target="_blank">出国艺术留学学费</a><a
                                            href="http://www.psoneart.com/tag_6/" target="_blank">零基础艺术留学</a><a>艺术留学推荐信</a><a
                                            href="http://www.psoneart.com/tag_26/" target="_blank">艺术留学注意事项</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 dn" id="yqlj_2">
                            <a href="http://www.psoneart.com/tag_27/" title="英国艺术留学" target="_blank">英国艺术留学</a><a
                                    href="http://www.psoneart.com/tag_40/" title="美国艺术留学" target="_blank">美国艺术留学</a><a
                                    href="http://cq.yuloo.com/" title="重庆育路网" target="_blank">重庆育路网</a><a
                                    href="https://www.xttdxl.com" title="武汉拓展培训" target="_blank">武汉拓展培训</a><a
                                    href="https://www.lawtime.cn/ningbo" title="宁波律师" target="_blank">宁波律师</a><a
                                    href="http://www.kaoersi.com/" title="留学生课业辅导" target="_blank">留学生课业辅导</a><a
                                    href="http://www.shijinedu.com/" title="学历提升" target="_blank">学历提升</a><a
                                    href="http://m.psoneart.com/" title="艺术留学" target="_blank">艺术留学</a><a
                                    href="http://www.yshsports.com/" title="上海拓展培训" target="_blank">上海拓展培训</a><a
                                    href="http://www.psoneart.com/ysyx/3255.html" title="艺术留学广场" target="_blank">艺术留学广场</a><a
                                    href="http://www.bechina.org/" title="美国留学" target="_blank">美国留学</a><a href=""
                                                                                                           title="美国留学培训"
                                                                                                           target="_blank">美国留学培训</a><a
                                    href="http://www.crs.jsj.edu.cn/" title="中华人民共和国教育部 " target="_blank"
                                    rel="external nofollow">中华人民共和国教育部 </a><a href="http://www.eventer.cn/" title="活动策划公司"
                                                                              target="_blank">活动策划公司</a><a
                                    href="http://www.mison.cn/" title="商务英语培训" target="_blank">商务英语培训</a><a
                                    href="http://www.jinghujiaoyu.com/" title="考研培训班" target="_blank">考研培训班</a><a
                                    href="http://www.bjxzxw.com/" title="职业学校排名" target="_blank">职业学校排名</a><a
                                    href="http://www.jc-edu.org/" title="西班牙语培训" target="_blank">西班牙语培训</a><a
                                    href="http://www.baijiu88.com/" title="定制白酒的平台" target="_blank">定制白酒的平台</a><a
                                    href="http://www.bjhkbl.com/" title="留学生落户北京" target="_blank">留学生落户北京</a><a
                                    href="http://www.chrisdn.com" title="艺术留学研究生" target="_blank">艺术留学研究生</a><a
                                    href="http://www.liumeinet.com" title="美国高中留学" target="_blank">美国高中留学</a><a
                                    href="http://www.bimcn.org/" title="BIM培训" target="_blank">BIM培训</a><a
                                    href="http://www.iqiaowai.com" title="侨外出国" target="_blank">侨外出国</a><a
                                    href="http://ixinda.com/" title="托福代报" target="_blank">托福代报</a><a
                                    href="https://www.cgzixue.cn/" title="CG自学网" target="_blank">CG自学网</a></div>
                        <div class="col-12 dn" id="yqlj_3">
                            <a href="http://www.huashen-edu.com" title="上海留学中心" target="_blank">上海留学中心</a><a
                                    href="http://www.cnitedu.cn/" title="IT培训" target="_blank">IT培训</a><a
                                    href="http://gxg.huatu.com" title="公务员遴选网" target="_blank">公务员遴选网</a><a
                                    href="http://www.guojixuexiao.org/" title="国际学校" target="_blank">国际学校</a><a
                                    href="http://www.huanxingedu.com/" title="出国留学费用" target="_blank">出国留学费用</a><a
                                    href="http://www.betteredu.net" title="毕达留学" target="_blank">毕达留学</a><a
                                    href="https://www.spoto.net/" title="网络工程师培训" target="_blank">网络工程师培训</a><a
                                    href="http://www.chazhongzhi.cn/" title="中职网" target="_blank">中职网</a><a
                                    href="http://www.qsiedu.com" title="心理学考研" target="_blank">心理学考研</a><a
                                    href="http://www.cscse.edu.cn" title="中国留学网" target="_blank"
                                    rel="external nofollow">中国留学网</a><a href="http://www.psoneart.com/tag_54/" title="意大利艺术留学"
                                                                        target="_blank">意大利艺术留学</a><a
                                    href="http://www.freekaobo.com/" title="考博论坛" target="_blank">考博论坛</a><a
                                    href="http://www.earthedu.com/" title="出国留学" target="_blank">出国留学</a><a
                                    href="http://www.100xue.net/" title="强基计划" target="_blank">强基计划</a><a
                                    href="http://www.jsj.edu.cn/" title="教育部涉外监管信息官网" target="_blank" rel="external nofollow">教育部涉外监管信息官网</a><a
                                    href="http://www.psoneart.com/ysyx/" title="英美国家艺术设计专业留学" target="_blank">英美国家艺术设计专业留学</a><a
                                    href="http://www.psoneart.com/zpjjn/" title="艺术作品集培训" target="_blank">艺术作品集培训</a><a
                                    href="http://www.psoneart.com/sqwsbd/" title="艺术留学申请攻略" target="_blank">艺术留学申请攻略</a><a
                                    href="http://www.psoneart.com/lxsh/" title="出国留学生活" target="_blank">出国留学生活</a><a
                                    href="http://www.psoneart.com/zpjjn/" title="艺术作品集培训" target="_blank">艺术作品集培训</a><a
                                    href="http://www.caibihui.com" title="水彩画" target="_blank">水彩画</a><a
                                    href="http://www.ruyishi.com" title="易经学习" target="_blank">易经学习</a><a
                                    href="http://www.stonepx.cn/" title="企业培训" target="_blank">企业培训</a><a
                                    href="https://www.chinacma.org/" title="注册管理会计师" target="_blank">注册管理会计师</a><a
                                    href="www.xialingying.cc" title="中小学夏令营" target="_blank">中小学夏令营</a><a
                                    href="http://www.91pth.com/" title="口才班培训学校" target="_blank">口才班培训学校</a></div>
                    </div>
                </div>
            </div>
            <script>

            </script>
            <div class="bqxx row">
                <div class="col-12">
                    Copyright ©2020 PS-ONE品思国际艺术教育 <a onclick="window.open('http://www.beian.miit.gov.cn/','_blank');"
                                                      rel="external nofollow">京ICP备14021608号</a> <br/>
                    PSONE国际艺术留学专注于 <a href="http://www.psoneart.com" target="_blank" rel="external nofollow">英国艺术留学</a> <a
                            href="http://www.psoneart.com" target="_blank" rel="external nofollow">美国艺术留学</a> <a
                            href="http://www.psoneart.com" target="_blank" rel="external nofollow">作品集培训</a>跨学科 专业的<a
                            href="http://www.psoneart.com" target="_blank" style="margin:0;"
                            rel="external nofollow">艺术留学申请机构</a>
                </div>
            </div>
        </div>
    </div>
    <div id="photos"></div>
    <a class="gotop" href="javascript:void(0);" id="gotop" style="display: none;"></a>
    <script src="/yishu/js/new.js"></script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>