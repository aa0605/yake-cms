<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@apps', dirname(dirname(__DIR__)) . '/apps');
//Yii::setAlias('@xing', dirname(dirname(__DIR__)) . '/vendor/xing.chen');

define('IMG_DOMAIN', 'https://xx.oss-cn-shenzhen.aliyuncs.com/');


define('PATH_UPLOAD_ROOT', 'upload/');
define('UPLOAD_URL', IMG_DOMAIN . PATH_UPLOAD_ROOT);
define('TOP_DOMAIN', 'qukuailianyu.com');


function hr($str='ok', $html = 0) {
    $html && !is_array($str) && $str = htmlspecialchars($str);
    print_r($str);
    echo "<hr>\r\n\r\n";
}
function er($str='ok', $html = 0) {
    hr($str,$html);
    exit();
}
function tr($str, $is_exit = 1){
    if(isset($_GET['test']))	$is_exit ? er($str) : hr($str);
}