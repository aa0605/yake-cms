<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [

        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => '127.0.0.1',
                'port' => 6379,
                'database' => 15,
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true, //对url进行美化
            'showScriptName' => false,// 取消隐藏index.php
            'suffix' => '',//后缀
            'enableStrictParsing'=>FALSE,//不要求网址严格匹配，则不需要输入rules
            'rules' => [
                "<lan:zh-CN>/<dir:[\w_-]+>/<page:\d+>.html" => "article/article/lists",
                "<lan:zh-CN>/<dir:[\w_-]+>" => "article/article/lists",
                "zh-CN/<dir:[\w_-]+>/view-<articleId:\d+>.html" => "article/article/view",
                "<controller:\w+>/<action:\w+>" => "<controller>/<action>",
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w_]+>/<action:[\w_]+>/<id:\d+>' => '<controller>/<action>',
            ]
        ],
    ],
];
