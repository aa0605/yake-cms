<?php

namespace common\map;

class CommonMap
{
    const APP_SECRET = 'dgkh!!#(fdjak';

    // 通用是否
    const BOOLEAN_YES = 1;
    const BOOLEAN_NO = 0;
    public static $boolean = [
        self::BOOLEAN_YES => '是',
        self::BOOLEAN_NO => '否,'
    ];

    public static $wxProVersion = '1.0.0';

    // 小数位
    const DECIMALS = 3;
}