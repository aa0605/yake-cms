<?php

namespace common\map\api;
use common\logic\wz\WzQueryLogic;
use common\modules\community\map\wz\QueryMap;

/**
 * Created by PhpStorm.
 * User: Nph
 * Date: 2017/6/27
 * Time: 8:32
 */
class ResponseMap
{

    // TODO 关键错误码为1-50,50以上为不重要的提示码

    const ERROR_VERIFY_CODE = -10;  # 请输入正确的验证码
    const SUCCESSFUL_OPERATION = 0;
    const ERROR_SIGN = 1;
    const ERROR_POWER_NOT = 2;
    const ERROR_ACCOUNT_EXCEPTION = 3;
    const ERROR_SERVER_MAINTENANCE = 10;
    const ERROR_DATABASE_ERROR = 11;
    const ERROR_MONEY_LACK = 40;
    const ERROR_GOLD_LACK = 41;
    const ERROR_GROUP_JOIN = 42;
    const ERROR_PARAMS = 50;
    const ERROR_GET_DATA = 51;
    const ERROR_DATA_EMPTY = 60;
    const ERROR_DATA_REPEAT = 61;
    const ERROR_DATA_EXCEED = 62; // 超出数量/达到上限
    const ERROR_USER_EXIST = 99;
    const ERROR_USER_LOGIN = 100;
    const ERROR_FORMAT_EMAIL = 101;
    const LOGIN_THIRD_PARTY = 108;

    const MATCH_CONTENT_EMPTY = 201;

    const ERROR_PRICE_ABNORMAL = 300;

    const ERROR_ORDER_STATUS = 350;
    const COMMENT_NOT_BAN = 360;
    const COMMENT_NOT_CHASE_RATINGS = 361;

    // 支付金额验证错误：金额不一致
    const PAY_ERROR_AMOUNT = 401;
    // 订单已支付
    const PAY_ALREADY_PAID = 402;

    // 好友为黑名单状态
    const FRIEND_BLACKLIST = 600;

    # 内容处理
    // 图片非法
    const IMAGE_ILLEGAL = 700;
    const CONTENT_ILLEGAL = 701;
    const FACE_ILLEGAL = 702;
    const PHOTO_GENDER_ILLEGAL = 703;

    // 一般的警告，通常用于不需要中断程序执行的异常错误捕获
    const WARNING_COMMON = 1000;



    public static $codes = [
        self::ERROR_VERIFY_CODE => '请输入正确的验证码',
        self::SUCCESSFUL_OPERATION => '操作成功',
        self::ERROR_SIGN => '验签失败',
        self::ERROR_POWER_NOT => '无权操作',
        self::ERROR_ACCOUNT_EXCEPTION => '您已被接入黑署名',
        self::ERROR_SERVER_MAINTENANCE => '服务器维护',
        self::ERROR_DATABASE_ERROR => '数据库写入失败',
        self::ERROR_MONEY_LACK => '余额不足',
        self::ERROR_GOLD_LACK => '您的蛙币不足',
        self::ERROR_GROUP_JOIN => '剩余的加群次数不足',
        self::ERROR_PARAMS => '参数错误',
        self::ERROR_GET_DATA => '获取数据失败',
        self::ERROR_DATA_EMPTY => '没有这条数据',
        self::ERROR_DATA_REPEAT => '数据已存在',
        self::ERROR_DATA_EXCEED => '超出数量',
        self::ERROR_USER_EXIST => '用户不存在',
        self::ERROR_USER_LOGIN => '您未登陆，请先登陆',
        self::ERROR_FORMAT_EMAIL => '邮箱格式不正确',
        self::LOGIN_THIRD_PARTY => '第三方登陆验证失败',
        self::COMMENT_NOT_BAN => '不可以再评论',
        self::COMMENT_NOT_CHASE_RATINGS => '您不可以再追加评论',
        self::ERROR_PRICE_ABNORMAL => '价格异常',
        self::ERROR_ORDER_STATUS => '订单状态不正确',
        self::PAY_ERROR_AMOUNT => '支付金额验证错误：金额不一致',
        self::PAY_ALREADY_PAID => '订单已支付',
        self::FRIEND_BLACKLIST => '好友关系为黑名单',
        self::MATCH_CONTENT_EMPTY => '您没有设置问候语',

        self::IMAGE_ILLEGAL => '请上传健康、和谐、不含广告的图片（禁色情、纸币、反动、政治人物）',
        self::CONTENT_ILLEGAL => '提交的内容含有不合法的文字，请检查，请确保内容健康、和谐',
        self::FACE_ILLEGAL => '请上传清晰、无遮挡露脸的人物照片',
        self::PHOTO_GENDER_ILLEGAL => '不能上传异性照片，请上传清晰、无遮挡正面露脸的相片',
    ];
}