<?php

namespace common\modules\article;

use common\map\CommonMap;
use common\modules\article\map\ArticleRecommendMap;
use common\modules\user\User;
use common\modules\user\UserFollow;
use xing\article\map\ArticleMap;
use xing\article\models\ArticleRecommend;
use xing\article\models\ArticleView;

/**
 * Class Article
 * @package common\modules\article
 */
class Article extends \xing\article\models\Article
{

}