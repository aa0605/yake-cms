<?php

namespace common\modules\article;

use common\modules\user\User;
use Yii;


class ArticleComment extends \xing\article\models\ArticleComment
{
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['userId' => 'userId']);
    }
}
