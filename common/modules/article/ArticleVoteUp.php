<?php

namespace common\modules\article;

use common\modules\user\User;
use Yii;

class ArticleVoteUp extends \xing\article\models\ArticleVoteUp
{

    public function rules()
    {
        $return = parent::rules();
        $return[] = [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'userId']];
        return $return;
    }
    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['userId' => 'userId']);
    }
}
