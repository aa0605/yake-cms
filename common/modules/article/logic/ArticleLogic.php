<?php

namespace common\modules\article\logic;

use common\modules\article\Article;
use common\modules\article\ArticleCategory;
use common\modules\article\map\ArticleRecommendMap;
use common\modules\article\map\ArticleCategoryMap;
use common\modules\user\logic\UserAuthorLevelLogic;
use common\modules\user\logic\UserCalculationLogLogic;
use common\modules\user\map\UserCalculationLogMap;
use xing\article\map\ArticleMap;
use xing\article\models\ArticleData;

class ArticleLogic
{

    public static function delete($userId, $articleId)
    {
        $article = Article::findOne($articleId);
        if (empty($article)) throw new \Exception('没有这个文章');
        if ($article->userId != $userId) throw new \Exception('没有权限');
        $article->status = ArticleMap::STATUS_CLOSE;
        $article->logicSave();
    }

    public static function publish($userId, $categoryId, $title, $thumb, $content)
    {
        $article = Article::create($userId, $categoryId, $title, $thumb);
        ArticleData::create($article->articleId, $content);

        return $article;
    }
}