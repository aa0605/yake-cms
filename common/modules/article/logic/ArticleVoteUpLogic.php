<?php


namespace common\modules\article\logic;


use common\modules\article\ArticleVoteUp;

class ArticleVoteUpLogic
{

    public static function create($userId, $articleId)
    {
        if (ArticleVoteUp::find()->where(['userId' => $userId, 'articleId' => $articleId])->exists())
            throw new \Exception('您已赞过了');
        return ArticleVoteUp::create($userId, $articleId);
    }

    public static function delete($userId, $articleId)
    {
        $voteUp = ArticleVoteUp::findOne(['userId' => $userId, 'articleId' => $articleId]);
        $voteUp->delete();
    }
}