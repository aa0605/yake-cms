<?php

namespace common\modules\site;

use Yii;

/**
 * This is the model class for table "site_ad".
 *
 * @property int $adId
 * @property string $title 标题
 * @property int $adType 广告类型：1轮播图 2 图片 3视频
 * @property int|null $status 状态：1正常 0 关闭
 *
 * @property SiteAdList[] $siteAdLists
 */
class SiteAd extends \xing\helper\yii\BaseActiveModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_ad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['adType', 'status'], 'integer'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'adId' => 'Ad ID',
            'title' => '标题',
            'adType' => '广告类型：1轮播图 2 图片 3视频',
            'status' => '状态：1正常 0 关闭',
        ];
    }

    /**
     * Gets query for [[SiteAdLists]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSiteAdLists()
    {
        return $this->hasMany(SiteAdList::className(), ['adId' => 'adId']);
    }
}
