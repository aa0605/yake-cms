<?php

namespace common\modules\site;

use Yii;

/**
 * This is the model class for table "site_ad_list".
 *
 * @property int $listId
 * @property int $adId 广告父id
 * @property string $title 广告标题
 * @property int $pageType 页面类别：1站内文章 2站内页面 3远程url
 * @property string $src 图片/视频地址
 * @property string|null $link 页面地址
 *
 * @property SiteAd $ad
 */
class SiteAdList extends \xing\helper\yii\BaseActiveModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_ad_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['adId', 'pageType'], 'integer'],
            [['title', 'src'], 'required'],
            [['title', 'src', 'link'], 'string', 'max' => 300],
            [['adId'], 'exist', 'skipOnError' => true, 'targetClass' => SiteAd::className(), 'targetAttribute' => ['adId' => 'adId']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'listId' => 'List ID',
            'adId' => '广告父id',
            'title' => '广告标题',
            'pageType' => '页面类别',
            'src' => '图片/视频地址',
            'link' => '页面地址',
        ];
    }

    /**
     * Gets query for [[Ad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAd()
    {
        return $this->hasOne(SiteAd::className(), ['adId' => 'adId']);
    }
}
