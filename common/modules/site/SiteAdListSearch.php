<?php

namespace common\modules\site;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\site\SiteAdList;

/**
 * SiteAdListSearch represents the model behind the search form of `common\modules\site\SiteAdList`.
 */
class SiteAdListSearch extends SiteAdList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['listId', 'adId', 'pageType'], 'integer'],
            [['title', 'src', 'link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteAdList::find();
        $order = [];
        if (isset($params['sort']) && !empty($params['sort'])) {
            $order = $params['sort'];
            unset($query['sort']);
        } elseif (isset($this->primaryKey()[0]) && !empty($this->primaryKey()[0])) {
            $order = [$this->primaryKey()[0] => SORT_DESC];
        }
        $query->orderBy($order);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'listId' => $this->listId,
            'adId' => $this->adId,
            'pageType' => $this->pageType,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'src', $this->src])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
