<?php

namespace common\modules\site;

use Yii;

/**
 * This is the model class for table "site_feedback".
 *
 * @property int $id
 * @property int $userId 用户id
 * @property int $type 反馈类型
 * @property string $content 内容
 * @property string $language 用户所属语言
 * @property string $email 电子邮箱
 * @property int $createdTime 创建时间
 */
class SiteFeedback extends \xing\helper\yii\BaseActiveModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'content'], 'required'],
            [['userId', 'createdTime'], 'integer'],
            [['content'], 'string'],
            [['type'], 'string', 'max' => 1],
            [['language'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => '用户id',
            'type' => '反馈类型',
            'content' => '内容',
            'language' => '用户所属语言',
            'email' => '电子邮箱',
            'createdTime' => '创建时间',
        ];
    }
}
