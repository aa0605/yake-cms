<?php

namespace common\modules\site\logic;

use common\modules\site\map\CommonSetMap;
use xing\commonSet\models\CommonSet;

class CommonSetLogic
{

    public static function getDailySqc()
    {
        return CommonSet::readKeyValue(CommonSetMap::KEY_SQC_GIVE_DAY);
    }

    public static function getExchangeRatio()
    {
        return CommonSet::readKeyValue(CommonSetMap::KEY_EXCHANGE_RATIO);
    }
}