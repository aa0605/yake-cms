<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2018/1/5
 * Time: 16:16
 */

namespace common\modules\site\logic;

use Yii;

class SmsLogic
{

    /**
     * @return \xing\sms\yii\Sms
     */
    public static function getInstance()
    {
        return Yii::$app->sms;
    }

    /**
     * @param $mobile
     * @throws \Exception
     */
    public static function sendTextCode($mobile)
    {
        if (!\xing\helper\text\CheckHelper::checkMobile($mobile)) throw new \Exception('请输入正确的手机号');

        $sms = static::getInstance()->setMobile($mobile);
        $code = $sms->createCode();
        if (!$sms->sendTextCode($code)) {
            throw new \Exception('发送验证码失败');
        }
    }


    /**
     * 清除code
     * @param $mobile
     * @return bool|mixed
     */
    public static function clearCode($mobile)
    {
        return static::getInstance()->setMobile($mobile)->clearCode();
    }

    /**
     * 发送模板消息
     * @param $mobile
     * @param $templateConfig
     * @param array $param
     * @return bool
     * @throws \Exception
     */
    public static function sendTemplateMessage($mobile, $templateConfig, $param = [])
    {
        return static::getInstance()->setMobile($mobile)->sendTemplateSms($templateConfig, $param);
    }
}