<?php
/**
 * Created by PhpStorm.
 * User: xing.chen
 * Date: 2017/8/13
 * Time: 8:08
 */

namespace common\service;


class TokenService extends CacheService
{
    public static $prefix = 'token:';

    /**
     * 检查token是否正确
     * @param $name
     * @param $token
     * @return bool
     */
    public static function checkToken($name, $token)
    {
        $oldToken = static::get(self::key($name));
        return $oldToken == $token;
    }
}