/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 5.7.26 : Database - waibao_qukuailianyu
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`waibao_qukuailianyu` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

/*Table structure for table `admin_role` */

DROP TABLE IF EXISTS `admin_role`;

CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '角色编号(只支持两级)',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名称',
  `des` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '角色描述',
  `create_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建人',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新人',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(1) DEFAULT NULL COMMENT '状态，1正常，0为已删除',
  `rule` text COLLATE utf8_unicode_ci COMMENT '权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='角色表';

/*Data for the table `admin_role` */

insert  into `admin_role`(`id`,`code`,`name`,`des`,`create_user`,`create_date`,`update_user`,`update_date`,`status`,`rule`) values 
(1,'001','超级管理员','admins',NULL,NULL,NULL,NULL,1,NULL),
(2,'002','普通管理员',NULL,NULL,NULL,NULL,NULL,1,NULL);

/*Table structure for table `admin_rule` */

DROP TABLE IF EXISTS `admin_rule`;

CREATE TABLE `admin_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) DEFAULT NULL COMMENT '上级id',
  `route` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '路由',
  `title` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标',
  `type` int(1) DEFAULT NULL COMMENT '类型',
  `condition` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `order` int(11) DEFAULT NULL COMMENT '排序',
  `tips` text COLLATE utf8_unicode_ci COMMENT '提示',
  `is_show` int(1) DEFAULT NULL COMMENT '是否显示',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='路由权限表';

/*Data for the table `admin_rule` */

insert  into `admin_rule`(`id`,`pid`,`route`,`title`,`icon`,`type`,`condition`,`order`,`tips`,`is_show`,`status`) values 
(1,0,'','系统管理','glyphicon glyphicon-cog blue ',1,'权限列表',NULL,NULL,1,1),
(2,1,'admin/admin-rule/index','权限列表','glyphicon glyphicon-cog blue ',1,NULL,NULL,NULL,1,1),
(3,1,'admin/admin-user/index','管理员列表','glyphicon glyphicon-user ',1,NULL,NULL,NULL,1,1),
(4,1,'admin/admin-role/index','角色列表','glyphicon glyphicon-th',1,NULL,NULL,NULL,1,1),
(6,0,NULL,'文章内容','',1,'',1,'1',1,1),
(7,6,'article/category/index','分类管理','',1,'',1,'1',1,1),
(8,6,'article/article/index','内容管理','',1,'',1,'1',1,1);

/*Table structure for table `admin_user` */

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名',
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '盐',
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '密码',
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '重置密码密钥',
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '邮件',
  `timezone` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '时区，默认8区，东八区，北京/上海',
  `created_at` int(11) DEFAULT NULL COMMENT '创建时间',
  `updated_at` text COLLATE utf8_unicode_ci COMMENT '提示',
  `role_id` int(1) DEFAULT NULL COMMENT '是否显示',
  `status` int(1) DEFAULT NULL COMMENT '状态,1 启用 0禁用',
  `mobile` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机',
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '业务员推荐码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='新后台用户表';

/*Data for the table `admin_user` */

insert  into `admin_user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`timezone`,`created_at`,`updated_at`,`role_id`,`status`,`mobile`,`code`) values 
(1,'admin',NULL,'$2y$13$iJgThDdz54qxpr.HrK./Bec8LdRCeLWQTasA/pxiZCo7HiwKrfwIy',NULL,NULL,NULL,NULL,NULL,1,1,'13600000000',NULL);

/*Table structure for table `article` */

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键自增id',
  `categoryId` int(11) NOT NULL COMMENT '栏目',
  `type` smallint(3) DEFAULT NULL COMMENT '文章类型',
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `keywords` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '关键词',
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态',
  `voteUp` int(11) DEFAULT '0' COMMENT '赞数量',
  `sorting` int(6) DEFAULT NULL COMMENT '排序',
  `model` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '文章模型（继承相应栏目）',
  `allowComment` tinyint(1) DEFAULT NULL COMMENT '是否允许评论',
  `commentNumber` int(1) DEFAULT NULL COMMENT '评论数量',
  `recommendId` int(11) DEFAULT NULL COMMENT '推荐位',
  `url` varchar(500) COLLATE utf8_unicode_ci NOT NULL COMMENT '指定链接',
  `thumbnail` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '修改时间',
  `template` varchar(2000) COLLATE utf8_unicode_ci NOT NULL COMMENT '使用模板',
  PRIMARY KEY (`articleId`),
  KEY `article_ibfk_recommendId` (`recommendId`),
  KEY `article_ibfk_categoryId` (`categoryId`),
  CONSTRAINT `article_ibfk_categoryId` FOREIGN KEY (`categoryId`) REFERENCES `article_category` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `article_ibfk_recommendId` FOREIGN KEY (`recommendId`) REFERENCES `article_recommend` (`recommendId`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `article` */

/*Table structure for table `article_category` */

DROP TABLE IF EXISTS `article_category`;

CREATE TABLE `article_category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL COMMENT '栏目名称',
  `parentId` int(10) NOT NULL COMMENT '栏目上级id',
  `dir` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT '栏目路径，只能是英文下划数字',
  `categoryTemplate` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '使用模板',
  `articleTemplate` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '文章模板',
  `model` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '文章模型',
  `childrenIds` varchar(10000) COLLATE utf8_unicode_ci NOT NULL COMMENT '子id',
  `url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '指定链接',
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT '栏目图片',
  `display` tinyint(1) NOT NULL COMMENT '是否显示',
  `sorting` int(6) NOT NULL COMMENT '排序，降序',
  `createTime` int(10) NOT NULL COMMENT '创建时间',
  `updateTime` int(10) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章栏目';

/*Data for the table `article_category` */

/*Table structure for table `article_comment` */

DROP TABLE IF EXISTS `article_comment`;

CREATE TABLE `article_comment` (
  `commentId` int(11) NOT NULL AUTO_INCREMENT,
  `articleId` int(11) NOT NULL COMMENT '文章id',
  `userId` int(11) NOT NULL COMMENT '用户',
  `parentId` int(11) NOT NULL COMMENT '评论父id',
  `status` int(1) DEFAULT '1' COMMENT '状态',
  `content` varchar(1500) COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  PRIMARY KEY (`commentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='article_comment';

/*Data for the table `article_comment` */

/*Table structure for table `article_data` */

DROP TABLE IF EXISTS `article_data`;

CREATE TABLE `article_data` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '文章内容',
  PRIMARY KEY (`articleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章内容';

/*Data for the table `article_data` */

/*Table structure for table `article_recommend` */

DROP TABLE IF EXISTS `article_recommend`;

CREATE TABLE `article_recommend` (
  `recommendId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '标题',
  `createTime` int(10) NOT NULL COMMENT '创建时间',
  `updateTime` int(10) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`recommendId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章推荐位设置';

/*Data for the table `article_recommend` */

/*Table structure for table `article_view` */

DROP TABLE IF EXISTS `article_view`;

CREATE TABLE `article_view` (
  `articleId` int(11) NOT NULL AUTO_INCREMENT,
  `all` int(10) NOT NULL COMMENT '总浏览次数',
  `year` int(10) DEFAULT NULL COMMENT '年浏览次数',
  `month` int(10) DEFAULT NULL COMMENT '月浏览次数',
  `day` int(10) DEFAULT NULL COMMENT '日浏览次数',
  `yearUpdateTime` int(4) DEFAULT NULL COMMENT '年浏览上次更新时间',
  `monthUpdateTime` int(2) DEFAULT NULL COMMENT '月浏览上次更新时间',
  `dayUpdateTime` int(2) DEFAULT NULL COMMENT '日浏览上次更新时间',
  PRIMARY KEY (`articleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章浏览次数记录';

/*Data for the table `article_view` */

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values 
('m200312_021840_article',1588942148),
('m200312_033045_article_data',1588942148),
('m200312_033106_article_recommend',1588942149),
('m200312_033117_article_view',1588942149),
('m200312_033131_article_category',1588942150),
('m200319_012320_article_comment',1588942150),
('m200310_114148_admin_user',1588942338),
('m200310_114058_admin_rule',1588942338),
('m200310_113957_admin_role',1588942337),
('m200510_072808_admin_rule',1589111179);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
